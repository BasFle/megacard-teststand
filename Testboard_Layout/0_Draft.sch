<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Frame" color="7" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="no"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="no"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="no"/>
<layer number="101" name="tframe" color="7" fill="1" visible="no" active="no"/>
<layer number="102" name="bframe" color="7" fill="1" visible="no" active="no"/>
<layer number="143" name="tDrill" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="209" name="EXPNames" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microchip">
<description>&lt;b&gt;Microchip PIC Microcontrollers and other Devices&lt;/b&gt;&lt;p&gt;
Based on the following sources :
&lt;ul&gt;
&lt;li&gt;Microchip Data Book, 1993
&lt;li&gt;THE EMERGING WORLD STANDARD, 1995/1996
&lt;li&gt;Microchip, Technical Library CD-ROM, June 1998
&lt;li&gt;www.microchip.com
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL28-3">
<description>&lt;B&gt;Dual In Line&lt;/B&gt;&lt;p&gt;
package type P</description>
<wire x1="-17.78" y1="-1.27" x2="-17.78" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.27" x2="-17.78" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="17.78" y1="-2.54" x2="17.78" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="2.54" x2="17.78" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.653" y1="-2.54" x2="17.78" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="-13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="-16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-17.907" y="-2.54" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-15.748" y="-0.9398" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO28W">
<description>&lt;B&gt;28-Lead Plastic Small Outline (SO) &lt;/B&gt; Wide, 300 mil Body (SOIC)&lt;/B&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/devicedoc/39632c.pdf</description>
<wire x1="-8.1788" y1="-3.7132" x2="9.4742" y2="-3.7132" width="0.1524" layer="21"/>
<wire x1="9.4742" y1="-3.7132" x2="9.4742" y2="3.7132" width="0.1524" layer="21"/>
<wire x1="9.4742" y1="3.7132" x2="-8.1788" y2="3.7132" width="0.1524" layer="21"/>
<wire x1="-8.1788" y1="3.7132" x2="-8.1788" y2="-3.7132" width="0.1524" layer="21"/>
<circle x="-7.239" y="-3.1496" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-7.62" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="2" x="-6.35" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="3" x="-5.08" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="4" x="-3.81" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="5" x="-2.54" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="6" x="-1.27" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="7" x="0" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="8" x="1.27" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="9" x="2.54" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="10" x="3.81" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="20" x="2.54" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="19" x="3.81" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="18" x="5.08" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="17" x="6.35" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="16" x="7.62" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="15" x="8.89" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="14" x="8.89" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="13" x="7.62" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="12" x="6.35" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="11" x="5.08" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="21" x="1.27" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="22" x="0" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="23" x="-1.27" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="24" x="-2.54" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="25" x="-3.81" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="26" x="-5.08" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="27" x="-6.35" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="28" x="-7.62" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<text x="-8.509" y="-4.064" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="11.557" y="-4.064" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-7.874" y1="-5.2626" x2="-7.366" y2="-3.7386" layer="51"/>
<rectangle x1="-6.604" y1="-5.2626" x2="-6.096" y2="-3.7386" layer="51"/>
<rectangle x1="-5.334" y1="-5.2626" x2="-4.826" y2="-3.7386" layer="51"/>
<rectangle x1="-4.064" y1="-5.2626" x2="-3.556" y2="-3.7386" layer="51"/>
<rectangle x1="-2.794" y1="-5.2626" x2="-2.286" y2="-3.7386" layer="51"/>
<rectangle x1="-1.524" y1="-5.2626" x2="-1.016" y2="-3.7386" layer="51"/>
<rectangle x1="-0.254" y1="-5.2626" x2="0.254" y2="-3.7386" layer="51"/>
<rectangle x1="1.016" y1="-5.2626" x2="1.524" y2="-3.7386" layer="51"/>
<rectangle x1="2.286" y1="-5.2626" x2="2.794" y2="-3.7386" layer="51"/>
<rectangle x1="3.556" y1="-5.2626" x2="4.064" y2="-3.7386" layer="51"/>
<rectangle x1="4.826" y1="-5.2626" x2="5.334" y2="-3.7386" layer="51"/>
<rectangle x1="6.096" y1="-5.2626" x2="6.604" y2="-3.7386" layer="51"/>
<rectangle x1="7.366" y1="-5.2626" x2="7.874" y2="-3.7386" layer="51"/>
<rectangle x1="8.636" y1="-5.2626" x2="9.144" y2="-3.7386" layer="51"/>
<rectangle x1="8.636" y1="3.7386" x2="9.144" y2="5.2626" layer="51"/>
<rectangle x1="7.366" y1="3.7386" x2="7.874" y2="5.2626" layer="51"/>
<rectangle x1="6.096" y1="3.7386" x2="6.604" y2="5.2626" layer="51"/>
<rectangle x1="4.826" y1="3.7386" x2="5.334" y2="5.2626" layer="51"/>
<rectangle x1="3.556" y1="3.7386" x2="4.064" y2="5.2626" layer="51"/>
<rectangle x1="2.286" y1="3.7386" x2="2.794" y2="5.2626" layer="51"/>
<rectangle x1="1.016" y1="3.7386" x2="1.524" y2="5.2626" layer="51"/>
<rectangle x1="-0.254" y1="3.7386" x2="0.254" y2="5.2626" layer="51"/>
<rectangle x1="-1.524" y1="3.7386" x2="-1.016" y2="5.2626" layer="51"/>
<rectangle x1="-2.794" y1="3.7386" x2="-2.286" y2="5.2626" layer="51"/>
<rectangle x1="-4.064" y1="3.7386" x2="-3.556" y2="5.2626" layer="51"/>
<rectangle x1="-5.334" y1="3.7386" x2="-4.826" y2="5.2626" layer="51"/>
<rectangle x1="-6.604" y1="3.7386" x2="-6.096" y2="5.2626" layer="51"/>
<rectangle x1="-7.874" y1="3.7386" x2="-7.366" y2="5.2626" layer="51"/>
</package>
<package name="SSOP28">
<description>&lt;b&gt;Shrink Small Outline Package&lt;/b&gt;&lt;p&gt;
package type SS</description>
<wire x1="-5.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.6" x2="5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.6" x2="-5.1" y2="2.6" width="0.2032" layer="21"/>
<smd name="1" x="-4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="2" x="-3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="3" x="-2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="4" x="-2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="5" x="-1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="6" x="-0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="7" x="-0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="8" x="0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="9" x="0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="10" x="1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="11" x="2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="12" x="2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="17" x="2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="18" x="2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="19" x="1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="20" x="0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="22" x="-0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="23" x="-0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="24" x="-1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="25" x="-2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="26" x="-2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="27" x="-3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="28" x="-4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<text x="-5.476" y="-2.6299" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.8999" y="-0.68" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.4028" y1="-3.937" x2="-4.0472" y2="-2.6416" layer="51"/>
<rectangle x1="-3.7529" y1="-3.937" x2="-3.3973" y2="-2.6416" layer="51"/>
<rectangle x1="-3.1029" y1="-3.937" x2="-2.7473" y2="-2.6416" layer="51"/>
<rectangle x1="-2.4529" y1="-3.937" x2="-2.0973" y2="-2.6416" layer="51"/>
<rectangle x1="-1.8029" y1="-3.937" x2="-1.4473" y2="-2.6416" layer="51"/>
<rectangle x1="-1.1529" y1="-3.937" x2="-0.7973" y2="-2.6416" layer="51"/>
<rectangle x1="-0.5029" y1="-3.937" x2="-0.1473" y2="-2.6416" layer="51"/>
<rectangle x1="0.1473" y1="-3.937" x2="0.5029" y2="-2.6416" layer="51"/>
<rectangle x1="0.7973" y1="-3.937" x2="1.1529" y2="-2.6416" layer="51"/>
<rectangle x1="1.4473" y1="-3.937" x2="1.8029" y2="-2.6416" layer="51"/>
<rectangle x1="2.0973" y1="-3.937" x2="2.4529" y2="-2.6416" layer="51"/>
<rectangle x1="2.7473" y1="-3.937" x2="3.1029" y2="-2.6416" layer="51"/>
<rectangle x1="3.3973" y1="-3.937" x2="3.7529" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="-3.937" x2="4.4028" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="2.6416" x2="4.4028" y2="3.937" layer="51"/>
<rectangle x1="3.3973" y1="2.6416" x2="3.7529" y2="3.937" layer="51"/>
<rectangle x1="2.7473" y1="2.6416" x2="3.1029" y2="3.937" layer="51"/>
<rectangle x1="2.0973" y1="2.6416" x2="2.4529" y2="3.937" layer="51"/>
<rectangle x1="1.4473" y1="2.6416" x2="1.8029" y2="3.937" layer="51"/>
<rectangle x1="0.7973" y1="2.6416" x2="1.1529" y2="3.937" layer="51"/>
<rectangle x1="0.1473" y1="2.6416" x2="0.5029" y2="3.937" layer="51"/>
<rectangle x1="-0.5029" y1="2.6416" x2="-0.1473" y2="3.937" layer="51"/>
<rectangle x1="-1.1529" y1="2.6416" x2="-0.7973" y2="3.937" layer="51"/>
<rectangle x1="-1.8029" y1="2.6416" x2="-1.4473" y2="3.937" layer="51"/>
<rectangle x1="-2.4529" y1="2.6416" x2="-2.0973" y2="3.937" layer="51"/>
<rectangle x1="-3.1029" y1="2.6416" x2="-2.7473" y2="3.937" layer="51"/>
<rectangle x1="-3.7529" y1="2.6416" x2="-3.3973" y2="3.937" layer="51"/>
<rectangle x1="-4.4028" y1="2.6416" x2="-4.0472" y2="3.937" layer="51"/>
<rectangle x1="-5.1999" y1="-2.5999" x2="-4.225" y2="2.5999" layer="27"/>
</package>
<package name="QFN28-ML_6X6MM">
<description>&lt;b&gt;QFN28-ML_6X6MM&lt;/b&gt;&lt;p&gt;
Source: http://www.microchip.com .. 39637a.pdf</description>
<wire x1="-2.8984" y1="-2.8984" x2="2.8984" y2="-2.8984" width="0.2032" layer="51"/>
<wire x1="2.8984" y1="-2.8984" x2="2.8984" y2="2.8984" width="0.2032" layer="51"/>
<wire x1="2.8984" y1="2.8984" x2="-2.22" y2="2.8984" width="0.2032" layer="51"/>
<wire x1="-2.22" y1="2.8984" x2="-2.22" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-2.8984" y1="2.8984" x2="-2.22" y2="2.8984" width="0.2032" layer="21"/>
<wire x1="-2.22" y1="2.9" x2="-2.8984" y2="2.2216" width="0.2032" layer="21"/>
<wire x1="-2.8984" y1="2.2216" x2="-2.8984" y2="-2.8984" width="0.2032" layer="51"/>
<wire x1="-2.8984" y1="2.2216" x2="-2.8984" y2="2.8984" width="0.2032" layer="21"/>
<smd name="1" x="-2.7" y="1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="2" x="-2.7" y="1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="3" x="-2.7" y="0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="4" x="-2.7" y="0" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="5" x="-2.7" y="-0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="6" x="-2.7" y="-1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="7" x="-2.7" y="-1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="8" x="-1.95" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="9" x="-1.3" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="10" x="-0.65" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="11" x="0" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="12" x="0.65" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="13" x="1.3" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="14" x="1.95" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="15" x="2.7" y="-1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="16" x="2.7" y="-1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="17" x="2.7" y="-0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="18" x="2.7" y="0" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="19" x="2.7" y="0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="20" x="2.7" y="1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="21" x="2.7" y="1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="22" x="1.95" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="23" x="1.3" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="24" x="0.65" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="25" x="0" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="26" x="-0.65" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="27" x="-1.3" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="28" x="-1.95" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="EXP" x="0" y="0" dx="3.7" dy="3.7" layer="1" roundness="20" stop="no" cream="no"/>
<text x="-3.175" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.055" y1="1.768" x2="-2.3465" y2="2.132" layer="29"/>
<rectangle x1="-3.042" y1="1.7875" x2="-2.3595" y2="2.1125" layer="31"/>
<rectangle x1="-3.055" y1="1.118" x2="-2.3465" y2="1.482" layer="29"/>
<rectangle x1="-3.042" y1="1.1375" x2="-2.3595" y2="1.4625" layer="31"/>
<rectangle x1="-3.055" y1="0.468" x2="-2.3465" y2="0.832" layer="29"/>
<rectangle x1="-3.042" y1="0.4875" x2="-2.3595" y2="0.8125" layer="31"/>
<rectangle x1="-3.055" y1="-0.182" x2="-2.3465" y2="0.182" layer="29"/>
<rectangle x1="-3.042" y1="-0.1625" x2="-2.3595" y2="0.1625" layer="31"/>
<rectangle x1="-3.055" y1="-0.832" x2="-2.3465" y2="-0.468" layer="29"/>
<rectangle x1="-3.042" y1="-0.8125" x2="-2.3595" y2="-0.4875" layer="31"/>
<rectangle x1="-3.055" y1="-1.482" x2="-2.3465" y2="-1.118" layer="29"/>
<rectangle x1="-3.042" y1="-1.4625" x2="-2.3595" y2="-1.1375" layer="31"/>
<rectangle x1="-3.055" y1="-2.132" x2="-2.3465" y2="-1.768" layer="29"/>
<rectangle x1="-3.042" y1="-2.1125" x2="-2.3595" y2="-1.7875" layer="31"/>
<rectangle x1="-2.3042" y1="-2.8827" x2="-1.5958" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-2.2912" y1="-2.8632" x2="-1.6088" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-1.6542" y1="-2.8827" x2="-0.9458" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-1.6412" y1="-2.8632" x2="-0.9588" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-1.0042" y1="-2.8827" x2="-0.2958" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-0.9912" y1="-2.8632" x2="-0.3088" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-0.3542" y1="-2.8827" x2="0.3542" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-0.3412" y1="-2.8632" x2="0.3412" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="0.2958" y1="-2.8827" x2="1.0042" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="0.3088" y1="-2.8632" x2="0.9912" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="0.9458" y1="-2.8827" x2="1.6542" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="0.9588" y1="-2.8632" x2="1.6412" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="1.5958" y1="-2.8827" x2="2.3042" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="1.6088" y1="-2.8632" x2="2.2912" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="2.3465" y1="-2.132" x2="3.0549" y2="-1.768" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-2.1125" x2="3.0419" y2="-1.7875" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-1.482" x2="3.0549" y2="-1.118" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-1.4625" x2="3.0419" y2="-1.1375" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-0.832" x2="3.0549" y2="-0.468" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-0.8125" x2="3.0419" y2="-0.4875" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-0.182" x2="3.0549" y2="0.182" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-0.1625" x2="3.0419" y2="0.1625" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="0.468" x2="3.0549" y2="0.832" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="0.4875" x2="3.0419" y2="0.8125" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="1.118" x2="3.0549" y2="1.482" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="1.1375" x2="3.0419" y2="1.4625" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="1.768" x2="3.0549" y2="2.132" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="1.7875" x2="3.0419" y2="2.1125" layer="31" rot="R180"/>
<rectangle x1="1.5958" y1="2.5187" x2="2.3042" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="1.6088" y1="2.5382" x2="2.2912" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="0.9458" y1="2.5187" x2="1.6542" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="0.9588" y1="2.5382" x2="1.6412" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="0.2958" y1="2.5187" x2="1.0042" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="0.3088" y1="2.5382" x2="0.9912" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-0.3542" y1="2.5187" x2="0.3542" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-0.3412" y1="2.5382" x2="0.3412" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.0042" y1="2.5187" x2="-0.2958" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-0.9912" y1="2.5382" x2="-0.3088" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.6542" y1="2.5187" x2="-0.9458" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-1.6412" y1="2.5382" x2="-0.9588" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-2.3042" y1="2.5187" x2="-1.5958" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-2.2912" y1="2.5382" x2="-1.6088" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.859" y1="-1.859" x2="1.859" y2="1.859" layer="29"/>
<rectangle x1="-1.7355" y1="-1.7355" x2="1.7355" y2="1.7355" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="MCP23017">
<wire x1="-10.16" y1="22.86" x2="10.16" y2="22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="-10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-10.16" y="24.13" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SCL" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="SDA" x="-12.7" y="-5.08" length="short"/>
<pin name="A0" x="-12.7" y="-10.16" length="short" direction="in"/>
<pin name="A1" x="-12.7" y="-12.7" length="short" direction="in"/>
<pin name="A2" x="-12.7" y="-15.24" length="short" direction="in"/>
<pin name="!RESET" x="-12.7" y="15.24" length="short" direction="in"/>
<pin name="INTA" x="-12.7" y="10.16" length="short" direction="out"/>
<pin name="INTB" x="-12.7" y="7.62" length="short" direction="out"/>
<pin name="GPB0" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="GPB1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="GPB2" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="GPB3" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="GPB4" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="GPB5" x="12.7" y="-15.24" length="short" rot="R180"/>
<pin name="GPB6" x="12.7" y="-17.78" length="short" rot="R180"/>
<pin name="GPB7" x="12.7" y="-20.32" length="short" rot="R180"/>
<pin name="GPA0" x="12.7" y="20.32" length="short" rot="R180"/>
<pin name="GPA1" x="12.7" y="17.78" length="short" rot="R180"/>
<pin name="GPA2" x="12.7" y="15.24" length="short" rot="R180"/>
<pin name="GPA3" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="GPA4" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="GPA5" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="GPA6" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="GPA7" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="VDD" x="-12.7" y="20.32" length="short" direction="pwr"/>
<pin name="VSS" x="-12.7" y="-20.32" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP23017" prefix="IC">
<description>&lt;b&gt;http://ww1.microchip.com/downloads/en/DeviceDoc/21952a.pdf&lt;/b&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/DeviceDoc/21952a.pdf</description>
<gates>
<gate name="G$1" symbol="MCP23017" x="0" y="0"/>
</gates>
<devices>
<device name="SP" package="DIL28-3">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SP" constant="no"/>
<attribute name="OC_FARNELL" value="1332088" constant="no"/>
<attribute name="OC_NEWARK" value="31K2959" constant="no"/>
</technology>
</technologies>
</device>
<device name="SO" package="SO28W">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SO" constant="no"/>
<attribute name="OC_FARNELL" value="1332087" constant="no"/>
<attribute name="OC_NEWARK" value="31K2958" constant="no"/>
</technology>
</technologies>
</device>
<device name="SS" package="SSOP28">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SS" constant="no"/>
<attribute name="OC_FARNELL" value="1467674" constant="no"/>
<attribute name="OC_NEWARK" value="31K2960" constant="no"/>
</technology>
</technologies>
</device>
<device name="ML" package="QFN28-ML_6X6MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="14"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="GPA0" pad="17"/>
<connect gate="G$1" pin="GPA1" pad="18"/>
<connect gate="G$1" pin="GPA2" pad="19"/>
<connect gate="G$1" pin="GPA3" pad="20"/>
<connect gate="G$1" pin="GPA4" pad="21"/>
<connect gate="G$1" pin="GPA5" pad="22"/>
<connect gate="G$1" pin="GPA6" pad="23"/>
<connect gate="G$1" pin="GPA7" pad="24"/>
<connect gate="G$1" pin="GPB0" pad="25"/>
<connect gate="G$1" pin="GPB1" pad="26"/>
<connect gate="G$1" pin="GPB2" pad="27"/>
<connect gate="G$1" pin="GPB3" pad="28"/>
<connect gate="G$1" pin="GPB4" pad="1"/>
<connect gate="G$1" pin="GPB5" pad="2"/>
<connect gate="G$1" pin="GPB6" pad="3"/>
<connect gate="G$1" pin="GPB7" pad="4"/>
<connect gate="G$1" pin="INTA" pad="16"/>
<connect gate="G$1" pin="INTB" pad="15"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="9"/>
<connect gate="G$1" pin="VDD" pad="5"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/ML" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="31K2957" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="0_HTL_analog">
<description>&lt;b&gt;Analoge Bautele für den FTKL Unterricht (HTL-Rankweil Version 1.0 110121)&lt;/b&gt;&lt;p&gt;
Zusammengestellt für den FTKL Unterricht:
&lt;ul&gt;
&lt;li&gt;Grundelemente (GND, Vcc, ...
&lt;li&gt;Widerstände
&lt;li&gt;Potentiometer
&lt;li&gt;Kondensatoren
&lt;li&gt;Elko
&lt;li&gt;Spulen
&lt;li&gt;Quarz
&lt;li&gt;Gleichrichter
&lt;li&gt;Dioden
&lt;li&gt;Transitoren
&lt;li&gt;LED
&lt;/ul&gt;
&lt;author&gt;www.HTL-Rankweil.at&lt;/author&gt;&lt;p&gt;
leopold.moosbrugger@htlr.snv.at</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="0_HTL_uC-MEGACARD">
<packages>
<package name="20POL">
<wire x1="5.715" y1="-10.16" x2="-7.62" y2="-10.16" width="0.6096" layer="21"/>
<wire x1="5.715" y1="-7.62" x2="-7.62" y2="-7.62" width="0.6096" layer="21"/>
<wire x1="5.715" y1="-5.08" x2="-7.62" y2="-5.08" width="0.6096" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="-7.62" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="5.715" y1="0" x2="-7.62" y2="0" width="0.6096" layer="21"/>
<wire x1="5.715" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="2.54" x2="-7.62" y2="2.54" width="0.6096" layer="21"/>
<wire x1="5.715" y1="5.08" x2="-7.62" y2="5.08" width="0.6096" layer="21"/>
<wire x1="5.715" y1="7.62" x2="-7.62" y2="7.62" width="0.6096" layer="21"/>
<wire x1="5.715" y1="10.16" x2="-7.62" y2="10.16" width="0.6096" layer="21"/>
<wire x1="5.715" y1="12.7" x2="-7.62" y2="12.7" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-11.43" x2="0.635" y2="-9.525" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-9.525" x2="0.635" y2="-8.255" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-8.255" x2="0.635" y2="-6.985" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-6.985" x2="0.635" y2="-5.715" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-5.715" x2="0.635" y2="-4.445" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-4.445" x2="0.635" y2="-3.175" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-3.175" x2="0.635" y2="-1.905" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-1.905" x2="0.635" y2="-0.635" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.635" y2="0.635" width="0.6096" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="1.905" width="0.6096" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="3.175" width="0.6096" layer="21"/>
<wire x1="0.635" y1="3.175" x2="0.635" y2="4.445" width="0.6096" layer="21"/>
<wire x1="0.635" y1="4.445" x2="0.635" y2="5.715" width="0.6096" layer="21"/>
<wire x1="0.635" y1="5.715" x2="0.635" y2="6.985" width="0.6096" layer="21"/>
<wire x1="0.635" y1="6.985" x2="0.635" y2="8.255" width="0.6096" layer="21"/>
<wire x1="0.635" y1="8.255" x2="0.635" y2="9.525" width="0.6096" layer="21"/>
<wire x1="0.635" y1="9.525" x2="0.635" y2="10.795" width="0.6096" layer="21"/>
<wire x1="0.635" y1="10.795" x2="0.635" y2="12.065" width="0.6096" layer="21"/>
<wire x1="0.635" y1="12.065" x2="0.635" y2="13.335" width="0.6096" layer="21"/>
<wire x1="0.635" y1="13.335" x2="0.635" y2="13.97" width="0.6096" layer="21"/>
<wire x1="0.635" y1="13.97" x2="0" y2="13.97" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="13.97" x2="-1.27" y2="13.97" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="13.97" x2="-1.905" y2="13.97" width="0.6096" layer="21"/>
<wire x1="-1.905" y1="13.97" x2="-1.905" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="-1.905" y1="-11.43" x2="-1.27" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="-11.43" x2="-0.635" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="0.635" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-11.43" x2="1.27" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-11.43" x2="1.27" y2="-9.525" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-9.525" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-8.255" x2="1.27" y2="-8.255" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.985" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-5.715" x2="1.27" y2="-5.715" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-4.445" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-3.175" x2="1.27" y2="-3.175" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-1.905" width="0.6096" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="1.27" y2="-0.635" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.6096" layer="21"/>
<wire x1="1.27" y1="0.635" x2="0.635" y2="0.635" width="0.6096" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.27" y2="1.905" width="0.6096" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="3.175" width="0.6096" layer="21"/>
<wire x1="0.635" y1="3.175" x2="1.27" y2="3.175" width="0.6096" layer="21"/>
<wire x1="0.635" y1="4.445" x2="1.27" y2="4.445" width="0.6096" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.6096" layer="21"/>
<wire x1="1.27" y1="5.715" x2="0.635" y2="5.715" width="0.6096" layer="21"/>
<wire x1="0.635" y1="6.985" x2="1.27" y2="6.985" width="0.6096" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.6096" layer="21"/>
<wire x1="1.27" y1="8.255" x2="0.635" y2="8.255" width="0.6096" layer="21"/>
<wire x1="0.635" y1="9.525" x2="1.27" y2="9.525" width="0.6096" layer="21"/>
<wire x1="1.27" y1="9.525" x2="1.27" y2="10.795" width="0.6096" layer="21"/>
<wire x1="1.27" y1="10.795" x2="0.635" y2="10.795" width="0.6096" layer="21"/>
<wire x1="0.635" y1="12.065" x2="1.27" y2="12.065" width="0.6096" layer="21"/>
<wire x1="1.27" y1="12.065" x2="1.27" y2="13.335" width="0.6096" layer="21"/>
<wire x1="1.27" y1="13.335" x2="0.635" y2="13.335" width="0.6096" layer="21"/>
<wire x1="0" y1="13.335" x2="0" y2="-10.795" width="0.6096" layer="21"/>
<wire x1="0" y1="-10.795" x2="-0.635" y2="-10.795" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="-10.795" x2="-0.635" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-0.635" y2="13.335" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="13.335" x2="-1.27" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="-11.43" x2="0" y2="-11.43" width="0.6096" layer="21"/>
<wire x1="0" y1="-11.43" x2="0" y2="-10.795" width="0.6096" layer="21"/>
<wire x1="0" y1="-10.795" x2="0" y2="13.97" width="0.6096" layer="21"/>
<wire x1="0" y1="13.97" x2="-0.635" y2="13.97" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="13.97" x2="-0.635" y2="13.335" width="0.6096" layer="21"/>
<wire x1="-0.635" y1="13.335" x2="-1.27" y2="13.335" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="13.335" x2="-1.27" y2="13.97" width="0.6096" layer="21"/>
<pad name="P$1" x="5.715" y="12.7" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$2" x="3.175" y="12.7" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$3" x="5.715" y="10.16" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$4" x="3.175" y="10.16" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$5" x="5.715" y="7.62" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$6" x="3.175" y="7.62" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$7" x="5.715" y="5.08" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$8" x="3.175" y="5.08" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$9" x="5.715" y="2.54" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$10" x="3.175" y="2.54" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$11" x="5.715" y="0" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$12" x="3.175" y="0" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$13" x="5.715" y="-2.54" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$14" x="3.175" y="-2.54" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$15" x="5.715" y="-5.08" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$16" x="3.175" y="-5.08" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$17" x="5.715" y="-7.62" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$18" x="3.175" y="-7.62" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<pad name="P$19" x="5.715" y="-10.16" drill="1.016" diameter="1.524" shape="offset"/>
<pad name="P$20" x="3.175" y="-10.16" drill="1.016" diameter="1.524" shape="offset" rot="R180"/>
<text x="-5.08" y="17.78" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.715" y="15.24" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="PTC-1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.4763" y1="0.8731" x2="-1.7463" y2="0.8731" width="0.1524" layer="51"/>
<wire x1="-1.7463" y1="0.8731" x2="-1.7463" y2="-0.8732" width="0.1524" layer="51"/>
<wire x1="-1.7463" y1="-0.8732" x2="-0.4763" y2="-0.8732" width="0.1524" layer="51"/>
<wire x1="0.4763" y1="0.8731" x2="1.905" y2="0.8731" width="0.1524" layer="51"/>
<wire x1="1.905" y1="0.8731" x2="1.905" y2="-0.8732" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-0.8732" x2="0.4763" y2="-0.8732" width="0.1524" layer="51"/>
<wire x1="-0.4763" y1="0.8731" x2="0.4763" y2="0.8731" width="0.1524" layer="21"/>
<wire x1="-0.4763" y1="-0.8732" x2="0.4763" y2="-0.8732" width="0.1524" layer="21"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7684" y1="-0.8763" x2="-1.0318" y2="0.8763" layer="51"/>
<rectangle x1="1.1907" y1="-0.8763" x2="1.9273" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="ML6">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.2863" x2="7.62" y2="4.2863" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.2863" x2="-7.62" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.2863" x2="7.62" y2="4.2863" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.2863" x2="2.032" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.2863" x2="-2.032" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-4.2863" x2="-7.62" y2="-4.2863" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.6353" x2="-2.032" y2="-2.6353" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="-2.6416" x2="-2.032" y2="-3.1687" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-2.6416" x2="2.032" y2="-3.1687" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-1.27" drill="1" diameter="1.5748" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1" diameter="1.5748" shape="square"/>
<pad name="3" x="0" y="-1.27" drill="1" diameter="1.5748" shape="square"/>
<pad name="4" x="0" y="1.27" drill="1" diameter="1.5748" shape="square"/>
<pad name="5" x="2.54" y="-1.27" drill="1" diameter="1.5748" shape="square"/>
<pad name="6" x="2.54" y="1.27" drill="1" diameter="1.5748" shape="square"/>
<text x="-7.62" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-0.4318" y="-4.0894" size="1.27" layer="51" ratio="10">6</text>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="ML6L">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-5.08" y1="10.287" x2="-2.54" y2="10.287" width="0.254" layer="21"/>
<wire x1="-2.54" y1="10.287" x2="-3.81" y2="7.747" width="0.254" layer="21"/>
<wire x1="-3.81" y1="7.747" x2="-5.08" y2="10.287" width="0.254" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="2.159" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="7.62" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.429" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="4.445" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.032" x2="-1.905" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="2.032" x2="-0.635" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.032" x2="0.635" y2="2.032" width="0.1524" layer="51"/>
<wire x1="0.635" y1="2.032" x2="1.905" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.032" x2="3.175" y2="2.032" width="0.1524" layer="51"/>
<wire x1="0" y1="10.16" x2="0" y2="10.414" width="0.508" layer="21"/>
<wire x1="3.302" y1="10.287" x2="3.302" y2="7.493" width="0.1524" layer="21"/>
<wire x1="3.302" y1="7.493" x2="7.112" y2="7.493" width="0.1524" layer="21"/>
<wire x1="7.112" y1="10.287" x2="7.112" y2="7.493" width="0.1524" layer="21"/>
<wire x1="7.112" y1="10.287" x2="3.302" y2="10.287" width="0.1524" layer="21"/>
<wire x1="7.62" y1="10.922" x2="7.62" y2="2.032" width="0.1524" layer="21"/>
<wire x1="7.62" y1="2.032" x2="6.477" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="-7.62" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="10.922" x2="-7.62" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="2.032" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.032" x2="5.461" y2="1.397" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.032" x2="3.175" y2="2.032" width="0.1524" layer="21"/>
<wire x1="5.461" y1="1.397" x2="6.477" y2="1.397" width="0.1524" layer="21"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="1.397" width="0.1524" layer="21"/>
<wire x1="6.477" y1="2.032" x2="5.461" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="2.032" x2="-5.461" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="2.032" x2="-3.175" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="1.397" x2="-6.477" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="2.032" x2="-6.477" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="2.032" x2="-5.461" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-4.6228" y="-1.6764" size="1.27" layer="21" ratio="10">1</text>
<text x="-4.6482" y="0.3556" size="1.27" layer="21" ratio="10">2</text>
<text x="-7.6454" y="11.43" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.6096" y="11.43" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="8.128" size="1.524" layer="21" ratio="10">6</text>
<rectangle x1="-0.254" y1="4.445" x2="0.254" y2="10.287" layer="21"/>
<rectangle x1="-4.953" y1="9.779" x2="-2.667" y2="10.287" layer="21"/>
<rectangle x1="-4.699" y1="9.271" x2="-2.921" y2="9.779" layer="21"/>
<rectangle x1="-4.445" y1="8.763" x2="-3.175" y2="9.271" layer="21"/>
<rectangle x1="-4.191" y1="8.255" x2="-3.429" y2="8.763" layer="21"/>
<rectangle x1="-3.937" y1="8.001" x2="-3.683" y2="8.255" layer="21"/>
<rectangle x1="-2.794" y1="0.381" x2="-2.286" y2="2.032" layer="51"/>
<rectangle x1="-2.794" y1="-0.381" x2="-2.286" y2="0.381" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-0.381" layer="51"/>
<rectangle x1="-0.254" y1="0.381" x2="0.254" y2="2.032" layer="51"/>
<rectangle x1="-0.254" y1="-0.381" x2="0.254" y2="0.381" layer="21"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-0.381" layer="51"/>
<rectangle x1="2.286" y1="0.381" x2="2.794" y2="2.032" layer="51"/>
<rectangle x1="2.286" y1="-0.381" x2="2.794" y2="0.381" layer="21"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-0.381" layer="51"/>
</package>
<package name="PIN10">
<wire x1="-8.89" y1="0.9525" x2="-8.5725" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.5725" y1="1.27" x2="-6.6675" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.6675" y1="1.27" x2="-6.35" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.9525" x2="-6.0325" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.0325" y1="1.27" x2="-4.1275" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.1275" y1="1.27" x2="-3.81" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.9525" x2="-3.4925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.4925" y1="1.27" x2="-1.5875" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.5875" y1="1.27" x2="-1.27" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.9525" x2="-1.5875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.5875" y1="-1.27" x2="-3.4925" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.4925" y1="-1.27" x2="-3.81" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.9525" x2="-4.1275" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.1275" y1="-1.27" x2="-6.0325" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.0325" y1="-1.27" x2="-6.35" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.9525" x2="-6.6675" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.6675" y1="-1.27" x2="-8.5725" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.5725" y1="-1.27" x2="-8.89" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.9525" x2="-8.89" y2="0.9525" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1" diameter="1.6764" shape="square"/>
<pad name="2" x="-5.08" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-2.54" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<text x="-8.89" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-8.89" y="1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<wire x1="1.27" y1="0.9525" x2="1.5875" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.5875" y1="1.27" x2="3.4925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.4925" y1="1.27" x2="3.81" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.9525" x2="4.1275" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.1275" y1="1.27" x2="6.0325" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.0325" y1="1.27" x2="6.35" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.9525" x2="6.6675" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.6675" y1="1.27" x2="8.5725" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.5725" y1="1.27" x2="8.89" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.9525" x2="8.5725" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.5725" y1="-1.27" x2="6.6675" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.6675" y1="-1.27" x2="6.35" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.9525" x2="6.0325" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.0325" y1="-1.27" x2="4.1275" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.1275" y1="-1.27" x2="3.81" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.9525" x2="3.4925" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.4925" y1="-1.27" x2="1.5875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.5875" y1="-1.27" x2="1.27" y2="-0.9525" width="0.1524" layer="21"/>
<pad name="4" x="0" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<pad name="5" x="2.54" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<pad name="6" x="5.08" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<pad name="7" x="7.62" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<wire x1="-0.9525" y1="1.27" x2="0.9525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9525" y1="-1.27" x2="-0.9525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.9525" x2="0.9525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.9525" y1="-1.27" x2="-1.27" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="0.9525" y1="1.27" x2="1.27" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.9525" x2="-0.9525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.9525" x2="9.2075" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.2075" y1="1.27" x2="11.1125" y2="1.27" width="0.1524" layer="21"/>
<wire x1="11.1125" y1="1.27" x2="11.43" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0.9525" x2="11.7475" y2="1.27" width="0.1524" layer="21"/>
<wire x1="11.7475" y1="1.27" x2="13.6525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="13.6525" y1="1.27" x2="13.97" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0.9525" x2="14.2875" y2="1.27" width="0.1524" layer="21"/>
<wire x1="14.2875" y1="1.27" x2="16.1925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="16.1925" y1="1.27" x2="16.51" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="16.51" y1="0.9525" x2="16.51" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="16.51" y1="-0.9525" x2="16.1925" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="16.1925" y1="-1.27" x2="14.2875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="14.2875" y1="-1.27" x2="13.97" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="13.97" y1="-0.9525" x2="13.6525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="13.6525" y1="-1.27" x2="11.7475" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="11.7475" y1="-1.27" x2="11.43" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-0.9525" x2="11.1125" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="11.1125" y1="-1.27" x2="9.2075" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.2075" y1="-1.27" x2="8.89" y2="-0.9525" width="0.1524" layer="21"/>
<pad name="8" x="10.16" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<pad name="9" x="12.7" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<pad name="10" x="15.24" y="0" drill="1" diameter="1.6764" shape="octagon"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<wire x1="-6.35" y1="-0.9525" x2="-6.35" y2="0.9525" width="0.1524" layer="21"/>
</package>
<package name="PIN2">
<wire x1="-2.54" y1="0.9525" x2="-2.2225" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-0.3175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="1.27" x2="0" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="0" y1="0.9525" x2="0.3175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.3175" y1="1.27" x2="2.2225" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.2225" y1="1.27" x2="2.54" y2="0.9525" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.9525" x2="2.2225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.2225" y1="-1.27" x2="0.3175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.3175" y1="-1.27" x2="0" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.9525" x2="-0.3175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="-1.27" x2="-2.2225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.2225" y1="-1.27" x2="-2.54" y2="-0.9525" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="21"/>
<pad name="P$2" x="-1.27" y="0" drill="1" diameter="1.778" shape="square"/>
<pad name="P$3" x="1.27" y="0" drill="1" diameter="1.778" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<wire x1="0" y1="0.9525" x2="0" y2="-0.9525" width="0.1524" layer="21"/>
</package>
<package name="PIN2-B">
<circle x="-1.27" y="0" radius="0.635" width="0.1524" layer="21"/>
<circle x="1.27" y="0" radius="0.635" width="0.1524" layer="21"/>
<pad name="P$2" x="-1.27" y="0" drill="0.8" diameter="1.8288" shape="square"/>
<pad name="P$3" x="1.27" y="0" drill="0.8" diameter="1.8288" shape="square"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="20POL">
<wire x1="-25.4" y1="22.86" x2="-17.78" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-17.78" y1="22.86" x2="-17.78" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-17.78" y1="-5.08" x2="-25.4" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="-5.08" x2="-25.4" y2="22.86" width="0.4064" layer="94"/>
<text x="-16.7682" y="21.6593" size="1.778" layer="95">1</text>
<text x="-27.6125" y="21.6593" size="1.778" layer="95">2</text>
<text x="-16.9864" y="-5.437" size="1.778" layer="95">19</text>
<text x="-29.1702" y="-5.6553" size="1.778" layer="95">20</text>
<text x="-20.7392" y="0.3275" size="1.778" layer="94" rot="R90">Stiftleiste 20pol</text>
<text x="-26.5243" y="-7.8591" size="1.778" layer="95">&gt;NAME</text>
<text x="-26.8517" y="-10.3695" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P$1" x="-15.24" y="20.32" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="P$2" x="-27.94" y="20.32" visible="off" length="short" direction="pwr"/>
<pin name="P$3" x="-15.24" y="17.78" visible="off" length="short" rot="R180"/>
<pin name="P$4" x="-27.94" y="17.78" visible="off" length="short"/>
<pin name="P$5" x="-15.24" y="15.24" visible="off" length="short" rot="R180"/>
<pin name="P$6" x="-27.94" y="15.24" visible="off" length="short"/>
<pin name="P$7" x="-15.24" y="12.7" visible="off" length="short" rot="R180"/>
<pin name="P$8" x="-27.94" y="12.7" visible="off" length="short"/>
<pin name="P$9" x="-15.24" y="10.16" visible="off" length="short" rot="R180"/>
<pin name="P$10" x="-27.94" y="10.16" visible="off" length="short"/>
<pin name="P$11" x="-15.24" y="7.62" visible="off" length="short" rot="R180"/>
<pin name="P$12" x="-27.94" y="7.62" visible="off" length="short"/>
<pin name="P$13" x="-15.24" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="P$14" x="-27.94" y="5.08" visible="off" length="short"/>
<pin name="P$15" x="-15.24" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="P$16" x="-27.94" y="2.54" visible="off" length="short"/>
<pin name="P$17" x="-15.24" y="0" visible="off" length="short" rot="R180"/>
<pin name="P$18" x="-27.94" y="0" visible="off" length="short"/>
<pin name="P$19" x="-15.24" y="-2.54" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="P$20" x="-27.94" y="-2.54" visible="off" length="short" direction="pwr"/>
</symbol>
<symbol name="PTC-1206">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.778" x2="-1.524" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.778" x2="2.032" y2="1.778" width="0.1524" layer="94"/>
<text x="-3.556" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.508" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="06P">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="PIN10">
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.032" x2="24.13" y2="2.032" width="0.127" layer="94"/>
<wire x1="24.13" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="2.032" width="0.127" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<text x="3.048" y="7.747" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="5.461" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="5.08" width="0.4064" layer="94"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="5.08" width="0.4064" layer="94"/>
<wire x1="24.13" y1="2.032" x2="24.13" y2="1.27" width="0.127" layer="94"/>
<wire x1="15.24" y1="2.54" x2="15.24" y2="5.08" width="0.4064" layer="94"/>
<pin name="5" x="10.16" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="6" x="12.7" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="7" x="15.24" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<pin name="4" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="17.78" y1="2.54" x2="17.78" y2="5.08" width="0.4064" layer="94"/>
<wire x1="20.32" y1="2.54" x2="20.32" y2="5.08" width="0.4064" layer="94"/>
<wire x1="22.86" y1="2.54" x2="22.86" y2="5.08" width="0.4064" layer="94"/>
<pin name="8" x="17.78" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="9" x="20.32" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="10" x="22.86" y="0" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="PIN2">
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.032" x2="3.81" y2="2.032" width="0.127" layer="94"/>
<wire x1="3.81" y1="2.032" x2="3.81" y2="1.27" width="0.127" layer="94"/>
<wire x1="3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="2.032" width="0.127" layer="94"/>
<text x="-2.032" y="7.747" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="5.461" size="1.778" layer="96">&gt;VALUE</text>
<pin name="KL1" x="0" y="0" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="KL2" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="20POL" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="20POL" x="30.48" y="12.7"/>
</gates>
<devices>
<device name="" package="20POL">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$17" pad="P$17"/>
<connect gate="G$1" pin="P$18" pad="P$18"/>
<connect gate="G$1" pin="P$19" pad="P$19"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$20" pad="P$20"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PTC-1206" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="PTC-1206" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PTC-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ML6" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="06P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML6">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L" package="ML6L">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN10" prefix="X">
<gates>
<gate name="G$1" symbol="PIN10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIN10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN2" prefix="X">
<gates>
<gate name="G$2" symbol="PIN2" x="0" y="0"/>
</gates>
<devices>
<device name="&quot;" package="PIN2">
<connects>
<connect gate="G$2" pin="KL1" pad="P$2"/>
<connect gate="G$2" pin="KL2" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="PIN2-B">
<connects>
<connect gate="G$2" pin="KL1" pad="P$2"/>
<connect gate="G$2" pin="KL2" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BSS138">
<packages>
<package name="SOT65P210X110-3N">
<smd name="1" x="-0.9398" y="0.6604" dx="1.1684" dy="0.4572" layer="1"/>
<smd name="2" x="-0.9398" y="-0.6604" dx="1.1684" dy="0.4572" layer="1"/>
<smd name="3" x="0.9398" y="0" dx="1.1684" dy="0.4572" layer="1"/>
<wire x1="0.6858" y1="0.5588" x2="0.6858" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="-0.127" y1="-1.0922" x2="0.6858" y2="-1.0922" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="-1.0922" x2="0.6858" y2="-0.5588" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="1.0922" x2="-0.127" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0.1016" x2="-0.6858" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.0922" x2="-0.0254" y2="0.7874" width="0.1524" layer="21" curve="-97"/>
<wire x1="-0.6858" y1="-1.0922" x2="0.6858" y2="-1.0922" width="0" layer="51"/>
<wire x1="0.6858" y1="-1.0922" x2="0.6858" y2="1.0922" width="0" layer="51"/>
<wire x1="0.6858" y1="1.0922" x2="-0.6858" y2="1.0922" width="0" layer="51"/>
<wire x1="-0.6858" y1="1.0922" x2="-0.6858" y2="-1.0922" width="0" layer="51"/>
<wire x1="-0.6858" y1="0.4572" x2="-0.6858" y2="0.8382" width="0" layer="51"/>
<wire x1="-0.6858" y1="0.8382" x2="-1.0922" y2="0.8382" width="0" layer="51"/>
<wire x1="-1.0922" y1="0.8382" x2="-1.0922" y2="0.4572" width="0" layer="51"/>
<wire x1="-1.0922" y1="0.4572" x2="-0.6858" y2="0.4572" width="0" layer="51"/>
<wire x1="-0.6858" y1="-0.8382" x2="-0.6858" y2="-0.4572" width="0" layer="51"/>
<wire x1="-0.6858" y1="-0.4572" x2="-1.0922" y2="-0.4572" width="0" layer="51"/>
<wire x1="-1.0922" y1="-0.4572" x2="-1.0922" y2="-0.8382" width="0" layer="51"/>
<wire x1="-1.0922" y1="-0.8382" x2="-0.6858" y2="-0.8382" width="0" layer="51"/>
<wire x1="0.6858" y1="0.2032" x2="0.6858" y2="-0.2032" width="0" layer="51"/>
<wire x1="0.6858" y1="-0.2032" x2="1.0922" y2="-0.2032" width="0" layer="51"/>
<wire x1="1.0922" y1="-0.2032" x2="1.0922" y2="0.2032" width="0" layer="51"/>
<wire x1="1.0922" y1="0.2032" x2="0.6858" y2="0.2032" width="0" layer="51"/>
<wire x1="0.3048" y1="1.0922" x2="-0.3048" y2="1.0922" width="0" layer="51" curve="-180"/>
<text x="-0.5842" y="1.2796" size="0.254" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-0.5842" y="-1.497" size="0.254" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NMOS">
<description>&lt;h3&gt; N-channel MOSFET transistor&lt;/h3&gt;
Switches electronic signals</description>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="0.6858" x2="-1.9812" y2="-0.8382" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.2954" x2="-1.9812" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="-1.9812" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="2.54" x2="-1.9812" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="-1.9812" y2="1.0922" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.2192" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.7112" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.8034" x2="2.54" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="3.302" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="1.778" y="-0.7112"/>
<vertex x="2.54" y="0.5588"/>
<vertex x="3.302" y="-0.7112"/>
</polygon>
<wire x1="3.302" y1="0.5588" x2="3.4798" y2="0.7366" width="0.1524" layer="94"/>
<wire x1="1.6002" y1="0.381" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.9812" y="0"/>
<vertex x="-1.2192" y="0.254"/>
<vertex x="-1.2192" y="-0.254"/>
</polygon>
<text x="5.08" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS138BKW" prefix="U">
<description>N-channel Trench MOSFET</description>
<gates>
<gate name="G$1" symbol="NMOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT65P210X110-3N">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="BSS138BKW" constant="no"/>
<attribute name="OC_FARNELL" value="2053836" constant="no"/>
<attribute name="OC_NEWARK" value="61T7488" constant="no"/>
<attribute name="PACKAGE" value="SOT-23-3" constant="no"/>
<attribute name="SUPPLIER" value="NXP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="0_HTL_MegaCard_V3">
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="ML10">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-8.89" y1="3.175" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-3.175" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-8.89" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.445" x2="-8.89" y2="4.445" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.445" x2="5.461" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.445" x2="10.16" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.445" x2="-10.16" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-3.175" x2="4.572" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-8.89" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="8.89" y1="4.445" x2="8.89" y2="4.699" width="0.1524" layer="21"/>
<wire x1="8.89" y1="4.699" x2="7.62" y2="4.699" width="0.1524" layer="21"/>
<wire x1="7.62" y1="4.445" x2="7.62" y2="4.699" width="0.1524" layer="21"/>
<wire x1="8.89" y1="4.445" x2="10.16" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.699" x2="-8.89" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="4.699" x2="-8.89" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.699" x2="-7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-3.175" x2="3.048" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-3.175" x2="4.572" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-3.175" x2="3.048" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-4.445" x2="2.54" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-3.937" x2="5.461" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-3.937" x2="4.572" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-3.429" x2="9.144" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="9.144" y1="-3.429" x2="9.144" y2="3.429" width="0.0508" layer="21"/>
<wire x1="9.144" y1="3.429" x2="-9.144" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-9.144" y1="3.429" x2="-9.144" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-9.144" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-3.429" x2="3.048" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-3.937" x2="2.54" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-3.429" x2="4.572" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-3.937" x2="3.048" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-4.445" x2="-4.445" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-4.318" x2="-4.445" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-4.318" x2="-5.715" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-4.445" x2="-5.715" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-4.445" x2="-10.16" y2="-4.445" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51" rot="R90"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-5.08" y="-1.27" drill="1" diameter="1.524" shape="square" rot="R90"/>
<pad name="2" x="-5.08" y="1.27" drill="1" diameter="1.524" shape="square" rot="R90"/>
<pad name="3" x="-2.54" y="-1.27" drill="1" diameter="1.524" shape="square" rot="R270"/>
<pad name="4" x="-2.54" y="1.27" drill="1" diameter="1.524" shape="square" rot="R90"/>
<pad name="5" x="0" y="-1.27" drill="1" diameter="1.524" shape="square" rot="R90"/>
<pad name="6" x="0" y="1.27" drill="1" diameter="1.524" shape="square" rot="R90"/>
<pad name="7" x="2.54" y="-1.27" drill="1" diameter="1.524" shape="square"/>
<pad name="8" x="2.54" y="1.27" drill="1" diameter="1.524" shape="square"/>
<pad name="9" x="5.08" y="-1.27" drill="1" diameter="1.524" shape="square"/>
<pad name="10" x="5.08" y="1.27" drill="1" diameter="1.524" shape="square"/>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">10</text>
<text x="-10.16" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-7.62" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<polygon width="0" layer="29">
<vertex x="-5.9436" y="2.1336"/>
<vertex x="-5.9436" y="2.3368"/>
<vertex x="-5.4864" y="2.794"/>
<vertex x="-4.6736" y="2.794"/>
<vertex x="-4.2164" y="2.3368"/>
<vertex x="-4.2164" y="2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-3.4036" y="2.1336"/>
<vertex x="-3.4036" y="2.3368"/>
<vertex x="-2.9464" y="2.794"/>
<vertex x="-2.1336" y="2.794"/>
<vertex x="-1.6764" y="2.3368"/>
<vertex x="-1.6764" y="2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-0.8636" y="2.1336"/>
<vertex x="-0.8636" y="2.3368"/>
<vertex x="-0.4064" y="2.794"/>
<vertex x="0.4064" y="2.794"/>
<vertex x="0.8636" y="2.3368"/>
<vertex x="0.8636" y="2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.6764" y="2.1336"/>
<vertex x="1.6764" y="2.3368"/>
<vertex x="2.1336" y="2.794"/>
<vertex x="2.9464" y="2.794"/>
<vertex x="3.4036" y="2.3368"/>
<vertex x="3.4036" y="2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="4.2164" y="2.1336"/>
<vertex x="4.2164" y="2.3368"/>
<vertex x="4.6736" y="2.794"/>
<vertex x="5.4864" y="2.794"/>
<vertex x="5.9436" y="2.3368"/>
<vertex x="5.9436" y="2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="5.9436" y="-2.1336"/>
<vertex x="5.9436" y="-2.3368"/>
<vertex x="5.4864" y="-2.794"/>
<vertex x="4.6736" y="-2.794"/>
<vertex x="4.2164" y="-2.3368"/>
<vertex x="4.2164" y="-2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="3.4036" y="-2.1336"/>
<vertex x="3.4036" y="-2.3368"/>
<vertex x="2.9464" y="-2.794"/>
<vertex x="2.1336" y="-2.794"/>
<vertex x="1.6764" y="-2.3368"/>
<vertex x="1.6764" y="-2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="0.8636" y="-2.1336"/>
<vertex x="0.8636" y="-2.3368"/>
<vertex x="0.4064" y="-2.794"/>
<vertex x="-0.4064" y="-2.794"/>
<vertex x="-0.8636" y="-2.3368"/>
<vertex x="-0.8636" y="-2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.6764" y="-2.1336"/>
<vertex x="-1.6764" y="-2.3368"/>
<vertex x="-2.1336" y="-2.794"/>
<vertex x="-2.9464" y="-2.794"/>
<vertex x="-3.4036" y="-2.3368"/>
<vertex x="-3.4036" y="-2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-4.2164" y="-2.1336"/>
<vertex x="-4.2164" y="-2.3368"/>
<vertex x="-4.6736" y="-2.794"/>
<vertex x="-5.4864" y="-2.794"/>
<vertex x="-5.9436" y="-2.3368"/>
<vertex x="-5.9436" y="-2.1336"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-5.9436" y="0.4064"/>
<vertex x="-5.7404" y="0.2032"/>
<vertex x="-4.4196" y="0.2032"/>
<vertex x="-4.2164" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-5.9436" y="2.1336"/>
<vertex x="-5.9436" y="2.286"/>
<vertex x="-5.4356" y="2.794"/>
<vertex x="-4.7244" y="2.794"/>
<vertex x="-4.2164" y="2.286"/>
<vertex x="-4.2164" y="2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-5.9436" y="0.4064"/>
<vertex x="-5.7912" y="0.254"/>
<vertex x="-5.7404" y="0.2032"/>
<vertex x="-4.4196" y="0.2032"/>
<vertex x="-4.2164" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-3.4036" y="2.1336"/>
<vertex x="-3.4036" y="2.286"/>
<vertex x="-2.8956" y="2.794"/>
<vertex x="-2.1844" y="2.794"/>
<vertex x="-1.6764" y="2.286"/>
<vertex x="-1.6764" y="2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-0.8636" y="2.1336"/>
<vertex x="-0.8636" y="2.286"/>
<vertex x="-0.3556" y="2.794"/>
<vertex x="0.3556" y="2.794"/>
<vertex x="0.8636" y="2.286"/>
<vertex x="0.8636" y="2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="1.6764" y="2.1336"/>
<vertex x="1.6764" y="2.286"/>
<vertex x="2.1844" y="2.794"/>
<vertex x="2.8956" y="2.794"/>
<vertex x="3.4036" y="2.286"/>
<vertex x="3.4036" y="2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="4.2164" y="2.1336"/>
<vertex x="4.2164" y="2.286"/>
<vertex x="4.7244" y="2.794"/>
<vertex x="5.4356" y="2.794"/>
<vertex x="5.9436" y="2.286"/>
<vertex x="5.9436" y="2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="5.9436" y="-2.1336"/>
<vertex x="5.9436" y="-2.286"/>
<vertex x="5.4356" y="-2.794"/>
<vertex x="4.7244" y="-2.794"/>
<vertex x="4.2164" y="-2.286"/>
<vertex x="4.2164" y="-2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="3.4036" y="-2.1336"/>
<vertex x="3.4036" y="-2.286"/>
<vertex x="2.8956" y="-2.794"/>
<vertex x="2.1844" y="-2.794"/>
<vertex x="1.6764" y="-2.286"/>
<vertex x="1.6764" y="-2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="0.8636" y="-2.1336"/>
<vertex x="0.8636" y="-2.286"/>
<vertex x="0.3556" y="-2.794"/>
<vertex x="-0.3556" y="-2.794"/>
<vertex x="-0.8636" y="-2.286"/>
<vertex x="-0.8636" y="-2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-1.6764" y="-2.1336"/>
<vertex x="-1.6764" y="-2.286"/>
<vertex x="-2.1844" y="-2.794"/>
<vertex x="-2.8956" y="-2.794"/>
<vertex x="-3.4036" y="-2.286"/>
<vertex x="-3.4036" y="-2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-4.2164" y="-2.1336"/>
<vertex x="-4.2164" y="-2.286"/>
<vertex x="-4.7244" y="-2.794"/>
<vertex x="-5.4356" y="-2.794"/>
<vertex x="-5.9436" y="-2.286"/>
<vertex x="-5.9436" y="-2.1336"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-3.4036" y="0.4064"/>
<vertex x="-3.2512" y="0.254"/>
<vertex x="-3.2004" y="0.2032"/>
<vertex x="-1.8796" y="0.2032"/>
<vertex x="-1.6764" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-0.8636" y="0.4064"/>
<vertex x="-0.7112" y="0.254"/>
<vertex x="-0.6604" y="0.2032"/>
<vertex x="0.6604" y="0.2032"/>
<vertex x="0.8636" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-4.2164" y="-0.4064"/>
<vertex x="-4.3688" y="-0.254"/>
<vertex x="-4.4196" y="-0.2032"/>
<vertex x="-5.7404" y="-0.2032"/>
<vertex x="-5.9436" y="-0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-1.6764" y="-0.4064"/>
<vertex x="-1.8288" y="-0.254"/>
<vertex x="-1.8796" y="-0.2032"/>
<vertex x="-3.2004" y="-0.2032"/>
<vertex x="-3.4036" y="-0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="0.8636" y="-0.4064"/>
<vertex x="0.7112" y="-0.254"/>
<vertex x="0.6604" y="-0.2032"/>
<vertex x="-0.6604" y="-0.2032"/>
<vertex x="-0.8636" y="-0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="-0.8636" y="0.4064"/>
<vertex x="-0.7112" y="0.254"/>
<vertex x="-0.6604" y="0.2032"/>
<vertex x="0.6604" y="0.2032"/>
<vertex x="0.8636" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="1.6764" y="0.4064"/>
<vertex x="1.8288" y="0.254"/>
<vertex x="1.8796" y="0.2032"/>
<vertex x="3.2004" y="0.2032"/>
<vertex x="3.4036" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="4.2164" y="0.4064"/>
<vertex x="4.3688" y="0.254"/>
<vertex x="4.4196" y="0.2032"/>
<vertex x="5.7404" y="0.2032"/>
<vertex x="5.9436" y="0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="5.9436" y="-0.4064"/>
<vertex x="5.7912" y="-0.254"/>
<vertex x="5.7404" y="-0.2032"/>
<vertex x="4.4196" y="-0.2032"/>
<vertex x="4.2164" y="-0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="5.9436" y="-0.4064"/>
<vertex x="5.7912" y="-0.254"/>
<vertex x="5.7404" y="-0.2032"/>
<vertex x="4.4196" y="-0.2032"/>
<vertex x="4.2164" y="-0.4064"/>
</polygon>
<polygon width="0" layer="30">
<vertex x="3.4036" y="-0.4064"/>
<vertex x="3.2512" y="-0.254"/>
<vertex x="3.2004" y="-0.2032"/>
<vertex x="1.8796" y="-0.2032"/>
<vertex x="1.6764" y="-0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-3.4036" y="0.4064"/>
<vertex x="-3.2004" y="0.2032"/>
<vertex x="-1.8796" y="0.2032"/>
<vertex x="-1.6764" y="0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-0.8636" y="0.4064"/>
<vertex x="-0.6604" y="0.2032"/>
<vertex x="0.6604" y="0.2032"/>
<vertex x="0.8636" y="0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.6764" y="0.4064"/>
<vertex x="1.8796" y="0.2032"/>
<vertex x="3.2004" y="0.2032"/>
<vertex x="3.4036" y="0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="4.2164" y="0.4064"/>
<vertex x="4.4196" y="0.2032"/>
<vertex x="5.7404" y="0.2032"/>
<vertex x="5.9436" y="0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="5.9436" y="-0.4064"/>
<vertex x="5.7404" y="-0.2032"/>
<vertex x="4.4196" y="-0.2032"/>
<vertex x="4.2164" y="-0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="3.4036" y="-0.4064"/>
<vertex x="3.2004" y="-0.2032"/>
<vertex x="1.8796" y="-0.2032"/>
<vertex x="1.6764" y="-0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="0.8636" y="-0.4064"/>
<vertex x="0.6604" y="-0.2032"/>
<vertex x="-0.6604" y="-0.2032"/>
<vertex x="-0.8636" y="-0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.6764" y="-0.4064"/>
<vertex x="-1.8796" y="-0.2032"/>
<vertex x="-3.2004" y="-0.2032"/>
<vertex x="-3.4036" y="-0.4064"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-4.2164" y="-0.4064"/>
<vertex x="-4.4196" y="-0.2032"/>
<vertex x="-5.7404" y="-0.2032"/>
<vertex x="-5.9436" y="-0.4064"/>
</polygon>
</package>
<package name="M3_10">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="15.875" y1="-4.2418" x2="15.875" y2="4.3" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="4.3" x2="-15.875" y2="-4.2418" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="-4.3" x2="-2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.3" x2="-2.54" y2="-3.048" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-4.3" x2="15.621" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="15.875" y1="4.3" x2="-15.875" y2="4.3" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-3" x2="2.54" y2="-3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3" x2="-8.89" y2="-3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="-3" x2="-8.89" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.763" y1="3" x2="-8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="-1.27" x2="-15.748" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-15.748" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="2.54" y2="-4.318" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-3" x2="8.89" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="3" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="15.748" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="8.89" y1="1.27" x2="15.748" y2="1.27" width="0.3048" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="0.8" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="0.8" shape="square"/>
<pad name="3" x="-2.54" y="-1.27" drill="0.8" shape="square"/>
<pad name="4" x="-2.54" y="1.27" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-1.27" drill="0.8" shape="square"/>
<pad name="6" x="0" y="1.27" drill="0.8" shape="square"/>
<pad name="7" x="2.54" y="-1.27" drill="0.8" shape="square"/>
<pad name="8" x="2.54" y="1.27" drill="0.8" shape="square"/>
<pad name="9" x="5.08" y="-1.27" drill="0.8" shape="square"/>
<pad name="10" x="5.08" y="1.27" drill="0.8" shape="square"/>
<text x="-15.24" y="5.08" size="2.54" layer="25">&gt;NAME</text>
<text x="2.54" y="5.08" size="2.54" layer="27">&gt;VALUE</text>
<polygon width="0.3048" layer="21">
<vertex x="-6.477" y="-4.826"/>
<vertex x="-3.683" y="-4.826"/>
<vertex x="-5.08" y="-5.969"/>
</polygon>
</package>
<package name="M3_10L">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<circle x="-10.9982" y="-3.2766" radius="1.9304" width="0" layer="42"/>
<circle x="10.9982" y="-3.2766" radius="1.9304" width="0" layer="42"/>
<circle x="-14.8082" y="-4.5466" radius="1.9304" width="0" layer="41"/>
<circle x="-10.9982" y="-3.2766" radius="1.9304" width="0" layer="41"/>
<circle x="10.9982" y="-3.2766" radius="1.9304" width="0" layer="41"/>
<wire x1="-15.875" y1="-6.0198" x2="-13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="15.875" y1="-6.0198" x2="15.875" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.875" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-6.0198" x2="-13.335" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-6.0198" x2="-8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-13.335" y1="-2.032" x2="-10.9982" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="-10.9982" y1="-0.4572" x2="-8.6614" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-8.6614" y1="-2.032" x2="-8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-8.6614" y1="-6.0198" x2="8.6614" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-6.0198" x2="8.6614" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-2.0574" x2="10.9982" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="10.9982" y1="-0.4572" x2="13.335" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="13.335" y1="-2.0574" x2="13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="8.6614" y1="-6.0198" x2="13.335" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="13.335" y1="-6.0198" x2="15.875" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="15.875" y1="2.54" x2="13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="10.9982" x2="-13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-8.89" y1="8.89" x2="-8.89" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="6.604" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="6.604" x2="-2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="7.874" x2="-2.54" y2="6.604" width="0.3048" layer="21" curve="-90"/>
<wire x1="-8.89" y1="8.89" x2="-7.874" y2="7.874" width="0.3048" layer="21" curve="90"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="6.604" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="3.81" y2="7.874" width="0.3048" layer="21" curve="-90"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="10.9982" x2="13.0048" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-7.874" y1="7.874" x2="7.874" y2="7.874" width="0.3048" layer="21"/>
<wire x1="8.89" y1="8.89" x2="8.89" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="7.874" y1="7.874" x2="8.89" y2="8.89" width="0.3048" layer="21" curve="90"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="7.62" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-5.08" drill="0.8" shape="square"/>
<pad name="2" x="-5.08" y="-2.54" drill="0.8" shape="square"/>
<pad name="3" x="-2.54" y="-5.08" drill="0.8" shape="square"/>
<pad name="4" x="-2.54" y="-2.54" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="7" x="2.54" y="-5.08" drill="0.8" shape="square"/>
<pad name="8" x="2.54" y="-2.54" drill="0.8" shape="square"/>
<pad name="9" x="5.08" y="-5.08" drill="0.8" shape="square"/>
<pad name="10" x="5.08" y="-2.54" drill="0.8" shape="square"/>
<text x="-15.24" y="-10.16" size="2.54" layer="25">&gt;NAME</text>
<text x="2.54" y="-10.16" size="2.54" layer="27">&gt;VALUE</text>
<hole x="-10.9982" y="-3.2766" drill="2.54"/>
<hole x="10.9982" y="-3.2766" drill="2.54"/>
</package>
<package name="ML10L">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-6.35" y1="10.16" x2="-3.81" y2="10.16" width="0.254" layer="21"/>
<wire x1="-3.81" y1="10.16" x2="-5.08" y2="7.62" width="0.254" layer="21"/>
<wire x1="-5.08" y1="7.62" x2="-6.35" y2="10.16" width="0.254" layer="21"/>
<wire x1="2.794" y1="9.906" x2="2.794" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.794" y1="9.906" x2="4.826" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.826" y1="10.922" x2="4.826" y2="9.906" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.683" x2="3.048" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.048" y1="5.969" x2="4.572" y2="5.969" width="0.1524" layer="21" curve="-180"/>
<wire x1="4.572" y1="5.969" x2="4.572" y2="4.445" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.683" x2="5.969" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="2.159" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.429" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="4.445" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="2.159" y2="3.683" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.683" x2="2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.429" x2="2.159" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.048" y1="5.969" x2="3.048" y2="4.445" width="0.1524" layer="21"/>
<wire x1="3.048" y1="4.445" x2="3.048" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.048" y1="4.445" x2="4.572" y2="4.445" width="0.1524" layer="21"/>
<wire x1="4.572" y1="4.445" x2="4.572" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.969" y1="3.683" x2="5.969" y2="10.922" width="0.1524" layer="21"/>
<wire x1="5.969" y1="3.683" x2="5.969" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.032" x2="-4.445" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="2.032" x2="-3.175" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.032" x2="-1.905" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="2.032" x2="-0.635" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.032" x2="0.635" y2="2.032" width="0.1524" layer="51"/>
<wire x1="0.635" y1="2.032" x2="1.905" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.032" x2="2.159" y2="2.032" width="0.1524" layer="51"/>
<wire x1="3.175" y1="2.032" x2="4.445" y2="2.032" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.032" x2="4.445" y2="2.032" width="0.1524" layer="51"/>
<wire x1="0" y1="10.033" x2="0" y2="10.287" width="0.508" layer="21"/>
<wire x1="6.731" y1="4.445" x2="9.525" y2="4.445" width="0.1524" layer="21"/>
<wire x1="9.525" y1="4.445" x2="9.525" y2="8.255" width="0.1524" layer="21"/>
<wire x1="6.731" y1="8.255" x2="9.525" y2="8.255" width="0.1524" layer="21"/>
<wire x1="6.731" y1="8.255" x2="6.731" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="10.16" y2="10.922" width="0.1524" layer="21"/>
<wire x1="10.16" y1="10.922" x2="10.16" y2="2.032" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.032" x2="9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="-10.16" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.032" x2="-10.16" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="5.969" y1="2.032" x2="5.715" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.159" x2="2.159" y2="2.032" width="0.1524" layer="51"/>
<wire x1="2.159" y1="2.032" x2="3.175" y2="2.032" width="0.1524" layer="51"/>
<wire x1="8.001" y1="2.032" x2="8.001" y2="1.397" width="0.1524" layer="21"/>
<wire x1="9.017" y1="1.397" x2="8.001" y2="1.397" width="0.1524" layer="21"/>
<wire x1="9.017" y1="1.397" x2="9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="8.001" y1="2.032" x2="5.969" y2="2.032" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="8.001" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="2.032" x2="-9.017" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="2.032" x2="-8.001" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="1.397" x2="-9.017" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="1.397" x2="-8.001" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="2.032" x2="-5.715" y2="2.032" width="0.1524" layer="21"/>
<rectangle x1="-0.254" y1="4.445" x2="0.254" y2="10.287" layer="21"/>
<rectangle x1="-6.223" y1="9.652" x2="-3.937" y2="10.16" layer="21"/>
<rectangle x1="-5.969" y1="9.144" x2="-4.191" y2="9.652" layer="21"/>
<rectangle x1="-5.715" y1="8.636" x2="-4.445" y2="9.144" layer="21"/>
<rectangle x1="-5.461" y1="8.128" x2="-4.699" y2="8.636" layer="21"/>
<rectangle x1="-5.207" y1="7.874" x2="-4.953" y2="8.128" layer="21"/>
<rectangle x1="-5.334" y1="-0.381" x2="-4.826" y2="0.381" layer="21"/>
<rectangle x1="-5.334" y1="0.381" x2="-4.826" y2="2.032" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-0.381" layer="51"/>
<rectangle x1="-2.794" y1="0.381" x2="-2.286" y2="2.032" layer="51"/>
<rectangle x1="-2.794" y1="-0.381" x2="-2.286" y2="0.381" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-0.381" layer="51"/>
<rectangle x1="-0.254" y1="0.381" x2="0.254" y2="2.032" layer="51"/>
<rectangle x1="-0.254" y1="-0.381" x2="0.254" y2="0.381" layer="21"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-0.381" layer="51"/>
<rectangle x1="2.286" y1="0.381" x2="2.794" y2="2.032" layer="51"/>
<rectangle x1="2.286" y1="-0.381" x2="2.794" y2="0.381" layer="21"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-0.381" layer="51"/>
<rectangle x1="4.826" y1="0.381" x2="5.334" y2="2.032" layer="51"/>
<rectangle x1="4.826" y1="-0.381" x2="5.334" y2="0.381" layer="21"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-0.381" layer="51"/>
<pad name="1" x="-5.08" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="0.9" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="0.9" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="0.9" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="0.9" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="0.9" shape="octagon"/>
<text x="-7.1628" y="-1.6764" size="1.27" layer="21" ratio="10">1</text>
<text x="-7.1882" y="0.3556" size="1.27" layer="21" ratio="10">2</text>
<text x="-10.1854" y="11.43" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0254" y="11.43" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="8.89" y="5.08" size="1.524" layer="21" ratio="10" rot="R90">10</text>
</package>
<package name="TASTER_SMD">
<circle x="0" y="0" radius="1.778" width="0.1524" layer="21"/>
<circle x="-2.159" y="-2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="-2.032" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="-2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.635" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.254" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.048" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="0.762" x2="-3.048" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="1.27" y2="3.048" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="-1.27" y2="-2.794" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="1.143" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-3.048" x2="-1.27" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.159" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="1.143" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.159" x2="1.27" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.27" y1="2.286" x2="-1.27" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.413" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="0.508" x2="-2.159" y2="-0.381" width="0.1524" layer="51"/>
<smd name="1" x="-4.9875" y="2.2028" dx="2.4" dy="3.4" layer="1" rot="R90"/>
<smd name="2" x="4.9889" y="2.2018" dx="2.4" dy="3.4" layer="1" rot="R90"/>
<smd name="3" x="-4.9889" y="-2.193" dx="2.4" dy="3.4" layer="1" rot="R90"/>
<smd name="4" x="4.9999" y="-2.1971" dx="2.4" dy="3.4" layer="1" rot="R90"/>
<text x="-3.048" y="3.683" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.048" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.318" y="1.651" size="1.27" layer="51" ratio="10">1</text>
<text x="3.556" y="1.524" size="1.27" layer="51" ratio="10">2</text>
<text x="-4.572" y="-2.794" size="1.27" layer="51" ratio="10">3</text>
<text x="3.556" y="-2.794" size="1.27" layer="51" ratio="10">4</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="10P">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TASTER_SMD">
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="-0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ML10" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="10P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3M" package="M3_10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3ML" package="M3_10L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L" package="ML10L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TASTER_SMD" prefix="S">
<description>Taster SMD</description>
<gates>
<gate name="G$1" symbol="TASTER_SMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TASTER_SMD">
<connects>
<connect gate="G$1" pin="P" pad="3"/>
<connect gate="G$1" pin="P1" pad="4"/>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$1" pin="S1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rpi_hat">
<description>&lt;h1&gt;Raspberry Pi HAT&lt;/h1&gt;
by &lt;i&gt;chris@hobbyelektronik.org&lt;/i&gt;
&lt;br /&gt;
Version 0.1
&lt;br /&gt;
based on the official Raspberry Pi HAT specification from https://github.com/raspberrypi/hats with small deviations.
&lt;br /&gt;
interfaces tested so far: Power, UART, ID-I2C</description>
<packages>
<package name="RPI_3BP_THT">
<pad name="1" x="-24.13" y="-1.27" drill="1" shape="square"/>
<pad name="2" x="-24.13" y="1.27" drill="1"/>
<pad name="3" x="-21.59" y="-1.27" drill="1"/>
<pad name="4" x="-21.59" y="1.27" drill="1"/>
<pad name="5" x="-19.05" y="-1.27" drill="1"/>
<pad name="6" x="-19.05" y="1.27" drill="1"/>
<pad name="7" x="-16.51" y="-1.27" drill="1"/>
<pad name="8" x="-16.51" y="1.27" drill="1"/>
<pad name="9" x="-13.97" y="-1.27" drill="1"/>
<pad name="10" x="-13.97" y="1.27" drill="1"/>
<pad name="11" x="-11.43" y="-1.27" drill="1"/>
<pad name="12" x="-11.43" y="1.27" drill="1"/>
<pad name="13" x="-8.89" y="-1.27" drill="1"/>
<pad name="14" x="-8.89" y="1.27" drill="1"/>
<pad name="15" x="-6.35" y="-1.27" drill="1"/>
<pad name="16" x="-6.35" y="1.27" drill="1"/>
<pad name="17" x="-3.81" y="-1.27" drill="1"/>
<pad name="18" x="-3.81" y="1.27" drill="1"/>
<pad name="19" x="-1.27" y="-1.27" drill="1"/>
<pad name="20" x="-1.27" y="1.27" drill="1"/>
<pad name="21" x="1.27" y="-1.27" drill="1"/>
<pad name="22" x="1.27" y="1.27" drill="1"/>
<pad name="23" x="3.81" y="-1.27" drill="1"/>
<pad name="24" x="3.81" y="1.27" drill="1"/>
<pad name="25" x="6.35" y="-1.27" drill="1"/>
<pad name="26" x="6.35" y="1.27" drill="1"/>
<pad name="27" x="8.89" y="-1.27" drill="1"/>
<pad name="28" x="8.89" y="1.27" drill="1"/>
<pad name="29" x="11.43" y="-1.27" drill="1"/>
<pad name="30" x="11.43" y="1.27" drill="1"/>
<pad name="31" x="13.97" y="-1.27" drill="1"/>
<pad name="32" x="13.97" y="1.27" drill="1"/>
<pad name="33" x="16.51" y="-1.27" drill="1"/>
<pad name="34" x="16.51" y="1.27" drill="1"/>
<pad name="35" x="19.05" y="-1.27" drill="1"/>
<pad name="36" x="19.05" y="1.27" drill="1"/>
<pad name="37" x="21.59" y="-1.27" drill="1"/>
<pad name="38" x="21.59" y="1.27" drill="1"/>
<pad name="39" x="24.13" y="-1.27" drill="1"/>
<pad name="40" x="24.13" y="1.27" drill="1"/>
<wire x1="-25.4" y1="2.54" x2="-25.4" y2="0" width="0.127" layer="22"/>
<wire x1="-25.4" y1="0" x2="-25.4" y2="-2.54" width="0.127" layer="22"/>
<wire x1="-25.4" y1="-2.54" x2="-22.86" y2="-2.54" width="0.127" layer="22"/>
<wire x1="-22.86" y1="-2.54" x2="25.4" y2="-2.54" width="0.127" layer="22"/>
<wire x1="25.4" y1="-2.54" x2="25.4" y2="2.54" width="0.127" layer="22"/>
<wire x1="25.4" y1="2.54" x2="-25.4" y2="2.54" width="0.127" layer="22"/>
<wire x1="-25.4" y1="0" x2="-22.86" y2="0" width="0.127" layer="22"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="-2.54" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="RPI_40">
<pin name="3V3@1" x="-15.24" y="22.86" length="short" direction="pwr"/>
<pin name="GPIO2@3" x="-15.24" y="20.32" length="short"/>
<pin name="GPIO3@5" x="-15.24" y="17.78" length="short"/>
<pin name="GPIO4@7" x="-15.24" y="15.24" length="short"/>
<pin name="GND@9" x="-15.24" y="12.7" length="short" direction="pwr"/>
<pin name="GPIO17@11" x="-15.24" y="10.16" length="short"/>
<pin name="GPIO27@13" x="-15.24" y="7.62" length="short"/>
<pin name="GPIO22@15" x="-15.24" y="5.08" length="short"/>
<pin name="3V3@17" x="-15.24" y="2.54" length="short" direction="pwr"/>
<pin name="GPIO10@19" x="-15.24" y="0" length="short"/>
<pin name="GPIO9@21" x="-15.24" y="-2.54" length="short"/>
<pin name="GPIO11@23" x="-15.24" y="-5.08" length="short"/>
<pin name="GND@25" x="-15.24" y="-7.62" length="short" direction="pwr"/>
<pin name="ID_SD@27" x="-15.24" y="-10.16" length="short"/>
<pin name="GPIO5@29" x="-15.24" y="-12.7" length="short"/>
<pin name="GPIO6@31" x="-15.24" y="-15.24" length="short"/>
<pin name="GPIO13@33" x="-15.24" y="-17.78" length="short"/>
<pin name="GPIO19@35" x="-15.24" y="-20.32" length="short"/>
<pin name="GPIO26@37" x="-15.24" y="-22.86" length="short"/>
<pin name="GND@39" x="-15.24" y="-25.4" length="short" direction="pwr"/>
<pin name="5V0@2" x="15.24" y="22.86" length="short" direction="pwr" rot="R180"/>
<pin name="5V0@4" x="15.24" y="20.32" length="short" direction="pwr" rot="R180"/>
<pin name="GND@6" x="15.24" y="17.78" length="short" direction="pwr" rot="R180"/>
<pin name="GPIO14@8" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="GPIO15@10" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="GPIO18@12" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="GND@14" x="15.24" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="GPIO23@16" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="GPIO24@18" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="GND@20" x="15.24" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="GPIO25@22" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="GPIO8@24" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="GPIO7@26" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="ID_SC@28" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="GND@30" x="15.24" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GPIO12@32" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="GND@34" x="15.24" y="-17.78" length="short" direction="pwr" rot="R180"/>
<pin name="GPIO16@36" x="15.24" y="-20.32" length="short" rot="R180"/>
<pin name="GPIO20@38" x="15.24" y="-22.86" length="short" rot="R180"/>
<pin name="GPIO21@40" x="15.24" y="-25.4" length="short" rot="R180"/>
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="25.908" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.7" y="-29.718" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RPI" prefix="X">
<gates>
<gate name="G$1" symbol="RPI_40" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RPI_3BP_THT">
<connects>
<connect gate="G$1" pin="3V3@1" pad="1"/>
<connect gate="G$1" pin="3V3@17" pad="17"/>
<connect gate="G$1" pin="5V0@2" pad="2"/>
<connect gate="G$1" pin="5V0@4" pad="4"/>
<connect gate="G$1" pin="GND@14" pad="14"/>
<connect gate="G$1" pin="GND@20" pad="20"/>
<connect gate="G$1" pin="GND@25" pad="25"/>
<connect gate="G$1" pin="GND@30" pad="30"/>
<connect gate="G$1" pin="GND@34" pad="34"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="GPIO10@19" pad="19"/>
<connect gate="G$1" pin="GPIO11@23" pad="23"/>
<connect gate="G$1" pin="GPIO12@32" pad="32"/>
<connect gate="G$1" pin="GPIO13@33" pad="33"/>
<connect gate="G$1" pin="GPIO14@8" pad="8"/>
<connect gate="G$1" pin="GPIO15@10" pad="10"/>
<connect gate="G$1" pin="GPIO16@36" pad="36"/>
<connect gate="G$1" pin="GPIO17@11" pad="11"/>
<connect gate="G$1" pin="GPIO18@12" pad="12"/>
<connect gate="G$1" pin="GPIO19@35" pad="35"/>
<connect gate="G$1" pin="GPIO20@38" pad="38"/>
<connect gate="G$1" pin="GPIO21@40" pad="40"/>
<connect gate="G$1" pin="GPIO22@15" pad="15"/>
<connect gate="G$1" pin="GPIO23@16" pad="16"/>
<connect gate="G$1" pin="GPIO24@18" pad="18"/>
<connect gate="G$1" pin="GPIO25@22" pad="22"/>
<connect gate="G$1" pin="GPIO26@37" pad="37"/>
<connect gate="G$1" pin="GPIO27@13" pad="13"/>
<connect gate="G$1" pin="GPIO2@3" pad="3"/>
<connect gate="G$1" pin="GPIO3@5" pad="5"/>
<connect gate="G$1" pin="GPIO4@7" pad="7"/>
<connect gate="G$1" pin="GPIO5@29" pad="29"/>
<connect gate="G$1" pin="GPIO6@31" pad="31"/>
<connect gate="G$1" pin="GPIO7@26" pad="26"/>
<connect gate="G$1" pin="GPIO8@24" pad="24"/>
<connect gate="G$1" pin="GPIO9@21" pad="21"/>
<connect gate="G$1" pin="ID_SC@28" pad="28"/>
<connect gate="G$1" pin="ID_SD@27" pad="27"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="0_HTL_analog_old">
<description>&lt;b&gt;Analoge Bautele für den FTKL Unterricht (HTL-Rankweil Version 1.0 110121)&lt;/b&gt;&lt;p&gt;
Zusammengestellt für den FTKL Unterricht:
&lt;ul&gt;
&lt;li&gt;Grundelemente (GND, Vcc, ...
&lt;li&gt;Widerstände
&lt;li&gt;Potentiometer
&lt;li&gt;Kondensatoren
&lt;li&gt;Elko
&lt;li&gt;Spulen
&lt;li&gt;Quarz
&lt;li&gt;Gleichrichter
&lt;li&gt;Dioden
&lt;li&gt;Transitoren
&lt;li&gt;LED
&lt;/ul&gt;
&lt;author&gt;www.HTL-Rankweil.at&lt;/author&gt;&lt;p&gt;
leopold.moosbrugger@htlr.snv.at</description>
<packages>
<package name="LED3MM">
<description>&lt;b&gt;Leuchtdiode&lt;/b&gt; &lt;p&gt;
Durchmesser 3 mm&lt;p&gt;
RM 2,54 mm</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.930333" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.261761" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.778"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.778"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;b&gt;Leuchtdiode&lt;/b&gt; &lt;p&gt;
Durchmesser 5 mm&lt;p&gt;
RM 2,54 mm</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.778"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.778"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622" cap="flat"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622" cap="flat"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="1206H">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size&lt;br&gt;
with Hole</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
<hole x="0" y="0" drill="2.6"/>
<circle x="0" y="0" radius="0.55" width="0.127" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="-1.27" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.572" y1="-0.762" x2="-5.969" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-5.842" y2="-3.302" width="0.1524" layer="94"/>
<text x="1.016" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.175" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="-2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="-2.54" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-5.969" y="-2.159"/>
<vertex x="-5.588" y="-1.27"/>
<vertex x="-5.08" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-5.842" y="-3.302"/>
<vertex x="-5.461" y="-2.413"/>
<vertex x="-4.953" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED*" prefix="D" uservalue="yes">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206H" package="1206H">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="BRD_SHEET_NAME" value="Schaltplan"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND1" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND2" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND4" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="IC1" library="microchip" deviceset="MCP23017" device="SP"/>
<part name="IC2" library="microchip" deviceset="MCP23017" device="SP"/>
<part name="IC3" library="microchip" deviceset="MCP23017" device="SP"/>
<part name="T2" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="R3" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R4" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="T1" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="R1" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R2" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="T3" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="R5" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R6" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="T4" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="R7" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R8" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="X3" library="0_HTL_MegaCard_V3" deviceset="ML10" device=""/>
<part name="X2" library="0_HTL_uC-MEGACARD" deviceset="20POL" device=""/>
<part name="X1" library="0_HTL_uC-MEGACARD" deviceset="20POL" device=""/>
<part name="GND6" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="PFUSE" library="0_HTL_uC-MEGACARD" deviceset="PTC-1206" device="" value="200mA"/>
<part name="GND7" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="S1" library="0_HTL_MegaCard_V3" deviceset="TASTER_SMD" device=""/>
<part name="S2" library="0_HTL_MegaCard_V3" deviceset="TASTER_SMD" device=""/>
<part name="R10" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R11" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="1k"/>
<part name="R12" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="1k"/>
<part name="R13" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="GND8" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND9" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND5" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND3" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="RPI" library="rpi_hat" deviceset="RPI" device=""/>
<part name="X4" library="0_HTL_uC-MEGACARD" deviceset="ML6" device=""/>
<part name="GPIO" library="0_HTL_uC-MEGACARD" deviceset="PIN10" device=""/>
<part name="GND10" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND" library="0_HTL_uC-MEGACARD" deviceset="PIN2" device="&quot;"/>
<part name="VCC" library="0_HTL_uC-MEGACARD" deviceset="PIN2" device="&quot;"/>
<part name="GND11" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="VCCPROT" library="0_HTL_uC-MEGACARD" deviceset="PIN2" device="&quot;"/>
<part name="ON" library="0_HTL_analog_old" deviceset="LED*" device="1206"/>
<part name="5VPROT" library="0_HTL_analog_old" deviceset="LED*" device="1206"/>
<part name="R9" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="1k5"/>
<part name="GND12" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND13" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="R15" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R16" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R17" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="R18" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="10k"/>
<part name="T5" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="T6" library="BSS138" deviceset="BSS138BKW" device=""/>
<part name="R14" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="1k2"/>
<part name="R19" library="0_HTL_MegaCard_V3" deviceset="R" device="-R0805" value="330"/>
<part name="GND14" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND15" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GND16" library="0_HTL_analog" deviceset="GND" device=""/>
<part name="GPIO1" library="0_HTL_uC-MEGACARD" deviceset="PIN10" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="154.94" y="60.96" size="1.778" layer="100">0100 000</text>
<text x="154.94" y="2.54" size="1.778" layer="100">0100 001</text>
<text x="157.48" y="-78.74" size="1.778" layer="100">0100 011</text>
<text x="287.02" y="12.7" size="1.778" layer="100">LS</text>
<text x="314.96" y="12.7" size="1.778" layer="100">HS</text>
<text x="287.02" y="50.8" size="1.778" layer="100">LS</text>
<text x="314.96" y="50.8" size="1.778" layer="100">HS</text>
<text x="287.02" y="-25.4" size="1.778" layer="100">LS</text>
<text x="314.96" y="-25.4" size="1.778" layer="100">HS</text>
<text x="289.56" y="-63.5" size="1.778" layer="100">LS</text>
<text x="317.5" y="-63.5" size="1.778" layer="100">HS</text>
</plain>
<instances>
<instance part="GND1" gate="1" x="165.1" y="45.72"/>
<instance part="GND2" gate="1" x="165.1" y="-15.24"/>
<instance part="GND4" gate="1" x="162.56" y="-93.98"/>
<instance part="IC1" gate="G$1" x="185.42" y="76.2"/>
<instance part="IC2" gate="G$1" x="182.88" y="12.7"/>
<instance part="IC3" gate="G$1" x="185.42" y="-68.58"/>
<instance part="T2" gate="G$1" x="302.26" y="15.24" rot="R270"/>
<instance part="R3" gate="G$1" x="292.1" y="20.32" rot="R90"/>
<instance part="R4" gate="G$1" x="309.88" y="20.32" rot="R90"/>
<instance part="T1" gate="G$1" x="302.26" y="53.34" rot="R270"/>
<instance part="R1" gate="G$1" x="292.1" y="58.42" rot="R90"/>
<instance part="R2" gate="G$1" x="309.88" y="58.42" rot="R90"/>
<instance part="T3" gate="G$1" x="302.26" y="-22.86" rot="R270"/>
<instance part="R5" gate="G$1" x="292.1" y="-17.78" rot="R90"/>
<instance part="R6" gate="G$1" x="309.88" y="-17.78" rot="R90"/>
<instance part="T4" gate="G$1" x="304.8" y="-60.96" rot="R270"/>
<instance part="R7" gate="G$1" x="294.64" y="-55.88" rot="R90"/>
<instance part="R8" gate="G$1" x="312.42" y="-55.88" rot="R90"/>
<instance part="X3" gate="G$1" x="223.52" y="-76.2" rot="R180"/>
<instance part="X2" gate="G$1" x="246.38" y="55.88"/>
<instance part="X1" gate="G$1" x="243.84" y="-7.62"/>
<instance part="GND6" gate="1" x="360.68" y="5.08"/>
<instance part="PFUSE" gate="G$1" x="86.36" y="60.96"/>
<instance part="GND7" gate="1" x="236.22" y="-86.36"/>
<instance part="S1" gate="G$1" x="129.54" y="20.32" rot="R270"/>
<instance part="S2" gate="G$1" x="129.54" y="7.62" rot="R270"/>
<instance part="R10" gate="G$1" x="119.38" y="27.94" rot="R90"/>
<instance part="R11" gate="G$1" x="99.06" y="20.32" rot="R180"/>
<instance part="R12" gate="G$1" x="99.06" y="12.7" rot="R180"/>
<instance part="R13" gate="G$1" x="109.22" y="27.94" rot="R90"/>
<instance part="GND8" gate="1" x="142.24" y="2.54"/>
<instance part="GND9" gate="1" x="142.24" y="15.24"/>
<instance part="GND5" gate="1" x="215.9" y="-22.86"/>
<instance part="GND3" gate="1" x="215.9" y="38.1"/>
<instance part="RPI" gate="G$1" x="35.56" y="38.1"/>
<instance part="X4" gate="1" x="360.68" y="-5.08"/>
<instance part="GPIO" gate="G$1" x="228.6" y="-40.64" rot="R270"/>
<instance part="GND10" gate="1" x="226.06" y="-66.04"/>
<instance part="GND" gate="G$2" x="116.84" y="-33.02"/>
<instance part="VCC" gate="G$2" x="116.84" y="-58.42"/>
<instance part="GND11" gate="1" x="116.84" y="-40.64"/>
<instance part="VCCPROT" gate="G$2" x="134.62" y="-58.42"/>
<instance part="ON" gate="G$1" x="93.98" y="38.1"/>
<instance part="5VPROT" gate="G$1" x="104.14" y="38.1"/>
<instance part="R9" gate="G$1" x="91.44" y="48.26" rot="R90"/>
<instance part="GND12" gate="1" x="91.44" y="27.94"/>
<instance part="GND13" gate="1" x="101.6" y="27.94"/>
<instance part="R15" gate="G$1" x="-25.4" y="66.04" rot="R90"/>
<instance part="R16" gate="G$1" x="2.54" y="66.04" rot="R90"/>
<instance part="R17" gate="G$1" x="-25.4" y="38.1" rot="R90"/>
<instance part="R18" gate="G$1" x="2.54" y="38.1" rot="R90"/>
<instance part="T5" gate="G$1" x="-12.7" y="63.5" rot="R270"/>
<instance part="T6" gate="G$1" x="-12.7" y="35.56" rot="R270"/>
<instance part="R14" gate="G$1" x="101.6" y="48.26" rot="R90"/>
<instance part="R19" gate="G$1" x="101.6" y="60.96" rot="R90"/>
<instance part="GND14" gate="1" x="12.7" y="5.08"/>
<instance part="GND15" gate="1" x="12.7" y="22.86"/>
<instance part="GND16" gate="1" x="10.16" y="45.72"/>
<instance part="GPIO1" gate="G$1" x="50.8" y="-12.7" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="309.88" y1="25.4" x2="309.88" y2="27.94" width="0.1524" layer="91"/>
<label x="309.88" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="309.88" y1="63.5" x2="309.88" y2="66.04" width="0.1524" layer="91"/>
<label x="309.88" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="309.88" y1="-12.7" x2="309.88" y2="-10.16" width="0.1524" layer="91"/>
<label x="309.88" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="312.42" y1="-50.8" x2="312.42" y2="-48.26" width="0.1524" layer="91"/>
<label x="312.42" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<label x="50.8" y="60.96" size="1.778" layer="95"/>
<pinref part="PFUSE" gate="G$1" pin="1"/>
<pinref part="RPI" gate="G$1" pin="5V0@2"/>
<wire x1="58.42" y1="60.96" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="60.96" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<junction x="53.34" y="60.96"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="58.42" y1="53.34" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
<pinref part="RPI" gate="G$1" pin="5V0@4"/>
<wire x1="50.8" y1="58.42" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<junction x="50.8" y="60.96"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD"/>
<label x="165.1" y="99.06" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="!RESET"/>
<wire x1="172.72" y1="91.44" x2="165.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="96.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="96.52" x2="165.1" y2="96.52" width="0.1524" layer="91"/>
<wire x1="165.1" y1="96.52" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<junction x="165.1" y="96.52"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="A0"/>
<wire x1="170.18" y1="2.54" x2="167.64" y2="2.54" width="0.1524" layer="91"/>
<wire x1="167.64" y1="2.54" x2="167.64" y2="5.08" width="0.1524" layer="91"/>
<label x="167.64" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="172.72" y1="-53.34" x2="160.02" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-53.34" x2="160.02" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VDD"/>
<wire x1="160.02" y1="-48.26" x2="172.72" y2="-48.26" width="0.1524" layer="91"/>
<label x="160.02" y="-45.72" size="1.778" layer="95"/>
<pinref part="IC3" gate="G$1" pin="!RESET"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="A1"/>
<wire x1="172.72" y1="-81.28" x2="167.64" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-81.28" x2="167.64" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="A0"/>
<wire x1="167.64" y1="-78.74" x2="172.72" y2="-78.74" width="0.1524" layer="91"/>
<label x="167.64" y="-78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="!RESET"/>
<wire x1="165.1" y1="27.94" x2="170.18" y2="27.94" width="0.1524" layer="91"/>
<label x="165.1" y="27.94" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<wire x1="170.18" y1="33.02" x2="165.1" y2="33.02" width="0.1524" layer="91"/>
<label x="165.1" y="33.02" size="1.778" layer="95"/>
<wire x1="165.1" y1="27.94" x2="165.1" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-40.64" x2="226.06" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-40.64" x2="226.06" y2="-35.56" width="0.1524" layer="91"/>
<label x="226.06" y="-35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="VCC" gate="G$2" pin="KL1"/>
<wire x1="116.84" y1="-60.96" x2="116.84" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-60.96" x2="119.38" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-60.96" x2="124.46" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-60.96" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="VCC" gate="G$2" pin="KL2"/>
<wire x1="119.38" y1="-58.42" x2="119.38" y2="-60.96" width="0.1524" layer="91"/>
<junction x="119.38" y="-60.96"/>
<label x="124.46" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="2.54" y1="71.12" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<label x="2.54" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="2.54" y1="43.18" x2="2.54" y2="48.26" width="0.1524" layer="91"/>
<label x="2.54" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A0"/>
<wire x1="172.72" y1="66.04" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="165.1" y1="66.04" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A2"/>
<wire x1="165.1" y1="63.5" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="48.26" width="0.1524" layer="91"/>
<wire x1="172.72" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<junction x="165.1" y="60.96"/>
<pinref part="IC1" gate="G$1" pin="A1"/>
<wire x1="172.72" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<junction x="165.1" y="63.5"/>
<pinref part="IC1" gate="G$1" pin="VSS"/>
<wire x1="172.72" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<junction x="165.1" y="55.88"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="A1"/>
<wire x1="170.18" y1="0" x2="165.1" y2="0" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="165.1" y1="0" x2="165.1" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VSS"/>
<wire x1="165.1" y1="-2.54" x2="165.1" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-7.62" x2="165.1" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-7.62" x2="165.1" y2="-7.62" width="0.1524" layer="91"/>
<junction x="165.1" y="-7.62"/>
<pinref part="IC2" gate="G$1" pin="A2"/>
<wire x1="170.18" y1="-2.54" x2="165.1" y2="-2.54" width="0.1524" layer="91"/>
<junction x="165.1" y="-2.54"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VSS"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="172.72" y1="-88.9" x2="162.56" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-88.9" x2="162.56" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="A2"/>
<wire x1="172.72" y1="-83.82" x2="162.56" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-83.82" x2="162.56" y2="-88.9" width="0.1524" layer="91"/>
<junction x="162.56" y="-88.9"/>
</segment>
<segment>
<wire x1="236.22" y1="-83.82" x2="236.22" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="X3" gate="G$1" pin="10"/>
<wire x1="231.14" y1="-81.28" x2="236.22" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="S"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="134.62" y1="7.62" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
<wire x1="142.24" y1="7.62" x2="142.24" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="S"/>
<wire x1="134.62" y1="20.32" x2="142.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="142.24" y1="20.32" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="P$20"/>
<wire x1="215.9" y1="-10.16" x2="215.9" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-17.78" x2="233.68" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-17.78" x2="215.9" y2="-20.32" width="0.1524" layer="91"/>
<junction x="215.9" y="-17.78"/>
<pinref part="X1" gate="G$1" pin="P$19"/>
<wire x1="233.68" y1="-10.16" x2="228.6" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-17.78" x2="233.68" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="P$20"/>
<wire x1="218.44" y1="53.34" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="215.9" y1="53.34" x2="215.9" y2="43.18" width="0.1524" layer="91"/>
<wire x1="215.9" y1="43.18" x2="233.68" y2="43.18" width="0.1524" layer="91"/>
<wire x1="233.68" y1="43.18" x2="233.68" y2="53.34" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P$19"/>
<wire x1="233.68" y1="53.34" x2="231.14" y2="53.34" width="0.1524" layer="91"/>
<wire x1="215.9" y1="43.18" x2="215.9" y2="40.64" width="0.1524" layer="91"/>
<junction x="215.9" y="43.18"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="10"/>
<wire x1="228.6" y1="-63.5" x2="226.06" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND" gate="G$2" pin="KL1"/>
<wire x1="116.84" y1="-33.02" x2="116.84" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="GND" gate="G$2" pin="KL2"/>
<wire x1="119.38" y1="-33.02" x2="119.38" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-38.1" x2="116.84" y2="-38.1" width="0.1524" layer="91"/>
<junction x="116.84" y="-38.1"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="ON" gate="G$1" pin="C"/>
<wire x1="91.44" y1="30.48" x2="91.44" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RPI" gate="G$1" pin="GND@30"/>
<wire x1="50.8" y1="25.4" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="55.88" y1="25.4" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<wire x1="55.88" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<junction x="91.44" y="30.48"/>
<pinref part="RPI" gate="G$1" pin="GND@6"/>
<wire x1="88.9" y1="30.48" x2="91.44" y2="30.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="55.88" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<wire x1="55.88" y1="55.88" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<junction x="88.9" y="30.48"/>
<pinref part="RPI" gate="G$1" pin="GND@20"/>
<wire x1="50.8" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<junction x="55.88" y="38.1"/>
<pinref part="RPI" gate="G$1" pin="GND@14"/>
<wire x1="50.8" y1="45.72" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<junction x="55.88" y="45.72"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="5VPROT" gate="G$1" pin="C"/>
<wire x1="101.6" y1="30.48" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RPI" gate="G$1" pin="GND@39"/>
<wire x1="20.32" y1="12.7" x2="12.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="RPI" gate="G$1" pin="GND@25"/>
<wire x1="20.32" y1="30.48" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="RPI" gate="G$1" pin="GND@9"/>
<wire x1="20.32" y1="50.8" x2="10.16" y2="50.8" width="0.1524" layer="91"/>
<wire x1="10.16" y1="50.8" x2="10.16" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-12.7" x2="50.8" y2="2.54" width="0.1524" layer="91"/>
<label x="50.8" y="5.08" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="6"/>
<wire x1="353.06" y1="-2.54" x2="353.06" y2="10.16" width="0.1524" layer="91"/>
<wire x1="353.06" y1="10.16" x2="360.68" y2="10.16" width="0.1524" layer="91"/>
<wire x1="360.68" y1="10.16" x2="360.68" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="7.62" y1="58.42" x2="20.32" y2="58.42" width="0.1524" layer="91"/>
<label x="7.62" y="58.42" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="GPIO2@3"/>
</segment>
<segment>
<pinref part="T5" gate="G$1" pin="S"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="60.96" x2="-25.4" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="60.96" x2="-33.02" y2="60.96" width="0.1524" layer="91"/>
<junction x="-25.4" y="60.96"/>
<label x="-35.56" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<wire x1="7.62" y1="55.88" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<label x="7.62" y="55.88" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="GPIO3@5"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="T6" gate="G$1" pin="S"/>
<wire x1="-25.4" y1="33.02" x2="-17.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="33.02" x2="-35.56" y2="33.02" width="0.1524" layer="91"/>
<junction x="-25.4" y="33.02"/>
<label x="-35.56" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="T2" gate="G$1" pin="G"/>
<wire x1="299.72" y1="20.32" x2="299.72" y2="25.4" width="0.1524" layer="91"/>
<label x="299.72" y="27.94" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="299.72" y1="25.4" x2="299.72" y2="27.94" width="0.1524" layer="91"/>
<wire x1="292.1" y1="25.4" x2="299.72" y2="25.4" width="0.1524" layer="91"/>
<junction x="299.72" y="25.4"/>
</segment>
<segment>
<pinref part="T1" gate="G$1" pin="G"/>
<wire x1="299.72" y1="58.42" x2="299.72" y2="63.5" width="0.1524" layer="91"/>
<label x="299.72" y="66.04" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="299.72" y1="63.5" x2="299.72" y2="66.04" width="0.1524" layer="91"/>
<wire x1="292.1" y1="63.5" x2="299.72" y2="63.5" width="0.1524" layer="91"/>
<junction x="299.72" y="63.5"/>
</segment>
<segment>
<pinref part="T3" gate="G$1" pin="G"/>
<wire x1="299.72" y1="-17.78" x2="299.72" y2="-12.7" width="0.1524" layer="91"/>
<label x="299.72" y="-10.16" size="1.778" layer="95"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="299.72" y1="-12.7" x2="299.72" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-12.7" x2="299.72" y2="-12.7" width="0.1524" layer="91"/>
<junction x="299.72" y="-12.7"/>
</segment>
<segment>
<pinref part="T4" gate="G$1" pin="G"/>
<wire x1="302.26" y1="-55.88" x2="302.26" y2="-50.8" width="0.1524" layer="91"/>
<label x="302.26" y="-48.26" size="1.778" layer="95"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="302.26" y1="-50.8" x2="302.26" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="294.64" y1="-50.8" x2="302.26" y2="-50.8" width="0.1524" layer="91"/>
<junction x="302.26" y="-50.8"/>
</segment>
<segment>
<wire x1="7.62" y1="40.64" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<label x="7.62" y="40.64" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="3V3@17"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="109.22" y1="33.02" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<label x="109.22" y="33.02" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="119.38" y1="33.02" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
<label x="119.38" y="33.02" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="71.12" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="76.2" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<pinref part="T5" gate="G$1" pin="G"/>
<wire x1="-15.24" y1="76.2" x2="-15.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="-15.24" y="76.2"/>
<label x="-15.24" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="43.18" x2="-25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="45.72" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="T6" gate="G$1" pin="G"/>
<wire x1="-15.24" y1="45.72" x2="-15.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="-15.24" y="45.72"/>
<label x="-20.32" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="10"/>
<wire x1="27.94" y1="-12.7" x2="27.94" y2="-10.16" width="0.1524" layer="91"/>
<label x="27.94" y="-12.7" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RSCLK" class="0">
<segment>
<wire x1="7.62" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<label x="7.62" y="33.02" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="GPIO11@23"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="T3" gate="G$1" pin="S"/>
<wire x1="292.1" y1="-22.86" x2="292.1" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-25.4" x2="297.18" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-25.4" x2="287.02" y2="-25.4" width="0.1524" layer="91"/>
<junction x="292.1" y="-25.4"/>
<label x="292.1" y="-27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="MSCLK" class="0">
<segment>
<label x="330.2" y="-20.32" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="327.66" y1="-20.32" x2="327.66" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-25.4" x2="309.88" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="T3" gate="G$1" pin="D"/>
<wire x1="307.34" y1="-25.4" x2="309.88" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="327.66" y1="-25.4" x2="309.88" y2="-25.4" width="0.1524" layer="91"/>
<junction x="309.88" y="-25.4"/>
<wire x1="375.92" y1="-5.08" x2="375.92" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="375.92" y1="-20.32" x2="327.66" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="X4" gate="1" pin="3"/>
<wire x1="375.92" y1="-5.08" x2="368.3" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RMISO" class="0">
<segment>
<wire x1="7.62" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<label x="7.62" y="35.56" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="GPIO9@21"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="T2" gate="G$1" pin="S"/>
<wire x1="292.1" y1="15.24" x2="292.1" y2="12.7" width="0.1524" layer="91"/>
<wire x1="292.1" y1="12.7" x2="297.18" y2="12.7" width="0.1524" layer="91"/>
<wire x1="292.1" y1="12.7" x2="287.02" y2="12.7" width="0.1524" layer="91"/>
<junction x="292.1" y="12.7"/>
<label x="292.1" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMISO" class="0">
<segment>
<label x="335.28" y="7.62" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="309.88" y1="12.7" x2="309.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="D"/>
<wire x1="307.34" y1="12.7" x2="309.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="309.88" y1="12.7" x2="342.9" y2="12.7" width="0.1524" layer="91"/>
<junction x="309.88" y="12.7"/>
<pinref part="X4" gate="1" pin="1"/>
<wire x1="368.3" y1="-7.62" x2="370.84" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="370.84" y1="-7.62" x2="370.84" y2="15.24" width="0.1524" layer="91"/>
<wire x1="370.84" y1="15.24" x2="342.9" y2="15.24" width="0.1524" layer="91"/>
<wire x1="342.9" y1="15.24" x2="342.9" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCS" class="0">
<segment>
<label x="347.98" y="12.7" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="345.44" y1="12.7" x2="345.44" y2="50.8" width="0.1524" layer="91"/>
<wire x1="309.88" y1="50.8" x2="309.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="D"/>
<wire x1="307.34" y1="50.8" x2="309.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="345.44" y1="50.8" x2="309.88" y2="50.8" width="0.1524" layer="91"/>
<junction x="309.88" y="50.8"/>
<pinref part="X4" gate="1" pin="5"/>
<wire x1="368.3" y1="-2.54" x2="368.3" y2="12.7" width="0.1524" layer="91"/>
<wire x1="368.3" y1="12.7" x2="345.44" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MMOSI" class="0">
<segment>
<pinref part="T4" gate="G$1" pin="D"/>
<wire x1="309.88" y1="-63.5" x2="312.42" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="312.42" y1="-63.5" x2="312.42" y2="-60.96" width="0.1524" layer="91"/>
<junction x="312.42" y="-63.5"/>
<wire x1="373.38" y1="-17.78" x2="373.38" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-63.5" x2="312.42" y2="-63.5" width="0.1524" layer="91"/>
<label x="363.22" y="-17.78" size="1.778" layer="95"/>
<pinref part="X4" gate="1" pin="4"/>
<wire x1="353.06" y1="-5.08" x2="345.44" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="345.44" y1="-5.08" x2="345.44" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="345.44" y1="-17.78" x2="373.38" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RMOSI" class="0">
<segment>
<wire x1="7.62" y1="38.1" x2="20.32" y2="38.1" width="0.1524" layer="91"/>
<label x="7.62" y="38.1" size="1.778" layer="95"/>
<pinref part="RPI" gate="G$1" pin="GPIO10@19"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="T4" gate="G$1" pin="S"/>
<wire x1="294.64" y1="-60.96" x2="294.64" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="294.64" y1="-63.5" x2="299.72" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="294.64" y1="-63.5" x2="289.56" y2="-63.5" width="0.1524" layer="91"/>
<junction x="294.64" y="-63.5"/>
<label x="294.64" y="-66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="RCS" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="T1" gate="G$1" pin="S"/>
<wire x1="292.1" y1="53.34" x2="292.1" y2="50.8" width="0.1524" layer="91"/>
<wire x1="292.1" y1="50.8" x2="297.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="292.1" y1="50.8" x2="287.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="292.1" y="50.8"/>
<label x="292.1" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO25@22"/>
<wire x1="50.8" y1="35.56" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<label x="55.88" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA0SL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB0"/>
<pinref part="X2" gate="G$1" pin="P$4"/>
<wire x1="198.12" y1="73.66" x2="218.44" y2="73.66" width="0.1524" layer="91"/>
<label x="213.36" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA1SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$6"/>
<pinref part="IC1" gate="G$1" pin="GPB1"/>
<wire x1="218.44" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<label x="213.36" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA2SL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB2"/>
<pinref part="X2" gate="G$1" pin="P$8"/>
<wire x1="198.12" y1="68.58" x2="218.44" y2="68.58" width="0.1524" layer="91"/>
<label x="213.36" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA3SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$10"/>
<pinref part="IC1" gate="G$1" pin="GPB3"/>
<wire x1="218.44" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<label x="213.36" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA4SL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB4"/>
<pinref part="X2" gate="G$1" pin="P$12"/>
<wire x1="198.12" y1="63.5" x2="218.44" y2="63.5" width="0.1524" layer="91"/>
<label x="213.36" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA5SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$14"/>
<pinref part="IC1" gate="G$1" pin="GPB5"/>
<wire x1="218.44" y1="60.96" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<label x="213.36" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA6SL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB6"/>
<pinref part="X2" gate="G$1" pin="P$16"/>
<wire x1="198.12" y1="58.42" x2="218.44" y2="58.42" width="0.1524" layer="91"/>
<label x="213.36" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA7SL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB7"/>
<pinref part="X2" gate="G$1" pin="P$18"/>
<wire x1="198.12" y1="55.88" x2="218.44" y2="55.88" width="0.1524" layer="91"/>
<label x="213.36" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC0SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$17"/>
<label x="233.68" y="55.88" size="1.778" layer="95"/>
<wire x1="269.24" y1="78.74" x2="269.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="269.24" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA7"/>
<wire x1="198.12" y1="78.74" x2="269.24" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC1SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$15"/>
<label x="233.68" y="58.42" size="1.778" layer="95"/>
<wire x1="231.14" y1="58.42" x2="266.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="266.7" y1="58.42" x2="266.7" y2="81.28" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA6"/>
<wire x1="266.7" y1="81.28" x2="198.12" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC2SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$13"/>
<label x="233.68" y="60.96" size="1.778" layer="95"/>
<wire x1="264.16" y1="83.82" x2="264.16" y2="60.96" width="0.1524" layer="91"/>
<wire x1="264.16" y1="60.96" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA5"/>
<wire x1="198.12" y1="83.82" x2="264.16" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC3SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$11"/>
<label x="233.68" y="63.5" size="1.778" layer="95"/>
<wire x1="231.14" y1="63.5" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="261.62" y1="63.5" x2="261.62" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA4"/>
<wire x1="261.62" y1="86.36" x2="198.12" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC4SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$9"/>
<label x="233.68" y="66.04" size="1.778" layer="95"/>
<wire x1="259.08" y1="88.9" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="259.08" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA3"/>
<wire x1="198.12" y1="88.9" x2="259.08" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC5SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$7"/>
<wire x1="256.54" y1="68.58" x2="231.14" y2="68.58" width="0.1524" layer="91"/>
<label x="233.68" y="68.58" size="1.778" layer="95"/>
<wire x1="256.54" y1="68.58" x2="256.54" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA2"/>
<wire x1="256.54" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC6SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$5"/>
<wire x1="231.14" y1="71.12" x2="254" y2="71.12" width="0.1524" layer="91"/>
<label x="233.68" y="71.12" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="GPA1"/>
<wire x1="198.12" y1="93.98" x2="254" y2="93.98" width="0.1524" layer="91"/>
<wire x1="254" y1="93.98" x2="254" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PC7SL" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$3"/>
<wire x1="251.46" y1="73.66" x2="231.14" y2="73.66" width="0.1524" layer="91"/>
<label x="233.68" y="73.66" size="1.778" layer="95"/>
<wire x1="251.46" y1="73.66" x2="251.46" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GPA0"/>
<wire x1="251.46" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PB0SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$4"/>
<wire x1="195.58" y1="10.16" x2="215.9" y2="10.16" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB0"/>
<label x="210.82" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB1SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$6"/>
<wire x1="215.9" y1="7.62" x2="195.58" y2="7.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB1"/>
<label x="210.82" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB2SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$8"/>
<wire x1="195.58" y1="5.08" x2="215.9" y2="5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB2"/>
<label x="210.82" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB3SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$10"/>
<wire x1="215.9" y1="2.54" x2="195.58" y2="2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB3"/>
<label x="210.82" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB4SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$12"/>
<wire x1="195.58" y1="0" x2="215.9" y2="0" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB4"/>
<label x="210.82" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB5SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$14"/>
<wire x1="215.9" y1="-2.54" x2="195.58" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB5"/>
<label x="210.82" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB6SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$16"/>
<wire x1="195.58" y1="-5.08" x2="215.9" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB6"/>
<label x="210.82" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB7SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$18"/>
<wire x1="195.58" y1="-7.62" x2="215.9" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPB7"/>
<label x="210.82" y="-7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD0SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$17"/>
<wire x1="228.6" y1="-7.62" x2="271.78" y2="-7.62" width="0.1524" layer="91"/>
<label x="233.68" y="-7.62" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="GPA7"/>
<wire x1="271.78" y1="15.24" x2="271.78" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="271.78" y1="15.24" x2="195.58" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD1SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$15"/>
<label x="233.68" y="-5.08" size="1.778" layer="95"/>
<wire x1="228.6" y1="-5.08" x2="269.24" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-5.08" x2="269.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA6"/>
<wire x1="195.58" y1="17.78" x2="269.24" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD2SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$13"/>
<label x="233.68" y="-2.54" size="1.778" layer="95"/>
<wire x1="228.6" y1="-2.54" x2="266.7" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA5"/>
<wire x1="266.7" y1="20.32" x2="266.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="195.58" y1="20.32" x2="266.7" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD3SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$11"/>
<label x="233.68" y="0" size="1.778" layer="95"/>
<wire x1="228.6" y1="0" x2="264.16" y2="0" width="0.1524" layer="91"/>
<wire x1="264.16" y1="0" x2="264.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA4"/>
<wire x1="264.16" y1="22.86" x2="195.58" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD4SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$9"/>
<label x="233.68" y="2.54" size="1.778" layer="95"/>
<wire x1="228.6" y1="2.54" x2="261.62" y2="2.54" width="0.1524" layer="91"/>
<wire x1="261.62" y1="25.4" x2="261.62" y2="2.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA3"/>
<wire x1="195.58" y1="25.4" x2="261.62" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD5SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$7"/>
<wire x1="259.08" y1="5.08" x2="228.6" y2="5.08" width="0.1524" layer="91"/>
<label x="233.68" y="5.08" size="1.778" layer="95"/>
<wire x1="259.08" y1="5.08" x2="259.08" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA2"/>
<wire x1="259.08" y1="27.94" x2="195.58" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD6SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$5"/>
<wire x1="228.6" y1="7.62" x2="256.54" y2="7.62" width="0.1524" layer="91"/>
<label x="233.68" y="7.62" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="GPA1"/>
<wire x1="195.58" y1="30.48" x2="256.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="256.54" y1="30.48" x2="256.54" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PD7SL" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P$3"/>
<wire x1="251.46" y1="10.16" x2="228.6" y2="10.16" width="0.1524" layer="91"/>
<label x="233.68" y="10.16" size="1.778" layer="95"/>
<wire x1="251.46" y1="10.16" x2="251.46" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GPA0"/>
<wire x1="251.46" y1="33.02" x2="195.58" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5VPROT" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="P$2"/>
<wire x1="218.44" y1="76.2" x2="215.9" y2="76.2" width="0.1524" layer="91"/>
<wire x1="215.9" y1="76.2" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P$1"/>
<wire x1="215.9" y1="99.06" x2="231.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="231.14" y1="99.06" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
<junction x="231.14" y="99.06"/>
<label x="226.06" y="101.6" size="1.778" layer="95"/>
<wire x1="231.14" y1="99.06" x2="231.14" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="231.14" y1="35.56" x2="231.14" y2="38.1" width="0.1524" layer="91"/>
<label x="231.14" y="38.1" size="1.778" layer="95"/>
<wire x1="213.36" y1="35.56" x2="231.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="231.14" y1="12.7" x2="228.6" y2="12.7" width="0.1524" layer="91"/>
<wire x1="231.14" y1="35.56" x2="231.14" y2="12.7" width="0.1524" layer="91"/>
<junction x="231.14" y="35.56"/>
<pinref part="X1" gate="G$1" pin="P$1"/>
<pinref part="X1" gate="G$1" pin="P$2"/>
<wire x1="215.9" y1="12.7" x2="213.36" y2="12.7" width="0.1524" layer="91"/>
<wire x1="213.36" y1="12.7" x2="213.36" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="238.76" y1="-73.66" x2="238.76" y2="-68.58" width="0.1524" layer="91"/>
<label x="238.76" y="-68.58" size="1.778" layer="95"/>
<pinref part="X3" gate="G$1" pin="4"/>
<wire x1="238.76" y1="-73.66" x2="231.14" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GPB5"/>
<wire x1="198.12" y1="-83.82" x2="203.2" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-83.82" x2="203.2" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-99.06" x2="205.74" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-99.06" x2="205.74" y2="-93.98" width="0.1524" layer="91"/>
<label x="205.74" y="-96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="VCCPROT" gate="G$2" pin="KL1"/>
<wire x1="134.62" y1="-58.42" x2="134.62" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-60.96" x2="137.16" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="VCCPROT" gate="G$2" pin="KL2"/>
<wire x1="137.16" y1="-60.96" x2="137.16" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="137.16" y1="-60.96" x2="142.24" y2="-60.96" width="0.1524" layer="91"/>
<junction x="137.16" y="-60.96"/>
<wire x1="142.24" y1="-60.96" x2="142.24" y2="-55.88" width="0.1524" layer="91"/>
<label x="142.24" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PFUSE" gate="G$1" pin="2"/>
<wire x1="91.44" y1="60.96" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
<wire x1="93.98" y1="60.96" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
<wire x1="93.98" y1="68.58" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="101.6" y1="68.58" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<label x="99.06" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="2"/>
<wire x1="353.06" y1="-7.62" x2="353.06" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="353.06" y1="-15.24" x2="347.98" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="347.98" y1="-15.24" x2="347.98" y2="-12.7" width="0.1524" layer="91"/>
<label x="347.98" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="93.98" y1="20.32" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RPI" gate="G$1" pin="GPIO16@36"/>
<wire x1="50.8" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="S1" gate="G$1" pin="P"/>
<wire x1="104.14" y1="20.32" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="119.38" y1="20.32" x2="124.46" y2="20.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="22.86" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="119.38" y="20.32"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="109.22" y1="22.86" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="109.22" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="12.7" x2="109.22" y2="7.62" width="0.1524" layer="91"/>
<junction x="109.22" y="12.7"/>
<pinref part="S2" gate="G$1" pin="P"/>
<wire x1="109.22" y1="7.62" x2="124.46" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="93.98" y1="15.24" x2="93.98" y2="12.7" width="0.1524" layer="91"/>
<wire x1="50.8" y1="15.24" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RPI" gate="G$1" pin="GPIO20@38"/>
</segment>
</net>
<net name="PB4JT" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="6"/>
<wire x1="231.14" y1="-76.2" x2="238.76" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-76.2" x2="238.76" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-91.44" x2="210.82" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-91.44" x2="210.82" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPB4"/>
<wire x1="210.82" y1="-81.28" x2="198.12" y2="-81.28" width="0.1524" layer="91"/>
<label x="208.28" y="-81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC5JT" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPB0"/>
<wire x1="198.12" y1="-71.12" x2="213.36" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-71.12" x2="213.36" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="9"/>
<wire x1="213.36" y1="-81.28" x2="215.9" y2="-81.28" width="0.1524" layer="91"/>
<label x="208.28" y="-71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC2JT" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPB3"/>
<wire x1="198.12" y1="-78.74" x2="205.74" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-78.74" x2="205.74" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-68.58" x2="215.9" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="1"/>
<wire x1="215.9" y1="-68.58" x2="215.9" y2="-71.12" width="0.1524" layer="91"/>
<label x="208.28" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC4JT" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPB2"/>
<wire x1="198.12" y1="-76.2" x2="208.28" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-76.2" x2="210.82" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="3"/>
<wire x1="215.9" y1="-73.66" x2="210.82" y2="-73.66" width="0.1524" layer="91"/>
<label x="210.82" y="-73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC3JT" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="5"/>
<wire x1="210.82" y1="-76.2" x2="215.9" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPB1"/>
<wire x1="198.12" y1="-73.66" x2="208.28" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-73.66" x2="210.82" y2="-76.2" width="0.1524" layer="91"/>
<label x="210.82" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPA0"/>
<wire x1="198.12" y1="-48.26" x2="203.2" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-48.26" x2="203.2" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="GPIO" gate="G$1" pin="2"/>
<wire x1="203.2" y1="-43.18" x2="228.6" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="GPIO" gate="G$1" pin="3"/>
<wire x1="228.6" y1="-45.72" x2="205.74" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-45.72" x2="205.74" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPA1"/>
<wire x1="205.74" y1="-50.8" x2="198.12" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPA2"/>
<wire x1="198.12" y1="-53.34" x2="208.28" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-53.34" x2="208.28" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="GPIO" gate="G$1" pin="4"/>
<wire x1="208.28" y1="-48.26" x2="228.6" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="GPIO" gate="G$1" pin="5"/>
<wire x1="228.6" y1="-50.8" x2="210.82" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-50.8" x2="210.82" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPA3"/>
<wire x1="210.82" y1="-55.88" x2="198.12" y2="-55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="213.36" y1="-58.42" x2="213.36" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="GPIO" gate="G$1" pin="6"/>
<wire x1="213.36" y1="-53.34" x2="228.6" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPA4"/>
<wire x1="198.12" y1="-58.42" x2="213.36" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="GPIO" gate="G$1" pin="7"/>
<wire x1="228.6" y1="-55.88" x2="215.9" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-55.88" x2="215.9" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPA5"/>
<wire x1="215.9" y1="-60.96" x2="198.12" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GPA6"/>
<wire x1="198.12" y1="-63.5" x2="218.44" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-63.5" x2="218.44" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="GPIO" gate="G$1" pin="8"/>
<wire x1="218.44" y1="-58.42" x2="228.6" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="GPIO" gate="G$1" pin="9"/>
<wire x1="228.6" y1="-60.96" x2="220.98" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-60.96" x2="220.98" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GPA7"/>
<wire x1="220.98" y1="-66.04" x2="198.12" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCLH" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<wire x1="172.72" y1="73.66" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
<label x="160.02" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="SCL"/>
<wire x1="172.72" y1="-71.12" x2="167.64" y2="-71.12" width="0.1524" layer="91"/>
<label x="165.1" y="-71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SCL"/>
<wire x1="170.18" y1="10.16" x2="165.1" y2="10.16" width="0.1524" layer="91"/>
<label x="162.56" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="T6" gate="G$1" pin="D"/>
<wire x1="-5.08" y1="33.02" x2="-7.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="33.02" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<junction x="-7.62" y="33.02"/>
<wire x1="2.54" y1="33.02" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<junction x="2.54" y="33.02"/>
<label x="7.62" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDAH" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="172.72" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<label x="160.02" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="SDA"/>
<wire x1="172.72" y1="-73.66" x2="167.64" y2="-73.66" width="0.1524" layer="91"/>
<label x="165.1" y="-73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SDA"/>
<wire x1="170.18" y1="7.62" x2="165.1" y2="7.62" width="0.1524" layer="91"/>
<label x="162.56" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="2.54" y1="60.96" x2="10.16" y2="60.96" width="0.1524" layer="91"/>
<label x="7.62" y="60.96" size="1.778" layer="95"/>
<pinref part="T5" gate="G$1" pin="D"/>
<wire x1="-7.62" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="2.54" y="60.96"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="ON" gate="G$1" pin="A"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="91.44" y1="40.64" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="5VPROT" gate="G$1" pin="A"/>
<wire x1="101.6" y1="40.64" x2="101.6" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="5VPROTSENSE" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO21@40"/>
<wire x1="50.8" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<label x="53.34" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="101.6" y1="53.34" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<junction x="101.6" y="55.88"/>
<label x="106.68" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO24" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO24@18"/>
<wire x1="50.8" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<label x="53.34" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="6"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="-7.62" width="0.1524" layer="91"/>
<label x="38.1" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO23" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO23@16"/>
<wire x1="50.8" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<label x="53.34" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="5"/>
<wire x1="40.64" y1="-12.7" x2="40.64" y2="-7.62" width="0.1524" layer="91"/>
<label x="40.64" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO18" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO18@12"/>
<wire x1="50.8" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<label x="53.34" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="4"/>
<wire x1="43.18" y1="-12.7" x2="43.18" y2="-7.62" width="0.1524" layer="91"/>
<label x="43.18" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO15" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO15@10"/>
<wire x1="50.8" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<label x="53.34" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="3"/>
<wire x1="45.72" y1="-12.7" x2="45.72" y2="-7.62" width="0.1524" layer="91"/>
<label x="45.72" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO14" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO14@8"/>
<wire x1="50.8" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<label x="53.34" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="2"/>
<wire x1="48.26" y1="-12.7" x2="48.26" y2="-7.62" width="0.1524" layer="91"/>
<label x="48.26" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO7" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO7@26"/>
<wire x1="50.8" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<label x="53.34" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="30.48" y1="-12.7" x2="30.48" y2="-2.54" width="0.1524" layer="91"/>
<label x="30.48" y="-7.62" size="1.778" layer="95" rot="R90"/>
<pinref part="GPIO1" gate="G$1" pin="9"/>
</segment>
</net>
<net name="GPIO12" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO12@32"/>
<wire x1="50.8" y1="22.86" x2="53.34" y2="22.86" width="0.1524" layer="91"/>
<label x="53.34" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="8"/>
<wire x1="33.02" y1="-12.7" x2="33.02" y2="-7.62" width="0.1524" layer="91"/>
<label x="33.02" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIO8" class="0">
<segment>
<pinref part="RPI" gate="G$1" pin="GPIO8@24"/>
<wire x1="50.8" y1="33.02" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<label x="55.88" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO1" gate="G$1" pin="7"/>
<wire x1="35.56" y1="-12.7" x2="35.56" y2="-7.62" width="0.1524" layer="91"/>
<label x="35.56" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
