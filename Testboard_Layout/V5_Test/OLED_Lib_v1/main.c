/*
 * OLED_Lib_v1.c
 *
 * Created: 10.05.2021 11:08:15
 * Author : ZKS
 *
 * Demo-Programm f�r die Nutzung des OLED Displays auf der neuen Megacard
 *
 * Das Programm dient nur als Einstiegshilfe. F�r die weitere Nutzung in Programmierprojekten
 * m�ssen komfortablere Bibliotheken entwickelt werden. 
 *
 */ 

/*
* Test.c
*
* Created: 03/06/2022 14:14:35
* Author : Basti
*/

#define F_CPU 12000000ul

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "mc51_oled.h"

enum{EstablishConnection,PortD_Test,PortA_Test,PortB_Test,PortC_Test,LED_Test,LED_Max_Test,Taster_Test, Buzzer_Test, ADC_Test, JTAG_Test, Display_Test, Test_Done};

volatile uint8_t cur_Case = EstablishConnection;
volatile int overflowCounter = 0;
volatile uint8_t adcValue = 0;

void wait_for_PD2_H()
{
	while(!(PIND & (1<<PD2))){}
}

void wait_for_PD2_L()
{
	while(PIND & (1<<PD2)){}
}

void wait_for_PD2_H_L()
{
	wait_for_PD2_H();
	wait_for_PD2_L();
}

void display_TxtToDisplay(char txt[], uint8_t len)
{
	for(uint8_t i = 0; i<len; i++)
	{
		display_CharToDisplay(txt[i]);
	}	
}

ISR(TIMER0_OVF_vect)
{
	switch(cur_Case){
		case LED_Test:
		overflowCounter++;
		TCNT0 = 220; //Preload
		break;
		
		case Buzzer_Test:
		TCNT0 = 206;		//256 - 50 = 206 - Frequency should be about 3.75kHz
		PORTB ^= (1<<PB3);
		break;
		
		default:
		overflowCounter = 0;
		break;
	}
}

ISR(ADC_vect)
{
	adcValue = ADCH;	//8-Bit-Messung
	
	TIFR |= (1<<TOV0);	//Clear the Timer 0 interruptflag
}


int main(void)
{
	uint8_t MyChar = 'A';
	uint8_t Zeile = 0;
	
	/* Replace with your application code */
	while (1)
	{
		switch(cur_Case)
		{
			//Establish Connection using PortD
			case EstablishConnection:
			DDRD |= (1<<PD3);
			PORTD |= (1<<PD3);
			
			wait_for_PD2_H();
			
			PORTD &= ~(1<<PD3);
			
			wait_for_PD2_L();
			
			DDRD = 0x01;
			PORTD = 0x01;
			
			while(!(PIND & (1<<PD3))){}
			
			cur_Case = PortD_Test;
			break;
			
			//Port-Tests
			#pragma region Port_Tests
			case PortD_Test:	//Consider: PD2 is input for test confirmation therefore not tested anymore.
			
			//Output Tests
			DDRD = ~(1<<PD2); //PD2 input Test is output
			
			//All Low Test
			PORTD = 0x00;	  //PDX is low/PD2 is High-Z input
			wait_for_PD2_H(); //wait for PD2

			//All High Test
			wait_for_PD2_L();	//wait for PD2 to be cleared
			PORTD = 0xFF;
			wait_for_PD2_H(); //wait for PD2 to be set
			
			//High Low Test A
			wait_for_PD2_L();	//wait for PD2 to be cleared
			PORTD = 0xAA; //0b1010 1010 = AA
			wait_for_PD2_H(); //wait for PD2 to be set
			
			//High Low Test B
			wait_for_PD2_L();	//wait for PD2 to be cleared
			PORTD = 0x51; //0b0101 0001 = 51
			wait_for_PD2_H(); //wait for PD2 to be set
			
			//Input Tests
			DDRD = (1<<PD2); //PD is input except PD2
			PORTD = 0x00;    //No Pull-Up & PD2 low
			//All-Low-Test
			_delay_ms(25);
			
			while((PIND&(~(1<<PD2))) != 0x00){}	//Wait for all low read
			PORTD = (1<<PD2); //Sets PD2

			//All-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while((PIND&(~(1<<PD2))) != 0xFB){}	//Wait for all High read (except PD2)
			PORTD = (1<<PD2); //Sets PD2
			
			//High-Low-Test
			_delay_ms(25);
			PORTD = 0x00;
			while((PIND&(~(1<<PD2))) != 0xAA){}	//Wait for 0x1010 1010 = 0xAA
			PORTD = (1<<PD2); //Sets PD2
			
			//Low-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while((PIND&(~(1<<PD2))) != 0x51){}	//Wait for 0x0101 0001 = 0x51
			PORTD = (1<<PD2); //Sets PD2

			_delay_ms(25);
			
			DDRD = 0x00; //DDRD is input
			PORTD = 0x00;
			cur_Case = PortA_Test;
			break;
			
			case PortA_Test:
			//Output Tests
			DDRC = 0xFF;
			DDRA = 0xFF;
			wait_for_PD2_L(); //Wait for PD2 to be cleared

			PORTA = 0x00;
			wait_for_PD2_H(); //Wait for PD2


			wait_for_PD2_L(); //Wait for PD2
			PORTA = 0xFF;
			wait_for_PD2_H(); //Wait for PD2
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTA = 0xAA;	//1010 1010 = AA
			wait_for_PD2_H(); //Wait for PD2
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTA = 0x55;	//0101 0101 = 55
			wait_for_PD2_H(); //Wait for PD2

			
			//Input Tests
			PORTA = 0x00;    //No Pull-Up
			DDRA = 0x00;     //PA is input
			
			DDRD = (1<<PD2);//PD2 is ouput
			PORTD = 0x00;
			
			//All-Low-Test
			PORTC = (1<<PC7);
			_delay_ms(25);
			
			while(PINA != 0x00){PORTC = (1<<PC6);}	//Wait for all low read
			PORTD = (1<<PD2); //Sets PD2
			PORTC = (1<<PC5);
			
			//All-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			
			while(PINA != 0xFF){}	//Wait for all High read (except PD2)
			PORTD = (1<<PD2); //Sets PD2

			//High-Low-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINA != 0xAA){}	//Wait for 0x1010 1010 = 0xAA
			PORTD = (1<<PD2); //Sets PD2
			
			//Low-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINA != 0x55){}	//Wait for 0x0101 0001 = 0x51
			PORTD = (1<<PD2); //Sets PD2

			_delay_ms(25);
			
			DDRD = 0x00;
			
			DDRA = 0x00;
			PORTA = 0x00;
			cur_Case = PortB_Test;
			break;
			
			case PortB_Test:
			//Output Tests
			DDRB = 0xFF;

			wait_for_PD2_L(); //Wait for PD2
			PORTB = 0x00;
			wait_for_PD2_H(); //Wait for PD2
			

			wait_for_PD2_L(); //Wait for PD2
			PORTB = 0xFF;
			wait_for_PD2_H(); //Wait for PD2
			
			wait_for_PD2_L(); //Wait for PD2
			PORTB = 0xAA;	//1010 1010 = AA
			wait_for_PD2_H(); //Wait for PD2
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTB = 0x55;	//0101 0101 = 55
			wait_for_PD2_H(); //Wait for PD2

			
			//Input Tests
			DDRB = 0x00; //PB is input
			PORTB = 0x00;    //No Pull-Up
			
			DDRD = (1<<PD2);//PD2 is ouput
			PORTD = 0x00;
			
			//All-Low-Test
			_delay_ms(25);
			
			while(PINB != 0x00){}	//Wait for all low read
			PORTD = (1<<PD2); //Sets PD2

			//All-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINB != 0xFF){}	//Wait for all High read (except PD2)
			PORTD = (1<<PD2); //Sets PD2
			
			//High-Low-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINB != 0xAA){}	//Wait for 0x1010 1010 = 0xAA
			PORTD = (1<<PD2); //Sets PD2
			
			//Low-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINB != 0x55){}	//Wait for 0x0101 0001 = 0x51
			PORTD = (1<<PD2); //Sets PD2

			_delay_ms(25);
			
			DDRD = 0x00;
			
			DDRB = 0x00;
			PORTB = 0x00;
			cur_Case = PortC_Test;
			break;
			
			case PortC_Test:
			DDRC = 0xFF;
			
			//Output Tests
			wait_for_PD2_L(); //Wait for PD2
			PORTC = 0x00;
			wait_for_PD2_H(); //Wait for PD2
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTC = 0xFF;
			wait_for_PD2_H(); //Wait for PD2
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTC = 0xAA;	//1010 1010 = AA
			wait_for_PD2_H(); //Wait for PD2
			
			
			
			wait_for_PD2_L(); //Wait for PD2
			PORTC = 0x55;	//0101 0101 = 55
			wait_for_PD2_H(); //Wait for PD2

			DDRC = 0x00;
			PORTC = 0x00;
			
			//Input Tests
			DDRC= 0x00; //PB is input
			PORTC = 0x00;    //No Pull-Up
			
			DDRD = (1<<PD2);//PD2 is ouput
			PORTD = 0x00;
			
			//All-Low-Test
			_delay_ms(25);
			
			while(PINC != 0x00){}	//Wait for all low read
			PORTD = (1<<PD2); //Sets PD2

			//All-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINC != 0xFF){}	//Wait for all High read (except PD2)
			PORTD = (1<<PD2); //Sets PD2
			
			//High-Low-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINC != 0xAA){}	//Wait for 0x1010 1010 = 0xAA
			PORTD = (1<<PD2); //Sets PD2
			
			//Low-High-Test
			_delay_ms(25);
			PORTD = 0x00;
			while(PINC != 0x55){}	//Wait for 0x0101 0001 = 0x51
			PORTD = (1<<PD2); //Sets PD2

			_delay_ms(25);
			
			DDRD = 0x00;
			PORTD = 0x00;
			
			DDRC = 0x00;
			PORTC = 0x00;
			
			cur_Case = LED_Test;
			break;
			#pragma endregion Port_Tests
			
			//LED-Tests mit Lauflicht + Timer0
			//Then all lights on
			#pragma region LED-Test
			case LED_Test:
			DDRC = 0xFF;
			PORTC = 0x00;
			//--Code-Position Sync between megacard and raspberrypi
			wait_for_PD2_H_L(); //Waits for PD2

			//wait_for_PD2_L();    //Waits for PD2 to clear
			
			//LED Test: Lauflicht �ber alle LEDs
			
			
			//Slow Timer init
			cli();					//Interrupts sperren
			TCCR0 = (1<<CS02);		//Prescaler= 256
			TCNT0 = 220;			//Preload = 220
			
			TIMSK = (1<<TOIE0);
			sei();					//Interrupt freischalten
			
			while(!(PIND & (1<<PD2)))
			{
				if(overflowCounter >= 375)
				{
					PORTC = (PORTC<<1);
					if(PORTC == 0x00)
					{
						PORTC = (1<<PC0);
					}
					overflowCounter = 0;
				}
			}	//Wait for PD2 whilst running light
			//PD2 is triggered, when user confirms, that light is sucessful.
			cli();		//Interrupts sperren
			TIMSK = 0x00;
			TCCR0 = 0x00;
			
			PORTC = 0x00;
			DDRC = 0x00;
			
			wait_for_PD2_L();    //Waits for PD2 to clear
			_delay_ms(35);
			
			cur_Case = LED_Max_Test;
			break;
			

			//LED-Load Test
			case LED_Max_Test:
			DDRC = 0xFF;
			PORTC = 0xFF; //All LEDs
			wait_for_PD2_H();	//Wait for PD2
			
			PORTC = 0x00;
			DDRC = 0x00;
			wait_for_PD2_L();    //Waits for PD2 to clear
			
			cur_Case = Taster_Test;
			break;
			#pragma endregion LED-Test
			
			//Taster-Test
			#pragma region Taster-Test
			case Taster_Test:
			PORTA = 0x0F; //Pull-Up Taster
			DDRA = 0x00;  //PortA Input
			
			PORTD = 0x00;
			DDRD = (1<<PD3); //PD3 is Output
			
			PORTC = 0x00;
			DDRC = 0xFF;
			
			//PA0
			wait_for_PD2_H();		//Wait for PD2
			
			while(!((~PINA)&(1<<PA0))){}	//Wait for PA0 press
			PORTC = (1<<PC0);
			while(((~PINA)&(1<<PA0))){}	    //Wait for PA0 release
			PORTC = 0x00;
			
			PORTD = (1<<PD3);				//Write PD3
			wait_for_PD2_L();		//Wait for PD2 clear
			
			//PA1
			PORTD = 0x00;					//Clear PD3
			wait_for_PD2_H();		//Wait for PD2
			
			while(!((~PINA)&(1<<PA1))){}	//Wait for PA1 press
			PORTC = (1<<PC1);
			while(((~PINA)&(1<<PA1))){}	    //Wait for PA1 release
			PORTC = 0x00;
			
			PORTD = (1<<PD3);				//Write PD3
			wait_for_PD2_L();		//Wait for PD2 clear
			
			
			//PA2
			PORTD = 0x00;					//Clear PD3
			wait_for_PD2_H();		//Wait for PD2
			
			while(!((~PINA)&(1<<PA2))){}	//Wait for PA2 press
			PORTC = (1<<PC2);
			while(((~PINA)&(1<<PA2))){}	    //Wait for PA2 release
			PORTC = 0x00;
			
			PORTD = (1<<PD3);				//Write PD3
			wait_for_PD2_L();		//Wait for PD2 clear
			
			//PA3
			PORTD = 0x00;					//Clear PD3
			wait_for_PD2_H();		//Wait for PD2
			
			while(!((~PINA)&(1<<PA3))){}	//Wait for PA3 press
			PORTC = (1<<PC3);
			while(((~PINA)&(1<<PA3))){}	    //Wait for PA3 release
			PORTC = 0x00;
			
			PORTD = (1<<PD3);				//Write PD3
			wait_for_PD2_L();		//Wait for PD2 clear
			
			PORTD = 0x00;
			DDRD = 0x00;
			
			PORTA = 0x00;					//Clear the Pull-Up Input
			cur_Case = Buzzer_Test;
			break;
			#pragma endregion Taster-Test
			
			//Buzzer-Test
			#pragma region Buzzer-Test
			case Buzzer_Test:
			PORTB = 0x00;				//Clear PA0
			DDRB = (1<<PB3);			//PA4 = Output with buzzer connected
			
			wait_for_PD2_H_L();
			
			TCCR0 = (1<<CS01)|(1<<CS00);	//Prescaler = 64
			TIMSK = (1<<TOIE0);				//Overflow Interrupt enable
			sei();							//Interruptfreigabe
			
			wait_for_PD2_H_L();
			
			PORTB = 0x00;
			DDRB = 0x00;
			
			TIMSK = 0x00;
			TCCR0 = 0x00;
			cli();
			
			cur_Case = ADC_Test;
			break;
			#pragma endregion Buzzer-Test
			
			//ADC-Test
			#pragma region ADC_Tst
			case ADC_Test:
			DDRC = 0xFF;
			PORTC = 0x00;
			
			wait_for_PD2_H_L();
			
			cli();
			TCCR0 |= (1<<CS02)|(1<<CS00);   //Prescaler: 1024 -> ca. alle 21ms ein Overflow
			TIMSK = (1<<TOIE0);
			
			//        Uref = AVCC, Relative messung
			//            |       Linksb�ndig, ADC5 Input
			ADCSRA |= (1<<ADEN) | (1<<ADIE)|(1<<ADATE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
			//fclk_adc = 12MHz/128 =  93.75kHz
			
			ADMUX  |= (1<<REFS0) |(1<<ADLAR) |(1<<MUX2)|(1<<MUX0);	//PD5 = Potentiometer on Megacard
			
			SFIOR |= (1<<ADTS2); //Trigger Source is Timer0 Overflow
			sei();
			
			while(!(PIND & (1<<PD2)))
			{
				PORTC = adcValue;			//Output ADC-Value to PortC
			}								//Wait for PD2
			wait_for_PD2_L();		//Wait for PD2 clear
			
			cli();
			TIMSK = 0x00;
			TCCR0 = 0x00;
			ADCSRA = 0x00;
			SFIOR = 0x00;
			sei();
			
			PORTC = 0x00;
			DDRC = 0x00;
			cur_Case = JTAG_Test;
			break;
			#pragma endregion
			
			//JTAG-Test
			#pragma region JTAG_Test
			case JTAG_Test:
			PORTD = 0x00;
			
			DDRC = 0xFF;
			PORTC = 0x00;
			
			DDRB = (1<<PB4);
			PORTB = 0x00;
			
			wait_for_PD2_H_L();
			
			//Init-State-Test
			wait_for_PD2_H_L();
			PORTC = (1<<PC2);
			wait_for_PD2_H_L();
			PORTC = (1<<PC4);
			wait_for_PD2_H_L();
			PORTC = (1<<PC3);
			wait_for_PD2_H_L();
			PORTC = (1<<PC5);
			wait_for_PD2_H_L();
			PORTC = 0x00;
			PORTD = (1<<PB4);
			wait_for_PD2_H_L();
			PORTB = 0x00;
			
			DDRB = 0x00;
			DDRC = 0x00;
			
			cur_Case = Display_Test;
			break;
			#pragma endregion
			
			//Display_Test
			case Display_Test:
			
			// Display Initialisierung ohne Parameter
			display_Init();
			
			// Ausgabe an der aktuellen Cursor-Position
			// Zeichen werden innerhalb einer Zeile nacheinander ausgegeben.
			// Am Ende geht der CUrsor wieder an den Zeilenanfang zur�ck.

			display_TxtToDisplay("Megacard V5",11);
			display_Pos(0,1);
			display_TxtToDisplay("Testand",7);
			display_Pos(0,2);
			display_TxtToDisplay("Fleischer B.",12);
			display_Pos(0,3);
			display_TxtToDisplay("Mueller J.",10);
			display_Pos(0,4);
			display_TxtToDisplay("HWE SS 2022",11);
			wait_for_PD2_H_L();
			
			cur_Case = Test_Done;
			break;
			
			case Test_Done:
			
			DDRA = 0x00;
			DDRB = 0x00;
			DDRC = 0x00;
			DDRD = 0x00;
			
			PORTA = 0x00;
			PORTB = 0x00;
			PORTC = 0x00;
			PORTD = 0x00;
			break;
			
			default:
			//cnt = EstablishConnection;
			break;
		}
	}
}