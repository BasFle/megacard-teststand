/* Implementierung von mc51_oled.h						    */
/*															*/
/*															*/
/*	Version 202109											*/				
/************************************************************/

#include <AVR/io.h>
#include <util/delay.h>
#include <stdio.h>
#include "twi_soft.h"
#include "fontdata.h"
#include "mc51_oled.h"



/****************************************************************/
/* HW-Abh�ngige Funktionen                                      */
/****************************************************************/

#define OLED_CTRL_BYTE_CMD  0b10000000
#define OLED_CTRL_BYTE_DATA 0b11000000
#define OLED_DEV_ADDR 0b01111000


// Header f�r intere Hilfsfunktionen

// Ganzes Display bedingungslos clearen
void oled_clear_full(void);

// Den Text-Cursor an eine definierte absolute Position setzen
void oled_pos_full(unsigned char col, unsigned char row);


/*********************************************************************/
/* Turn on the OLED Display                                          */
/*********************************************************************/
void _hw_Init(void)
{
	
	// Initialisierung der SW-I2C Schnittstelle
	twi_s_init();
	
	//********************************************************
	// Display einschalten
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_DEV_ADDR>>1);
	
	// Daten schreiben
	twi_s_write(0xaf);
	
	// Transfer beenden
	twi_s_stop();
	
	// Execute
	_delay_ms(100);
	//********************************************************

	//********************************************************
	// Charge Pump hochfahren
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_CMD);
	
	// Daten schreiben
	twi_s_write(0x8d);

	// Daten schreiben
	twi_s_write(0x14);
	
	// Transfer beenden
	twi_s_stop();
	
	// Execute
	_delay_ms(100);
	//********************************************************

	//********************************************************
	// Enable RAM Content
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_CMD);
	
	// Daten schreiben
	twi_s_write(0xa4);

	// Transfer beenden
	twi_s_stop();
	
	// Execute
	_delay_ms(100);
	//********************************************************

	// Clear the display
	oled_clear_full();
	_delay_ms(100);
}

/*************************************************************/
/* Sets the next Output to position(col, row)	       		 */
void oled_pos_full(unsigned char col, unsigned char row)
{
	// catch invalid parameters
	if(row>=OLED_ROWS) row=(OLED_ROWS-1);
	if(col>=OLED_COLS) col=(OLED_COLS-1);


	//********************************************************
	// Set Page Adress
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_CMD);
	
	// Daten schreiben
	twi_s_write((0b10110000 | row));

	// Transfer beenden
	twi_s_stop();
	//**************************************

	//********************************************************
	// Lower Nibble Col Address
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_CMD);
	
	// Daten schreiben
	twi_s_write((col<<3) & 0x0f);

	// Transfer beenden
	twi_s_stop();
	//**************************************

	//********************************************************
	// Higher Nibble Col Adress
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_CMD);
	
	// Daten schreiben
	twi_s_write((0x10 | ((col>>1) & 0x0f)));

	// Transfer beenden
	twi_s_stop();
	//**************************************
	
}

void _hw_Pos(unsigned char col, unsigned char row)
{
	oled_pos_full(col,row);
}


/*************************************************************/
/* Sets the next Output to position(0,0) of user space					 */
void _hw_Home(void)
{
	_hw_Pos(0,0);
}


/*************************************************************/
// clear the Display by writing zeros to all pages 
void oled_clear_full()
{
	unsigned char ix,jx;
	
	// loop over all pages
	for(ix=0;ix<OLED_ROWS;ix++)
	{
		//Set cursor to beginning of next page
		oled_pos_full(0,ix);
		
		//loop over all columns
		for(jx=0;jx<OLED_PIXEL_X;jx++)
		{
			
				//********************************************************
				// write zero to next GDRAM Position
				// Bus �ffnen
				twi_s_start(OLED_DEV_ADDR);
	
				// Daten schreiben
				twi_s_write(OLED_CTRL_BYTE_DATA);
	
				// Daten schreiben
				twi_s_write(0);

				// Transfer beenden
				twi_s_stop();
				//**************************************
		}
	}
	// finally we set the cursor back to 0,0 of the user space
	_hw_Pos(0,0);
}

// Clear the user space
void _hw_Clear()
{
	oled_clear_full();
}


/*************************************************************/
/* Display character at current position                     */
void _hw_CharToDisplay (uint8_t c)
{
	unsigned char cnt;
	
	// fix data out of range
	if (c<FONT_CHAR_FIRST) c=FONT_CHAR_FIRST;
	if (c>FONT_CHAR_LAST) c=FONT_CHAR_LAST;
	
	// Adjust data to become an index into fontData
	c=c-FONT_CHAR_FIRST;
	
	// Display character c at current position
	for (cnt=0;cnt<7;cnt++)
	{
			//********************************************************
			// write zero to next GDRAM Position
			// Bus �ffnen
			twi_s_start(OLED_DEV_ADDR);
	
			// Daten schreiben
			twi_s_write(OLED_CTRL_BYTE_DATA);
	
			// Daten schreiben
			twi_s_write(fontData7x8[c][cnt]);

			// Transfer beenden
			twi_s_stop();
			//**************************************
	}
	
	//********************************************************
	// write zero to next GDRAM Position (empty Pixel-Line)
	// Bus �ffnen
	twi_s_start(OLED_DEV_ADDR);
	
	// Daten schreiben
	twi_s_write(OLED_CTRL_BYTE_DATA);
				
	// Daten schreiben
	twi_s_write(0);

	// Transfer beenden
	twi_s_stop();
	//**************************************
}

/* Ende der HW-Abh�ngigen Funktionen						   */	
/***************************************************************/




/**************************************************************************/
/* Hier folgen die Bibliotheksfunktionen                                  */
/* Diese greifen ausschliesslich auf Low Level Funktionen zu              */

// Init the display controller
void display_Init(void)
{
	// Intitialisieren des Controllers	
	_hw_Init();
}

// L�scht das gesamt Display und setzt den Cursor oben links (Home)
// Schreibt spaces in das Memory
// Setzt den Cursor auf 0/0
// Updated das Display
void display_Clear(void)
{
	_hw_Clear();
}

// Den Cursor an die Position (0,0) verschieben
void display_Home(void)
{
	// Aufruf der Low-Level Funktion
	_hw_Home();
}

// Den Cursor an eine beliebige Position im Display verschieben
// Bei �berschreiten der Limite bleibt der Cursor unver�ndert
// X-Position von links nach rechts, y-Position von oben nach unten
// Z�hler beginnt bei 0
void display_Pos(unsigned char x, unsigned char y)
{
	if((x<DISP_COLS)&&(y<DISP_LINES))
	{
		// Aufruf der Low-Level Funktion
		_hw_Pos(x,y);
	}
}

// Ein Zeichen an der aktuellen Position ausgeben. 
// Aufruf der entsprechenden internen Funktion
// Das Zeichen im Speicher eintragen
// Wenn n�tig, scrollen
// Am Ende zeigen Pointer und HW-Pos auf das n�chste freie Zeichen, d.h.
// Die Inkrementierung und Pr�fung erfolgt am Ende
void display_CharToDisplay (uint8_t c)
{
	// Jetzt erfolgt die Ausgabe des Zeichens und die Speicherung im internen Mem
	_hw_CharToDisplay(c);
}


/* Ende der HW unabh�ngigen Bibliotheksfunktionen                                         */
/**************************************************************************/