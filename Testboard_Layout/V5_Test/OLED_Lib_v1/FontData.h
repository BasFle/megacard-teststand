
#define FONT_CHAR_FIRST 32
#define FONT_CHAR_LAST	129
#define FONT_CHARS 98
#define FONT_WIDTH 7
#define FONT_HEIGHT 8
#define FONT_SPACING 1

unsigned char fontDataRotated(unsigned char ixChar, unsigned char ixLine);

const unsigned char fontData7x8[FONT_CHARS][FONT_WIDTH];