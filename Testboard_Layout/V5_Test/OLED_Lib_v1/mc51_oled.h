/************************************************************/
/* Erste Version Display Treiber f�r das OLED Display       */
/*															*/
/* Autor: ZKS												*/
/*															*/
/*															*/
/* Versionsinfos:											*/
/* 10.5.2021, Initial release 		     					*/
/************************************************************/ 
#include <stdint.h>

// allgemeine defines
#define ASCII_CR 0x0d
#define ASCII_LF 0x0a

/* Defines f�r das OLED Display */
#define OLED_CTRL_BYTE_CMD  0b10000000
#define OLED_CTRL_BYTE_DATA 0b11000000

#define OLED_ROWS 8
#define OLED_COLS 16
#define OLED_PIXEL_X 128
#define OLED_PIXEL_Y 64

#define DISP_LINES 7
#define DISP_COLS 15
#define DISP_LEN (DISP_LINES*DISP_COLS)

// Konfigurieren des Displays und der Leitungen
void display_Init(void);

// Den Cursor an die Position (0,0) verschieben
void display_Home(void);

// Den Cursor an eine beliebige Position im Display verschieben
// Bei �berschreiten der Limite bleibt der Cursor unver�ndert
// X-Position von links nach rechts, y-Position von oben nach unten
// Z�hler beginnt bei 0
void display_Pos(unsigned char x, unsigned char y);

// Das Display l�schen und den Cursor an die Position (0,0) verschieben
void display_Clear();

// Ein Zeichen an der aktuellen Cursor-Position ausgeben und
// den Cursor um eine Stelle weiterr�cken. 
// Bei Erreichen der letzten Stelle wird das Display gescrollt.
void display_CharToDisplay (uint8_t c);


