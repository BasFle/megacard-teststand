# Megacard Teststand

Teststand für die MegacardV4 bzw. V5.

HWE Projekt für das Sommersemester 2022 der 4. Klassen.

## Teammitglieder

- Bastian Fleischer
- Julian Müller


## Inhaltsverzeichnis
[TOC]


## Verzeichniserklärung

| Datei                    | Inhalt                                                       |
| ------------------------ | ------------------------------------------------------------ |
| README.md         | Projektdokumentation                                         |
| Images                   | Dateien, welche in der Dokumenation verlinkt werden          |
| ATMEGA16_Testprogramme   | C-Programme (Microchip Studio Files), welche auf die Megacard geladen werden um dort den Testablauf durchzuführen |
| RaspberryPi_Verzeichnis  | Verzeichnis, welches für die Tests auf dem RaspberryPi verwendet wird. |
| RaspberryPi_Testprogramm | Testprogramm, welches auf dem RaspberryPi die Testabläufe steuert. |
| Testboard_Layout         | Layout des Testboardes (Eagle File), welches für die Tests zum Einsatz kommt |
| MegacardV4_Unterlagen    | Schaltplan und andere Unterlagen zur MegacardV4              |
| MegacardV5_Unterlagen    | Scahltplan und andere Unterlagen zur MegacardV5              |


# Projektrahmen
## Problemstellung 
In der HTL Rankweil kommt die MegacardV4 bzw. zukünftig die MegacardV5 zum Einsatz. Es handelt sich dabei um eine Microcontroller-Karte, ähnlich wie beispielsweise ein Arduino UNO.
Die Karte wird im Zuge des Fachpraktischen Unterrichtes in der 2. Klasse gefertigt und ab der 3. Klasse von den SchülerInnen verwendet.

Die Fertigung geschieht in der HTL durch SchülerInnen. Das Risiko, dass es zu Fehlfunktionen kommt ist, trotz stetiger Kontrollen während der Herstellung, entsprechend relativ hoch. Zusätzlich haben SchülerInnen der 2. Klasse noch kein Wissen über den Microcontroller/die Anwendung der Karte selbst, was die Fehlerwahrscheinlichkeit nicht reduziert. Weiterführend handelt es sich um die erste SMD-Bestückung, welche viele der SchülerInnen im laufe ihrer Ausbildung tätigen.

## Lösung/Zielsetzung
Um verifizieren zu können, ob eine gefertigte Megacard funktioniert, soll daher ein Teststand entwickelt werden, welcher es ermöglicht eine fertige Megacard auf Funktionsfähigkeit zu testen. Der Fokus liegt dabei auf die Peripherie, welche zum ATMEGA16, auf dem die Megacard basiert, aufgelötet wird und die Funktionalität der Megacard ausmacht.

Der Teststand soll zusätzlich dazu dienen, die nötigen Bootloader auf die zu testende Megacard zu spielen. 

## Rahmenbedingungen
Das Projekt findet im Zuge des Halbjahrprojektes der 4. Klassen im Sommersemester 2022 statt. Teammitglieder sind Bastian Fleischer und Julian Müller. Die zuständige Lehrperson ist Günther Jena.

Die Team-Zuteilung erfolgte am 21.02.22. Die Projektabgabe erfolgt am 24.06.22. Im Zeitraum dazwischen stehen 15 Unterrichtseinheiten zur Verfügung. Es soll sowohl ein Hardwareteil, als auch ein Softwareteil realisiert werden.

## Verwendete Tools / Hilfsmittel
| Tool                | Funktion                                   | 
| ------------------- | ------------------------------------------ | 
| [Obsidian](https://obsidian.md/)            | Markdowneditor zur Dokumentation           |      |
| [Microsoft Teams](https://www.microsoft.com/de-at/microsoft-teams/group-chat-software)     | Kommunikation zwischen den Teammitgliedern |      |
| [OneDrive](https://www.microsoft.com/de-at/microsoft-365/onedrive/online-cloud-storage)            | Schnittstelle zwischen den Teammitgliedern |      |
| [MicrochipStudio](https://www.microchip.com/en-us/tools-resources/develop/microchip-studio)     | C-Programmierung des Microcontrollers      |      |
| [Visual Studio Code](https://code.visualstudio.com/)  | Python Programmierung                      |      |
| [Eagle](https://www.autodesk.de/products/eagle/free-download)               | Leiterplattenentwicklung                   |      |
| [Spotify](spotify.com/)             | Unterhaltung                               |      |
| [draw.io](draw.io/)             | Zeichentool                                |      |
| Stefan Zudrell Koch | Viele nützliche Unterlagen zur Megacard v5 |      |
| [AVRDude](https://www.nongnu.org/avrdude/)             | Flashen der MegacardV5                     |      |
| [HID-Bootflash](http://vusb.wikidot.com/project:hidbootflash)       | Flashen der MegacardV4                     |      |


# Umsetzung
## Konzept
Als Herzstück des realisierten Teststandes dient ein **RaspberryPi**. Dieser steuert den Testablauf, welcher in einem **Python-Skript** umgesetzt wurde.

Der RaspberryPi bietet für das Testen einen guten Rahmen, da er die Kommunikation zwischen Nutzer und Teststand einfach über die Konsole ermöglicht. Außerdem gibt es in diesem Framework viele Tutorials und Beispiele. Gleichzeitig verfügt er über GPIO-Pins, SPI-Schnitstelle, I2C-Schnitstelle, ... .
Es ergibt sich jedoch das Problem, dass der RaspberryPi auf 3.3V Basis umgesetzt ist, während die Megacardv4 auf 5V Basis arbeitet ist.

Als Schnittstelle zwischen Megacard und RaspberryPi kommt unter anderem deswegen und um deutlich mehr GPIO-Pins zur Verfügung zu haben ein Adapterboard zum Einsatz, welches im Rahmen dieses Projektes entwickelt wurde und den Hardwareteil des Projektes darstellt.

![ ](Images/Pasted image 20220617145602.png)

Auf der zu testenden Megacard kommt ein Testprogramm zum Einsatz, welches ebenfalls im Zuge dieses Projektes realisiert wurde. Es kommunziert mit dem in Python realisierten Testprogramm, welches zeitgleich auf dem RaspberryPi läuft. Die beiden Programme zusammen sorgen so dafür, dass die Megacard Zustände einstellt (C-Programm). Der RaspberryPi überprüft, mithilfe des Testboards, ob der Zustand richtig hergestellt werden konnte, also ob die jeweilige Funktion der Megacard gegeben ist. 
Der RaspberryPi als Master gibt der Megacard das Signal zum nächsten Test (= Zustand) zu wechseln.

## Unterschiede zwischen Megacard v4 und v5
Der Teststand soll nach Möglichkeit so realisiert werden, dass die Megacard V5 getestet werden kann. Da diese sich jedoch immernoch in Entwicklung/Überarbeitung befindet, bzw. dies vorallem zu beginn des Semesters noch tat, wurde der Teststand so realisiert, dass die V4 und V5 getestet werden können. Konkret wurde der Teststand auf Basis der V52 entwickelt.

Die Tests für beide Versionen unterscheiden sich weitgehend nicht voneinander, es gibt jedoch einzelne Besonderheiten, welche berücksichtigt werden müssen. 

Die für dieses Projekt wichtigsten Unterschiede sind:

1. **Flashing**

Während die Megacardv4 noch auf einen HID-Bootloader setzt um Programme auf den Microcontroller zu spielen, verfügt die MegacardV5 nun über einen UART to USB Converter IC, welcher es ermöglicht das Flashen mittels AVR-Dude durchzuführen, ähnlich wie bei einem Arduino durchzuführen.
Entsprechend unterscheidet sich auch der Bootloader auf den Karten. Auf der V5 muss außerdem zustätzlich der Converter Chip getestet werden.

2. **Display**

Die MegacardV4 verfügt über einen Port, mithilfe dessen LCD-Displays angeschlossen werden können.
Die MegacardV5 verfügt über ein integriertes, I2C gesteuertes, OLED Display.

3. **Infrarot-Sender**

Die MegacardV5 verfügt nicht mehr über einen IR-Sender bzw. Empfänger, weshalb wir uns entschieden haben, diese Funktion auch auf der MegacardV4 nicht mehr zu testen.

4. **Buzzer**

Auf der MegacardV4 ist der Buzzer auf Pin PA4 verbunden. Auf der MegacardV5 auf Pin PB3.



Ein weiterer Unterschied ist die Betriebsspannung. Die MegacardV5 ermöglicht es durch setzten eines Jumpers zwischen 5V und 3.3V Betrieb umzuschalten. Diese Eigenschaft war für die Realisierung unseres Projektes jedoch nicht von Relevanz.

## Schaltplan MegacardV5

![ ](Images/Schaltplan_V5.png)

## Tests - Überlegung
Basierend auf den obigen Unterschieden und auf den Schaltplänen der Megacards haben wir entschieden folgende Funktionen/Tests zu realisieren:
1. **Kurzschluss**

Auf Anregung von Herr Jena, ist es sinnvoll, eine neue Megacard auf einen Kurzschluss o.Ä. zu testen. Daher wird beim ersten Verbinden getestet, ob die Stromaufnahme nicht zu hoch ist bzw. der Maximalstrom begrenzt, damit keine Bauteile beschädigt werden können.

2. **GPIO-Pin-Tests**

Die im Unterricht am häufigsten/regelmäßigsten verwendete Funktion der Megacard sind die GPIO-Pins.
Die Pins sollen daher auf ihre erfolgreiche IO-Fähigkeit getestet werden.

3. **LED-Funktionstest**

4. **Taster-Funktionstest**

5. **Potentiometer-Funktionstest**

Mithilfe des internen ADC der Megacard soll das Potentiometer auf seine Funktionsfähigkeit getestet werden.

5. **Buzzer-Funktionstest**

6. **JTAG-Stecker-Verbindung**

7. **ISP-Stecker-Verbindung**

8. **Display**

Das interne Display der MegacardV5 bzw. der Display-Port der MegacardV4 sollen durch Textausgaben getestet werden.


Ein Test welcher nur für die MegacardV5 gilt:

9. **USB-Schnittstelle**

Die MegacardV5 verfügt hierzu, wie erwähnt, über einen IC, welcher die UART-Schnittstelle in ein USB-Interface konvertiert. Dieser kann relativ einfach falsch aufgelötet werden, ebenso wie der Mini-USB-Stecker, weshalb auch dies getestet werden soll. Dazu soll nach Uploaden des Bootloaders auf die Megacard die Kommunikation mit dem RaspberryPi über die USB-Schnittstelle überprüft werden.


Durch diese Tests werden alle wesentlichen Funktionen der MegacardV4, sowie der MegacardV5 (mit Ausnahme der 3.3/5V Versorgung) getestet.

## Grundlegendes zu Bootloader und ISP 
Auf der Megacard befindet sich ein Stecker namens *ISP* (In System Programming).
Es handelt sich um einen Standartstecker, der es ermöglicht Mikrocontroller unter Anwendung der SPI-Schnittstelle zu programmieren.

Programmiert man den ATMEGA16 der MegacardV4/V5 über diesen ISP-Stecker, wird der Bootloader überschrieben. Der Bootloader ist jenes Programm, welches beim Power-On des Mikrocontrollers ausgeführt wird. Auf der Megacard dient dieser eigentlich dazu, es zu ermöglichen Programme über die USB-Schnittstelle auf den Mikrocontroller zu laden und diese dort auszuführen.

## Ausnutzen der ISP-Schnittstelle
Um unseren Teststand zu programmieren ist es nötig, das Testprogramm auf das Testboard zu spielen. Außerdem müssen wir davon ausgehen, dass die AVRs aus der Fertigung kommen und noch nicht über einen Bootloader verfügen.

Die Idee ist daher zuerst unser Testprogramm, welches mit dem RaspberryPi kommuniziert um die Megacard zu testen, per ISP auf den Mikrocontroller zu spielen. Nach Abschluss der Tests wird die ISP-Schnittstelle verwendet um den Bootloader auf den Mikrocontroller zu spielen.

## Kommunikation zwischen RaspberryPi und Megacard
Damit ein Testablauf zwischen RaspberryPi und Megacard erfolgen kann, muss im wesentlichen der RaspberryPi der Megacard sagen, dass sie xy tun soll. Die Megacard tut das angeforderte bzw. probiert dies. Der RaspberryPi überprüft, ob sich die erwarteten Zustände einstellen und kommunziert der Megacard, dass sie den nächsten Zustand einstellen soll, wenn der Test erfolgreich war.

Um dies umzusetzen, wird in diesem Projekt sowohl im RaspberryPi mittels Python, als auch auf der Megacard mittels C, der selbe Ablauf programmiert. Die Kommunikation zwischen Megacard und RaspberryPi erfolgt über PD2 und PD3, da diese Ports auf der Megacard ansonsten keine Verwendung haben. Mit Kommunikation ist kein komplizierter Kommunikationsablauf gemeint, sondern ein simpler High/Low als Hinweis für den jeweils anderen, zum nächsten Testschritt zu wechseln.

![ ](Images/Kommunikationsablauf.png)


# Das Testboard (Hardware)
Das Testboard dient im wesentlichen dazu, die Verbindung zwischen Megacard-GPIO und RaspberryPi herzustellen.

## Layout

![ ](Images/Pasted image 20220617155950.png)

1. RaspberryPi-Steckverbindung
2. ISP und JTAG-Stecker zur Verbindung mit der Megacard über Kabel
3. Megacard-Steckerverbindung
4. Pegel-Converter zwischen RaspberryPi 3.3V und Megcard/Expander 5V

Diese werden benötigt, um zwischen I2C des RaspberryPis, sowie I2C der Port-Expander, und zwischen ISP-Stecker der Megacard und RaspberryPi, kommunizieren zu können.

5. 3x I2C-Port-Expander

Zum Einsatz kommt MCP23017 in der THT-Ausführung. Dieser verfügt über 16 GPIO-Pins.
Die THT-Ausführung wurde Aufgrund von Lieferschwierigkeiten gewählt, da SMD-Bauformen nicht zeitnahe zur Verfügung standen.

6. Status LEDs

Versorgung des Testboards (links)
Versorgung der Megacard (rechts)

-> Die rechte LED erlischt, wenn auf dem zu testenden Board ein Kurzschluss vorhanden ist

7. 2 Taster

Wobei der linke Taster S2 und der rechte Taster S1 darstellt. Die Taster dienen dazu, mit dem Nutzer während des Testablaufes zu kommunizieren.

8. Rückstellbare Sicherung

Die Sicherung unterbricht die Stromversorgung für die zu testende Hardware, wenn mehr als 200mA an Strom fließen.

## Erklärung
Das Layout ist nicht sonderlich kompliziert. Herzstück sind die 3-Port-Expander, welche die gesamte Kommunikation zwischen RaspberryPi und Megacard-Pins bewerkstelligen. Die Expander ermöglichen es, durch die I2C-Schnittstelle des RaspberryPis alle Pins der Megacard einzeln anzusteuern.

Als Verbindung zwischen RaspberryPi (3.3V) und den Expandern (5V) kommen die Pegelconverter zum Einsatz. Ebenso zur Verbindung zwischen RaspberryPi SPI-Schnittstelle des RaspberryPis und dem ISP-Stecker der Megacard, da dieser später zum Flashen der Megacard dient und daher direkt verbunden sein muss.
Als Pegelconverter kommt eine simple, bidirektionale Schaltung zum Einsatz, welche das Pegelkonvertieren in beide Richtungen ermöglicht:

![ ](Images/Pasted image 20220618105354.png)


Um den Teststand, bzw. die angeschlossene Megacard im Falle eines Kurzschlusses, welcher nach der Fertigung gegeben sein könnte, vor zu großen Strömen zu schützen, kommt eine rückstellbare Sicherung zum Einsatz. Diese sorgt dafür, dass beim Anschließen der Megacard maximal 200mA Strom fließen können.


# Der Testablauf (Software)
## Grundlegendes
*Hinweis*: Wir sind beide in der Python-Programmierung relativ unerfahren. Daher hat uns dies teilweise vor Herausforderungen gestellt. Jedoch ist Python sehr anfängerfreundlich, was die Umsetzung vereinfacht hat. Als Hilfestellung hat das Buch "Python für Dummies", sowie diverse Websites gedient.

Da sehr Hardwarenahe gearbeitet wird, ist es nötig mit 8-Bit-Binärdarstellungen Bitoperationen, ... durchführen zu können. Als Hilfstool kommt hierzu *numpy* zum Einsatz.

Wie bereits erwähnt kommunizieren die Megacard und der RaspberryPi durch die Pins PD2 und PD3 miteinander. Das ganze ist sieht im Konzept so aus, dass RaspberryPi bzw. Megacard PD2 oder PD3 auf High ziehen, dort, im Falle des RaspberryPis für 150ms belassen und anschließend auf Low setzen.
Der jeweils andere Wartet auf genau dies, und weiß dadurch, wann ein Test abgeschlossen ist und der nächste Zustand ausgegeben/eingelesen werden soll.

Im RaspberryPi-Programm gibt es dazu die Funktion `PD2_Blink`.
Im ATMEGA16-Programm gibt es dazu die Funktion `wait_for_PD2_H_L`, welches auf dieses High-Low wartet, und anschließend erst im Programm weiterläuft.

So kann sichergestellt werden, dass die Megacard und der RaspberryPi sich im gleichen "Tempo" durch das Programm bewegen und sich immer an der gleichen Stelle des Testablaufes befinden.

Grundsätzlich kann PD2/PD3 hier auf gewisse Weise mit einem Takt verglichen werden, der dem anderen Signalisiert, zum nächsten Schritt zu wechseln.
Der Delay ist mit 150ms standardmäßig relativ hoch gewählt, jedoch garantieren wir so, dass der Pegel sicher erkannt wird und es ist zu beachten, dass unser Teststand keine Zeitkritische Anwendung ist. Daher: Wenn ein Testablauf 5 Sekunden länger dauert ist dies auch egal, da die menschliche Interaktion immer langsamer sein wird als unser Test.

## Unterteilung
Das Programm kann grundsätzlich in 3-Abschnitte und eine Klasse unterteilt werden:

1. MegacardPort-Klasse
2. Verbindungsherstellung und Flashen
3. Testabläufe
4. Beenden der Tests und programmieren des Bootloaders


Während aller Testschritte kann man das Programm  als 3-Instanzen betrachten: Nutzer, Megacard, RaspberryPi. Zwischen den Instanzen gibt es jeweils Wege der Verbindung bzw. Kommunikation:

```mermaid
sequenceDiagram
	participant Nutzer;
	participant RaspberryPi;
	participant Megacard;
	Nutzer ->> RaspberryPi: Kommunikation durch S2 und S1
	Nutzer ->> Megacard: Kontrolle durch sehen/hören und physikalischer Zugriff auf Verbindungen
	RaspberryPi ->> Nutzer: Ausgaben und Aufforderungen per Terminal
	RaspberryPi ->> Megacard: Verbindung durch Testboard
	Megacard ->> RaspberryPi: 
	RaspberryPi ->> Megacard: Kommunikation durch PD2 und PD3
	Megacard ->> RaspberryPi: 
	Megacard ->> Nutzer: Megacard LEDs, anschlossenes Display und Buzzer
```

## 1. MegacardPort - Klasse
### Intro
Die Port-Expander müssen über die I2C-Schnittstelle angesprochen werden. Jeder Expander verfügt über 16-Pins, welche in PortA und PortB (jeweilige 8-Bit Register) aufgeteilt sind.

Im Layout wurde darauf geachtet, dass ein Expander Port, mit je einem Port der Megacard verbunden wird. Dadurch lassen sich beide quasi identisch ansprechen. Daher wurde hier eine Klasse realisiert, welche das Ansprechen mittels *DDR*, *PIN*, *Port* Register noch ähnlicher zum Programmieren eines Ports wie auf der Megacard.

### Klassendiagramm
```mermaid
classDiagram
class MegacardPortKlasse
MegacardPortKlasse : char expanderI2CAdresse
MegacardPortKlasse : char a_or_b
MegacardPortKlasse : bool inv
MegacardPortKlasse : MegacardPortKlasse(expanderI2CAdress, a_or_b = 'a', inv = True)
MegacardPortKlasse : char get_Pin()
MegacardPortKlasse : set_DDR(value)
MegacardPortKlasse : char get_DDR()
MegacardPortKlasse : set_Port(value)
MegacardPortKlasse : char get_Port()
```

### Erläuterung Funktionen
**Konstruktor**

Die Initialisierung der Klasse erfordert das Übergeben von 2 (optional 3) Parametern.

Erstens wird die **I2C-Adresse des Port-Expanders**, welcher angesprochen wird übergeben.
Als zweites das Keyword **a_or_b** = 'a' bzw. a_or_b = 'b'. Hier wird angegeben, ob Ausgangsport A bzw. B des Port-Expanders angesprochen werden muss.
Als drittes wird das Keyword **inv** = True bzw. inv = False übergeben, da für manche Ports im Layout, die Reihenfolge der Pins umgekehrt zur Reihenfolge der Pins der Megacard ist. Bei True wird daher, die Reihenfolge der übergebenen Bits immer invertiert.

**get_Pin**

Diese Funktion ist identisch zum Keyword `PINX` auf der Megacard. Es wird verwendet, um den an einem Eingang anliegenden Spannungswert einzulesen und zurückzugeben.

**set_DDR/get_DDR**

Auch dieses Register ist identisch zur Megacard. Es setzt die IO-Richtung der Pins fest.
'1' = Output, '0' = Input

**set_Port/get_Port**

Auch dieses Register ist ähnlich zur Megacard. Ist der Port als Ausgang definiert, werden hier die Pegel ('1' = High, '0' = Low) festgelegt, welche an den Pins ausgegeben werden.
Unterschied zum ATMEGA16-Programmieren, dass hier keine Pull-Up-Widerstände gesetzt werden können.

### Anwendung im Programm
Zur Anwendung kommt die Klasse um 5-Objekte zu definieren. Einerseits die Ports A-D und einen JTAG-Port. Die Ports A-D sind, wie der Name bereits vermuten lässt direkt mit den entsprechenden Megacard Ports verbunden bzw. direkt so anzusprechen.
Der JTAG-Port dient dazu, die relevanten Pins des JTAG-Steckers testen zu können, da dieser als eigener Port-Expander-Port ausgelegt ist.

```python
PortA = MegacardPort(PortExpander_Megacard_PortAC, a_or_b='b')  
PortB = MegacardPort(PortExpander_Megacard_PortBD, a_or_b='b')  
PortC = MegacardPort(PortExpander_Megacard_PortAC, a_or_b='a', inv=True)  
PortD = MegacardPort(PortExpander_Megacard_PortBD, a_or_b='a', inv=True)  
JTAG = MegacardPort(PortExpander_Megacard_Misc, a_or_b='b')
```

Die Anwendung würde nun so funktionieren (einige Beispiele):


```python
#Setzen von PD2 auf High, Ausgang
PortD.set_DDR((1<<PD2))
PortD.set_PORT((1<<PD2))

#Lesen von PD3 als Input
PortD.get_PIN() & (1<<PD3)

#PortC auf 1010 1010 setzten
PortC.set_DDR(0xff) #Alles auf Ausgang setzten
PortC.set_PORT(0b10101010)
```


## 2. Verbindungsaufbau
Den ersten Teil des Testprogrammes stellt der Hardwaremäßige-Verbindungsaufbau zwischen RaspberryPi, Testboard und Megacard dar.

Dazu wird der Nutzer der Reihe nach aufgefordert, das Testboard, die Megacard, gewisse Kabel, ... zu verbinden. Hat der Nutzer die getätigte Verbindung durchgeführt, so bestätigt er dies durch Drücken der Tasten auf dem Testboard.

Konkret sieht der Ablauf wie folgt aus:

1. **Testboard anschließen**

Im ersten Schritt muss das Testboard mit dem RaspberryPi verbunden werden. Dazu wird der Nutzer per Terminal-Ausgabe aufgefordert. Ist die Aktion beendet, bestätigt der Nutzer durch Drücken von S2. Mit S1 kann das Programm hier noch abgebrochen werden.
Bestätigt der Nutzer, dass, das Testboard verbunden ist, wird die Funktionsfähigkeit der I2C-Kommunikation mit den Port-Expandern überprüft. Gleichzeitig dient dies als Test, ob das Testboard angeschlossen wurde.

Python-Code dazu:

```python
print("Connect the testboard to the raspberrypi!")  
  
#Der Code wartet, bis die 3 I2C-Adressen der Port-Expander über die I2C-Schnittstelle erkannt werden. Werden diese erkannt, wird das Testboard als angeschlossen angenommen.

#subprocess.Popen ermöglicht es, aus dem Python-Skript heraus Kommandozeilen-Commands auszuführen. In diesem Fall, um Geräte an der I2C-Schnittstelle zu erkennen

line = ""  
while not ('20 21 -- 23' in line):  
    p = subprocess.Popen(['i2cdetect', '-y', '1'], stdout=subprocess.PIPE, )  
    line = str(p.stdout.readlines())  
    if (not '20 21 -- 23' in line):  
        print("ERROR! You must connect to the testboard!")  
  
Clear_Console()  
print("Testboard connected.")
```

*Hinweis: Nachfolgend ist nicht immer Python-Code aufgeführt. Wenn der Code-Interessant ist, kann dieser in der Datei betrachtet werden. Hier aufgeführt sind nur beispielhaft einzelne Code-Ausschnitte. Der Gesamte Code im Python-Skript beläuft sich auf etwa 900 Zeilen, das Megacard-C-Programm hat insgesamt etwa 600 Zeilen.*

2. **Megacard anschießen**

Im zweiten Schritt muss an das Testboard eine Megacard angeschlossen werden. Auch hier wird wieder der Nutzer dazu aufgefordert. Ist die Verbindung hergestellt, bestätigt der Nutzer dies.
Softwareseitig wird überprüft, ob zu viel Strom fließt (> 200mA). Daher: ob es einen groben Fehler in der Herstellung der Megacard gegeben hat.

3. **ISP-Kabel anschließen**

Im dritten Verbindungsschritt wird der Nutzer aufgefordert das ISP-Kabel anzuschließen. Außerdem muss der PROG-Jumper auf der Megacard gesetzt werden. Er bestätigt dies. Softwareseitig wird überprüft, ob eine Kommunikation mit dem Chip möglich ist. Einerseits kann dies an einer falschen Verbindung liegen, andererseits daran, dass der ATMEGA16 auf der Megacard nicht ordentlich aufgelötet ist bzw. gar nicht funktioniert oder auch an einem Problem mit dem ISP-Stecker auf der Megacard.

```python
print("Bitte eine Megacard ueber ISP anschliesen.\nProg Jumper setzten!")  
ReadyOrCancle()  #Definierte Unterfunktion, die bei Drücken von S2 im Programm weiterläuft, bei S1 wird das Programm abgebrochen

#Das Programm ermöglicht das Versuchen der Verbindung mit der Megacard unendlich lange, bzw. bis es erfolgreich ist, oder nach einem fehlgeschlagenen Versuch durch Drücken von S1 abgebrochen wird.
while True:  
    output = subprocess.run('sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 - U flash:r:"dev/null":R',  
                            shell=True, capture_output=True).stderr  
    output = str(output)  
  
    if ("initialized" in output):  
        break  
    else:  
        print("Error. Connect and try again! Set the PROG Jumper!")  
        ReadyOrCancle()
```

4. **JTAG-Kabel anschließen**

Im letzten Verbindungsschritt wird der Nutzer aufgefordert das JTAG-Kabel anzuschließen. Er bestätigt auch dies durch Drücken eines Tasters.

Bis zu diesem Zeitpunkt hat der Nutzer bei einer Aufforderung auch immer die Möglichkeit den Test abzubrechen.

5. **Auswahl der Megacard Version**

Da es leichte Unterschiede zwischen den Megacards gibt, muss der Nutzer in diesem Schritt durch Drücken von S1 bzw. S2 auswählen, ob eine Megacard V4 oder V5 getestet wird.

6. **Testproramm laden**

Über die ISP-Schnittstelle wird das Testprogramm auf die Megacard geladen. Ist dies nicht erfolgreich, kann der Nutzer den Test abbrechen oder das Ganze erneut probieren. 

```python
print("Loading testprogramm to Device...\n")  

# Basierend auf der Megacard-Version wird nachfolgend das Testprogramm auf die Megacard geladen (über die ISP-Schnittstelle).
#Schlägt das Ganze fehl, kann der Nutzer durch S2 erneut versuchen, durch S1 den Testlauf abbrechen

while True:  
    output = subprocess.run(  
        f'sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 -U flash:w:"/home/pi/testand/v{megaVer}Programm.hex":i',  
        shell=True, capture_output=True).stderr  
    output = str(output)  
  
    if ("error" in output):  
        print("ERROR. Could not load program. Let the PROG-Jumper connected!")  
        ReadyOrCancle()  
    else:  
        break
```

7. **Testablauf starten**

Nun muss der Nutzer den PROG-Jumper wieder entfernen. Anschließend kann der Test durch Drücken von S1 oder S2 gestartet werden.

8. **Verbindungsherstellung**

Als erster Schritt der Tests dient die Kommunikation zwischen RaspberryPi und Megacard. Diese erfolgt später über PD2 und PD3, weshalb hier die Funktionsfähigkeit dieser beiden Pins auf Ein-/Ausgabe getestet wird. Funktioniert diese Verbindung wird mit den eigentlichen Tests losgelegt.

Der Ablauf sieht wie folgt aus: 

```mermaid
sequenceDiagram
	participant RaspberryPi;
	participant Megacard;
	Megacard->>RaspberryPi: PD3 High
	RaspberryPi->>Megacard: PD2 High
	Megacard ->>RaspberryPi: PD3 Low
	RaspberryPi ->>Megacard: PD2 Low
	Megacard->>RaspberryPi: PD2 High
	RaspberryPi->>Megacard: PD3 High
	RaspberryPi-->Megacard: Verbindung hergestellt.
```

Ab diesem Zeitpunkt gehört eine Python-Code-Sektion immer zu einer C-Programm-Sektion, welche auf der Megacard ausgeführt wird.

Im Python-Code wird die zeitliche Komponente berücksichtigt. Wir nach 5 bzw. 2 Sekunden der erwartete Zustand von der Megacard nicht eingestellt, so wird der Test als fehlgeschlagen bewertet.

Python-Programm:

```Python
print("\nPD Connection Establishment...")  
 
#Wartet maximal 5 Sekunden lang auf PD3
t_end = time.time() + 5  # tries to connect for 5 seconds  
  
while time.time() < t_end:  
    if PortD.get_Pin() & (1 << PD3):  
        break  
  
if (time.time() > t_end):  
    print("Could not establish connection with PortD")  
    End_Tests(Errors.PortD_Connection_Error)  
  
#PD3 erhalten, setzt PD2
PortD.set_DDR((1 << PD2))  
PortD.set_Port((1 << PD2))  
  
#Wartet maximal 5 Sekunden lang auf Löschen von PD3
t_end = time.time() + 5  
while time.time() < t_end:  
    if (not (PortD.get_Pin() & (1 << PD3))):  
        break  
  
if (time.time() > t_end):  
    print("Could not establish connection with PortD")  
    End_Tests(Errors.PortD_Connection_Error)  
  
#PD3 erhalten, löscht PD2
PortD.set_Port(PortD.get_Port() & ~numpy.ubyte(1 << PD2))  
  
#Wartet maximal 2 Sekunden lang auf PD2
t_end = time.time() + 2  
while time.time() < t_end:  
    if (not (PortD.get_Pin() & (1 << PD2))):  
        break  
  
if (time.time() > t_end):  
    print("Could not establish connection with PortD")  
    End_Tests(Errors.PortD_Connection_Error)  
 
#Setzt PD3
PortD.set_DDR(PortD.get_DDR() | (1 << PD3))  
PortD.set_Port(PortD.get_Port() | (1 << PD3))  
print("Connected\n")
```

C-Progamm:

```C
case EstablishConnection:
		DDRD |= (1<<PD3);	//Setzt PD3
		PORTD |= (1<<PD3);	

		wait_for_PD2_H();	//Wartet auf PD2 High

		PORTD &= ~(1<<PD3);	//Löscht PD3

		wait_for_PD2_L();	//Wartet auf PD2 Low

		DDRD = 0x01;		//Setzt PD2
		PORTD = 0x01;

		while(!(PIND & (1<<PD3))){}	//Wartet auf PD3 High

		cur_Case = PortD_Test;
break;
```

## 3. Testabläufe
Nach dem die Verbindung und Kommunikation zwischen Megacard und RasberryPi funktioniert, kann der eigentliche Testablauf gestartet werden.

Schlägt ein Test fehl, wird zum Beenden der Tests gewechselt.

Der Testablauf ist wie folgt:

1. **GPIO-Tests**

Die Ports D,C,B,A werden auf Ausgeben von Signalen und einlesen von Signalen getestet. Der Test erfolgt einmal für Ausgeben und einmal für Einlesen, wobei erst alles auf Low, anschließend alles auf High, dann das Muster 1010 1010 und abschließend das Muster 0101 0101 ausgegeben/eingelesen wird. Bei PortD muss beachtet werden, dass PD2 und PD3 nicht im Test verwendet werden, da diese unter 1. bereits getestet wurden. 

Dieser Test wird hier beispielhaft noch mit den Code-Abläufen vereinfach für das Testen von PortD dargestellt. Nachfolgende Tests sind prinzipiell gleich aufgebaut, der Code wird nicht mehr angeführt. Er ist in den jeweiligen Dateien zu finden.

Nachfolgend folgt der Python Code. 

Es gibt einmal die Output-Tests, wobei die Megacard ein Muster ausgibt. Der RaspberryPi wartet auf dieses Muster. Wird das Muster erfolgreich erkannt, wird PD2 kurz gesetzt und anschließend wieder gelöscht. Die Megacard wartet auf das setzten von PD2 und stellt dann den nächsten Zustand ein.

```mermaid
sequenceDiagram
	participant RaspberryPi;
	participant Megacard;
	RaspberryPi ->> Megacard: PD2 High;
	RaspberryPi --> Megacard: 0.5 Sekunden delay;
	RaspberryPi ->> Megacard: PD2 Low;
	note right of Megacard: Zustand 1 einstellen
	note left of RaspberryPi: wartet auf Megacard Zustand 1
	Megacard ->> RaspberryPi: Zustand 1
	note right of Megacard: wartet auf PD2 H/L
	RaspberryPi ->> Megacard: PD2 High
	RaspberryPi --> Megacard: 0.5 Sekunden delay
	RaspberryPi ->> Megacard: PD2 Low
	note right of Megacard: Zustand 2 einstellen
	Megacard --> RaspberryPi: Zustand 2
	Megacard --> RaspberryPi: ...
```

Python-Code dazu: 

```python
print("PortD Test...")

PortD.set_DDR((1 << PD2))  # PD2 output rest input  
PortD.set_Port(0x00)  	   # PD2 low setzten

#Output-Tests
#Megacard gibt Werte aus, RaspberryPi liest Werte ein

#Gesamter Port auf LOW
print("\tLow Test...")   

#Wartet maximal 1 Sekunde lang auf das Einstellen des Zustands
t_end = time.time() + 1  
while time.time() < t_end:  
    if (PortD.get_Pin() == 0b00000000):  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  

	
print("\tSucessful\t" + bin(PortD.get_Pin()))  
PortD.set_Port(0b00000100)  #PD2 High, wenn der Test erfolgreich war, damit die Megacard zum nächsten Zustand wechselt
time.sleep(0.5)  
PortD.set_Port(0b00000000)  #PD2 wieder auf Low
  
print("\tHigh Test...")  
#Gesamter Port auf High

#Wartet maximal 1 Sekunde lang auf das Einstellen des Zustands
t_end = time.time() + 1  
while time.time() < t_end:  
    if (PortD.get_Pin() == 0b11111011):  #nicht 0b1111 1111, da PD2 für Kommunikation verwendet wird und bereits getetstet ist
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
  
PortD.set_Port(0b00000100)  #PD2 High, wenn der Test erfolgreich war, damit die Megacard zum nächsten Zustand wechselt
time.sleep(0.5)  
PortD.set_Port(0b00000000)  #PD2 wieder auf Low 
  
print("\tHigh-Low-Pattern Test...") 
#Muster 1010 1010 wird auf Port ausgegeben

#Wartet maximal 1 Sekunde lang auf das Einstellen des Zustands 
t_end = time.time() + 1  
while time.time() < t_end:  
    if (PortD.get_Pin() == 0b10101010):  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
  
print("\tSucessful\t " + bin(PortD.get_Pin()))  
PortD.set_Port(0b00000100)  #PD2 High, wenn der Test erfolgreich war, damit die Megacard zum nächsten Zustand wechselt
time.sleep(0.5)  
PortD.set_Port(0b00000000)  #PD2 wieder auf Low 
  
print("\tLow-High-Pattern Test...") 
#Muset 0101 0101 wird ausgegeben

#Wartet maximal 1 Sekunde lang auf das Einstellen des Zustands  
t_end = time.time() + 1  
while time.time() < t_end:  
    if (PortD.get_Pin() == 0b01010001):  # PD2 is communication therefore not set  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
  
print("\tSucessful\t" + bin(PortD.get_Pin()))  
PortD.set_Port(0b00000100)  #PD2 High, wenn der Test erfolgreich war, damit die Megacard zum nächsten Zustand wechselt
time.sleep(0.5)  
PortD.set_Port(0b00000000)  #PD2 wieder auf Low 
```

C-Code dazu:

```c
//Port-Tests
			case PortD_Test:
			
			//Output Tests
			DDRD = ~(1<<PD2); //PD2 is Input, rest is output
			
			//All Low Test
			PORTD = 0x00;	  //Alles Low ausgeben
			wait_for_PD2_H(); //Auf PD2 warten

			//All High Test
			wait_for_PD2_L();	//Auf Löschen von PD2 warten
			PORTD = 0xFF;		//Alles High ausgeben
			wait_for_PD2_H(); 	//Auf PD2 warten
			
			//High Low Test A
			wait_for_PD2_L();	//Auf Löschen von PD2 warten
			PORTD = 0xAA; 		//1010 1010 ausgeben
			wait_for_PD2_H(); 	//Auf PD2 warten
			
			//High Low Test B
			wait_for_PD2_L();	//Auf Löschen von PD2 warten
			PORTD = 0x51; 		//0101 0101 ausgeben
			wait_for_PD2_H(); 	//Auf setzten von PD2 warten
```

Den zweiten Teil der GPIO-Tests sind die Input Tests. Dabei gibt der RaspberryPi ein Signal aus. Die Megacard liest das Signal aus und gibt bei erfolgreichem Einlesen PD2 High für 0.25 Sekunden aus. Anschließend löscht die Megacard PD2 wieder. Der RaspberryPi stellt dann den nächsten Zustand ein.

```mermaid
sequenceDiagram
	participant RaspberryPi;
	participant Megacard;
	Megacard ->> RaspberryPi: PD2 High;
	Megacard --> RaspberryPi: 0.25 Sekunden delay;
	Megacard ->> RaspberryPi: PD2 Low;
	note left of RaspberryPi: Zustand 1 einstellen
	note right of Megacard: wartet auf Megacard Zustand 1
	RaspberryPi ->> Megacard: Zustand 1
	note right of Megacard: wartet auf PD2 H/L
	Megacard ->> RaspberryPi: PD2 High
	Megacard --> RaspberryPi: 0.25 Sekunden delay
	Megacard ->> RaspberryPi: PD2 Low
	note left of RaspberryPi: Zustand 2 einstellen
	RaspberryPi --> Megacard: Zustand 2
	RaspberryPi --> Megacard: ...
```

Python-Code:

```Python
# Input Tests
# RaspberryPi gibt Pegel aus, die Megacard liest die Pegel ein

PortD.set_DDR(invert_as_uint8((1 << PD2)))  # PD2 is Input, Rest is output  
 
#Zustand 1
# 0x00 wird ausgegeben
print("\tLow-Input Test...")  
t_end = time.time() + 2  #wartet maximal 2 Sekunden auf eine Antwort, ansonsten ist der Test fehlgeschlagen
PortD.set_Port(0x00)  #Alles auf Low wird ausgegeben  
#Die Megacard bestätigt, wenn der Zustand erfolgreich gelesen wurde durch setzten von PD2 auf high 

while time.time() < t_end:  
    if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
print("\tSucessful")  
  
time.sleep(0.25)  
  
#Nächster Zustand
print("\tHigh-Input Test...")  
t_end = time.time() + 2  #wartet maximal 2 Sekunden auf eine Antwort, ansonsten ist der Test fehlgeschlagen
PortD.set_Port(0xFF)  # Alles High wird ausgegeben

#Die Megacard bestätigt, wenn der Zustand erfolgreich gelesen wurde durch setzten von PD2 auf high   
while time.time() < t_end:  
    if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
print("\tSucessful")  
  
time.sleep(0.25)  
  
#Nächster Zustand
print("\tHigh-Low-Input Test...")  
t_end = time.time() + 2  #wartet maximal 2 Sekunden auf eine Antwort, ansonsten ist der Test fehlgeschlagen
PortD.set_Port(0b10101010)  # Es wird 1010 1010 ausgegeben 
  
#Die Megacard bestätigt, wenn der Zustand erfolgreich gelesen wurde durch setzten von PD2 auf high 
while time.time() < t_end:  
    if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
  
print("\tSucessful")  
  
time.sleep(0.25)  
  
#Nächster Zustand
print("\tLow-High-Input Test...")  
t_end = time.time() + 2  #wartet maximal 2 Sekunden auf eine Antwort, ansonsten ist der Test fehlgeschlagen
PortD.set_Port(0b01010101)  # Es wird 0101 01010 ausgegeben
  
#Die Megacard bestätigt, wenn der Zustand erfolgreich gelesen wurde durch setzten von PD2 auf high 
while time.time() < t_end:  
    if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2  
        break  
  
if (time.time() > t_end):  
    print("Failed!")  
    End_Tests()  
print("\tSucessful")  
# endregion InputTests  
  
print("PortD Test Sucessful\t.\n")  
```

C-Code dazu:

```c
		//Input Tests
		DDRD = (1<<PD2); //PortD is input except PD2
		PORTD = 0x00;    //No Pull-Up & PD2 low

		//All-Low-Test - Wartet auf Zustand 1
		//Wartet darauf, dass alles Low ist
		_delay_ms(25);

		while((PIND&(~(1<<PD2))) != 0x00){}	//Wartet bis alles Low anliegt
		PORTD = (1<<PD2); 					//Setzt PD2 


		//All-High-Test - Wartet auf Zustand 2
		//Wartet darauf, dass alles gesetzt ist
		_delay_ms(25);
		PORTD = 0x00;						//Löscht PD2 wieder

		while((PIND&(~(1<<PD2))) != 0xFB){}	//Wait for all High read (except PD2)
		PORTD = (1<<PD2); 					//Setzt PD2


		//High-Low-Test - Wartet auf Zustand 3
		//Wartet auf das Muster 10101010
		_delay_ms(25);
		PORTD = 0x00;	//Löscht PD2 wieder
		while((PIND&(~(1<<PD2))) != 0xAA){}	//Wait for 0x1010 1010 = 0xAA
		PORTD = (1<<PD2); //Sets PD2


		//Low-High-Test - Wartet auf Zustand 4
		//Wartet auf das Muster 01010101
		_delay_ms(25);
		PORTD = 0x00;
		while((PIND&(~(1<<PD2))) != 0x51){}	//Wait for 0x0101 0001 = 0x51
		PORTD = (1<<PD2); //Sets PD2

		_delay_ms(25);

		DDRD = 0x00; //DDRD is input
		PORTD = 0x00;
		cur_Case = PortA_Test;
		break;
```

2. **LED-Tests**

Mit Timer0 wird auf den LEDs ein Lauflicht erzeugt, so dass, jede LED einzeln aufleuchtet. Die Bestätigung, ob/dass dies funktioniert hat, gibt der Nutzer. Anschließend werden alle LEDs eingeschaltet und der Nutzer bestätigt, ob diese hell aufleuchten.

Damit der Nutzer bestätigen kann, ob der Zustand erfolgreich eingestellt wurde, dienen die Tasten S2 und S1 auf dem Testboard. Wird S2 (linker Taster) gedrückt, so wird der Test als erfolgreich gewertet. Wird S1 gedrückt, wird der Test als fehlgeschlagen gewertet.

3. **Taster-Tests**

Der Nutzer drückt der Reihe nach die Tasten 0 - 3. Werden diese erfolgreich erkannt, ist der Test erfolgreich. Werden diese nicht erkannt ist der Test fehlgeschlagen. Der Nutzer hat für jede Taste 12 Sekunden Zeit. Ist in dieser Zeit die Taste nicht gedrückt worden, ist der Test fehlgeschlagen.

4. **Buzzer-Test**

Mittels Timer0 wird ein Rechteck-Signal von etwa 350 Hz erzeugt. Dieses wird auf den Buzzer-Pin (PA4 - MegaV4, PB3 - MegaV5) ausgegeben. Der Nutzer muss den Buzzer-Jumper gesetzt haben. Ein erfolgreicher Test wird vom Nutzer durch Tastendruck bestätigt.

5. **ADC-Test**

Um den ADC, sowie das Potentiometer der Megacard zu testen, wird ein Wert ADC-Wert eingelesen. Dieser wird auf den LEDs von PortC in binärer Darstellung ausgegeben. Der Nutzer kann am Potentiometer drehen. Der Wert auf den LEDs sollte sich von 0 bis 255 verändern. Ein erfolgreicher Test wird vom Nutzer durch Tastendruck bestätigt.

6. **JTAG-Tests**

Der JTAG-Stecker wird auf Verbindung getestet, indem alle Pins, die keinen fixen Pegel haben der reihe nach einzeln auf High geschaltet werden.

7. **Display-Test**

Im Falle der MegacardV5 wird auf dem integrierten Display ein Text ausgegeben. Dieser kann vom Nutzer gelesen werden und dadurch die Funktionsfähigkeit des Display bestätigt.
Im Falle der MegacradV4 muss der Nutzer über den dafür vorgesehenen Stecker ein Display an die Megacard anschließen. Auch dort wird ein Text ausgegeben, wenn der Nutzer diesen lesen kann, ist die Funktionsfähigkeit des Displays bestätigt.

8. **USB-Test**

Dieser Test wird nur im Falle der MegacardV5 durchgeführt. Er geschieht nicht nach dem Display-Test, sondern erst nach beenden des Tests. An dieser Programmstelle befindet sich auf der Megacard bereits wieder der Bootloader.
Dieser ermöglicht über den dafür vorgesehenen IC eine Kommunikation per USB-Schnittstelle, wie sich auch bei einem Arduino funktioniert.
Daher verwenden wir diese um die Funktionalität des UART to USB-Converters zu testen.
Es werden alle Verbindungen getrennt und die Megacard per USB-Kabel auf USB-Buchse 0 
des RaspberryPis verbunden. Hier wird die Megacard erkannt und ein normales Programm (Displayausgabe + blinkende LEDs) auf die Megacard gespielt.
So kann die Funktionsfähigkeit des UART to USB Chips getestet werden, als auch, dass die
Megacard erfolgreich per AVR-Dude bespielt werden kann.

Alle Tests zusammen Testen die Megacard prinzipiell auf nahezu die gesamte Peripherie, welche sie bietet und welche im Unterricht zum Einsatz kommt.

## 4. Beenden der Tests

Schlägt ein Test fehl oder alle Tests werden erfolgreich abgeschlossen, wird zum Beenden der Tests gewechselt.
Hier werden alle Pins der Port-Expander zurückgesetzt und der Bootloader für die jeweilige Megacard wird auf die Karte gespielt.
Im Falle der MegacardV5 wird hier auch der oben erwähnte USB-Test durchgeführt.

Um dem Nutzer anzeigen zu können, wo der Test fehlgeschlagen ist, wird hier ein Error-Code laut der nachfolgenden Liste ausgegeben:

### Error-Codes
| Error-Code | Beschreibung              | Ursachen                                                                                                                                                        |
| ---------- | ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0          | Erfolgreicher Test        | Die gesamte Peripherie hat wie gewünscht funktioniert.                                                                                                          |
| 001        | PD2/PD3 Verbindungs Error | Es lässt sich keine Kommunikation zwischen Megacard und PortD herstellen. Die Ursache liegt wahrscheinlich in der Verbindung von PortD.                         |
| 1XX        | Port-Error                | Auf der Leiterplatte gibt es Fehler, Kurzschlüsse, o.Ä., die verhindern, dass jeder Pin einzeln angesprochen werden kann.                                       |
| 101        | PortA-Output-Error        |                                                                                                                                                                 |
| 102        | PortA-Input-Error         |                                                                                                                                                                 |
| 103        | PortB-Output-Error        | Der PROG-Jumper wurde vor Starten der Tests nicht entfernt.                                                                                                     |
| 104        | PortB-Input-Error         |                                                                                                                                                                 |
| 105        | PortC-Output-Error        |                                                                                                                                                                 |
| 106        | PortC-Input-Error                          |                                                                                                                                                                 |
| 107        | PortD-Output-Error        |                                                                                                                                                                 |
| 108        | PortD-Input-Error         |                                                                                                                                                                 |
| 201        | LED-Single-Error          | LEDs wurden falsch herum aufgelötet oder der LED-Jumper wurde nicht gesetzt.                                                                                    |
| 202        | LED-All-Error             |                                                                                                                                                                 |
| 3XX        | Taster-Error              | Ein Taster ist nicht ordentlich aufgelötet oder der Nutzer hat für das Drücken > 12 Sekunden benötigt                                                           |
| 301        | Taster0-Error             |                                                                                                                                                                 |
| 302        | Taster1-Error             |                                                                                                                                                                 |
| 303        | Taster2-Error             |                                                                                                                                                                 |
| 304        | Taster3-Error             |                                                                                                                                                                 |
| 401        | Buzzer-Error              | Der Buzzer wurde falsch aufgelötet oder der Nutzer hat den Buzzer-Jumper nicht gesetzt                                                                          |
| 501        | ADC-Error                 | Das Potentiometer wurde falsch aufgelötet                                                                                                                       |
| 601        | JTAG-Error                | Die Verbindung mit dem Kabel wurde nicht ordentlich hergestellt                                                                                                 |
| 701        | Display-Error             | V4: Das Display wurde nicht ordentlich verbunden oder es wurde mit Poti kein ordentlicher Kontrast eingestellt. V5: Das Display ist falsch aufgelötet/verbunden |

# Bedienungsanleitung
## Video-Demo
## Materialien
Zum Verwenden des Testboards werden folgende Dinge benötigt:
- Testboard
- RaspberryPi + Versorgung
- ISP-Kabel
- JTAG-Kabel
- Min. 1x Jumper, besser mehr

## Anleitung
1. **Testprogramm starten**

Auf dem RaspberryPi findet sich das Testprogramm, sowie alle hinzugehörigen Dinge im Verzeichnis `/home/pi/testand`. Das Testskript trägt den Titel "test.py" und sollte mit Python3 gestartet werden.
Nach dem Starten sollte folgende Ausgabe zu erkennen sein:
	
```
	Connect the testboard to the raspberrypi!
```

2. **Testboard verbinden**

Wie in der obigen Ausgabe zu erkennen, soll das Testboard mit dem RaspberryPi verbunden werden. Die sollte wie folgt aussehen:

![ ](Images/Pasted image 20220618160815.png)

Die zwei Status-LEDs des Testandes sollten aufleuchten.
Wurde das Testboard verbunden, sollte sich die Ausgabe automatisch verändern.

3. **Megacard verbinden**

Nach anschließen des Testboards sollte die folgende Ausgabe zu erkennen sein:
	
```
	Bitte eine Megacard über die Steckleiste anschliesen.
	
	S2 when ready. S1 to cancle.
```

Ist dies der Fall, kann die zu testende Megacard an die Steckleisten X1 und X2 auf der rechten Seite des Testboards verbunden werden. Dies sollte wie folgt aussehen:

![ ](Images/Pasted image 20220618162128.png)

Wurde die Megacard verbunden, kann dies durch Drücken von S2 (linker Taster) bestätigt werden. Mit S1 (rechter Taster) kann der Test abgebrochen werden.

In diesem Schritt wird auch auf Kurzschlüsse der Megacard getestet. Erlischt beim Anschließen der Megacard die rechte Lampe (Beschriftung: "5VPROT"), wurde ein Kurzschluss angeschlossen. Diese ist schnellstmöglich zu entfernen. Durch die rückstellbare Sicherung, sollte jedoch nichts kaputt gehen können.
Beispielbild (KS mit Kabel simuliert):

![ ](Images/Pasted image 20220618161911.png)

Wird das erlöschen der Lampe nicht erkannt, wird auch beim Drücken von S2 um zum nächsten Schritt zu wechseln ein Fehler ausgegeben:
	
```
	Overcurrent detected!
	Please disconnect and check for a short circuit.
	Press a Button (S2/S1) to exit.
```

4. **ISP verbinden**

Es sollte sich der folgende Output ergeben:
	
```
	Bitte eine Megacard ueber ISP anschliesen.
	Prog Jumper setzten!
	
	S2 when ready. S1 to cancle.
```

Nun soll, wie dargestellt der ISP-Stecker der Megacard mit dem ISP-Stecker des Testboards per Kabel verbunden werden. Außerdem muss der PROG-Jumper gesetzt werden.
	
Beispielbild:

![ ](Images/Pasted image 20220618163025.png)

Wurde das Ganze verbunden, wird die Taste S2 gedrückt. Mit S1 kann der Test abgebrochen werden.

Ist die Verbindung nicht erfolgreich, ergibt sich folgende Fehlermeldung. Es ist zu überprüfen, ob das Kabel richtig angeschlossen ist, und ob der PROG-Jumper gesetzt ist.
	
```
	Error. Connect and try again! Set the PROG Jumper!
	
	S2 when ready. S1 to cancle.
```

Wie dargestellt kann mit S2 die Verbindung erneut versucht werden, mit S1 kann der Test abgebrochen werden.

5. **JTAG verbinden**

Es sollte sich der nachfolgende Output ergeben:
	
```
	Bitte eine Megacard ueber JTAG anschliesen.
	
	S2 when ready. S1 to cancle.
```

Wie gesagt, sollte die Megacard und das Testboard über die JTAG-Schnittstelle verbunden werden. 
Dazu wird einfach ein Kabel angeschlossen.
Wurde die Verbindung hergestellt kann mit S2 zum nächsten Schritt gewechselt werden.
Beispielbild:

![ ](Images/Pasted image 20220618164146.png)
	
6. **Version auswählen**

Es sollte sich folgender Output ergeben:
	
```
	V4 (S2) or V5 (S1)?
```

Wie dargestellt, kann die jeweilige Taste gedrückt werden um die Version der Megacard auszuwählen.

7. **Starten der Tests**

Es ergibt sich folgender Output:
	
```
	Testprogram loaded!
	Disconnect the PROG-Jumper now and start!

	S2 or S1 when ready
```

Wie dargestellt ist der PROG-Jumper zu entfernen. Es wird empfohlen, diesen an die Stelle des LED-Jumpers bzw. des Buzzer-Jumpers zu verschieben.
Generell wird an dieser Stelle empfohlen, den LED, sowie den Buzzer-Jumper zu setzen.

Durch Drücken eines Tasters wird hier der Testablauf gestartet.

Im Falle der MegacardV4 ergibt sich zusätzlich folgende Ausgabe:
	
```
	Connect the external Display now as well.
```

Wie hier erwähnt sollte hier, falls vorhanden, auch das externe 16x2 LCD-Display an der MegacardV4 angeschlossen werden, vor der Testablauf gestartet wird.

Beispielbild:

![ ](Images/Pasted image 20220618164417.png)

8. **LED-Test**

Das Programm läuft selbständig durch Port-Tests. 
Anschließend folgt die folgende Ausgabe:
	
```
	LED test...
		The light should be running through all LEDs now.
		Make dure the LED Jumper is connected.
		Is the Test sucessful?
		S2 sucessful. S1 else.
```

Wie gesagt, sollte der Nutzer nun auf die Megacard schauen. Wenn sich dort ein Lauflicht einzeln durch alle LEDs bewegt ist der Test erfolgreich und S2 sollte gedrückt werden. Ansonsten S1.

Bei erfolgreichem Test folgt nun folgende Ausgabe:
	
```
	LED test...
		The light should be running through all LEDs now.
		Make dure the LED Jumper is connected.
		Is the Test sucessful?
		S2 sucessful. S1 else.
```

Alle LEDs auf der Megacard sollten in gleicher Helligkeit leuchten. Wenn sie dies tun, ist der Test erfolgreich und es sollte S2 gedrückt werden. Ansonsten soll S1 gedrückt werden.

9. **Taster-Test**

In diesem Test ergibt sich folgende Ausgabe:
	
```
	Taster Test...
		Please push and release Button 0...
```

Der Nutzer sollte hier S0 auf der Megacard drücken.
Anschließend erfolgt dieselbe Ausgabe für Button 1 - 3 (S1-S3 auf der Megacard).
Für das Drücken jedes Buttons stehen bis 12 Sekunden zur Verfügung. Ansonsten wird ist der Test nicht erfolgreich.
	
10. **Buzzer-Test**

Für diesen Test sollte der BEEP-Jumper der Megacard gesetzt sein.
Es ergibt sich folgende Ausgabe:
	
```
	Buzzer-Test...
		The Buzzer should be ringing now. Please make sure to connect the Jumper.
		Is the test sucessfull?
		S2 sucessful. S1 else.
```

Wie dargestellt, sollte der Buzzer einen Ton ausgeben werden. Wenn der Ton gehört wird sollte S2 gedrückt werden. Wenn er nicht gehört wird, S1, der Test ist dann fehlgeschlagen.

11. **ADC-Poti-Test**

Es ergibt sich folgende Ausgabe:
	
```
	ADC-Poti-Test
		We will output the ADC-Value to the LEDs now. Rotate the Potentiometer and the value should change.
		Ranging from 0 - 255.
		S2 sucessful. S1 else.
```

Wie erwähnt wird die Potentiometerstellung mit dem ADC eingelesen und auf PORTC ausgegeben. Der Nutzer soll am Potentiometer drehen. Der Wert sollte von 0 (alle LEDs aus) bis 255 (alle LEDs an) verändert werden können.
Ist dies der Fall, soll S2 gedrückt werden. Ist dies nicht der Fall soll S1 gedrückt werden.

12. **Display-Test**

Es folgt der automatisch ablaufende Test der JTAG-Verbindung.

Anschließend ergibt sich folgende Ausgabe:
	
```
	Display Test...
		Check the Megacard Display. Can you read text there?
		Make sure to set a proper contrast on v4!
		S2 sucessful. S1 else.
```

Der Nutzer sollte nun das angeschlossene bzw. eingebaute Display überprüfen. Darauf sollte ein Text zu erkennen sein.
	
Im Falle der V5:

![ ](Images/Pasted image 20220618170432.png)

Im Falle der V4:

![ ](Images/Pasted image 20220618170450.png)

Hier muss gegebenenfalls der Kontrast mit dem Potentiometer eingestellt werden.

Kann der Text gelesen werden, sollte S2 gedrückt werden (Test erfolgreich). Kann keine Ausgabe erkannt werden, sollte S1 gedrückt werden (Test fehlgeschlagen).

13. **Beenden der Tests**

Schlägt ein Test fehl bzw. alle Tests sind abgeschlossen wird zu dieser Code-Sektion gewechselt.

Bei keinen erkannten Fehler ergibt sich folgender Output:

```
	Tests exited. No Errors detected.
	Set the PROG-Jumper to load the bootloader.
	S2 or S1 when ready
```

Wenn sich in einem Test ein Fehler ergeben hat ergibt sich beispielsweise folgender Output:
	
```
	Tests exited with Error-Code: 103 - PortB_Output_Error

	Set the PROG-Jumper to load the bootloader.
	S2 or S1 when ready
```

Es wird hierbei zusätzlich der Error-Code ausgegeben, sowie der Name dieses Codes im Code. Die Errors können in der dazugehörigen Liste nachgeschaut werden bzw. auch anhand des Namens erkannt werden.


Im Anschluss an beide Ausgaben wird der Bootloader auf die Megacard gespielt. Dazu muss wieder der PROG-Jumper gesetzt werden.
Wurde der Jumper gesetzt kann mit S2 oder S1 fortgesetzt werden.

Kann nicht mit der Megacard kommuniziert werden erscheint folgende Meldung. Mit S2 oder S1 kann die Kommunikation erneut versucht werden.
Ursache ist meist, dass der PROG-Jumper nicht gesetzt bzw. die ISP-Verbindung getrennt wurde.
	
```
	Failed to communicate. Set the PROG-Jumper and the ISP-Connector!
	S2 or S1 when ready
```

Kann eine Verbindung hergestellt werden, ergibt sich folgender Output:

```
	Bootloader for v5 loaded.
	Fully disconnect the board from the testboard.
	S2 or S1 when done
```

Der Bootloader wurde auf das Testboard geladen. Das Testboard soll vollständig vom Testboard getrennt werden.

Im Falle der Megacard v4 ist der Test hiermit abgeschlossen.

14. **USB-Connection-Test (V5)**

Im Falle der Megacard v5 wird nun noch die Verbindung zu einem Computer (o.Ä.) zum Flashen der Karte mittels AVRDude getestet. 
Es ergibt sich folgender Output:
	
```
	Testing AVR Flashing and USB-Connection-Chip.
	Connect Megacard over USB.
	S2 or S1 when ready.
```

Wie dargestellt sollte die Megacard V5 per USB-Kabel mit dem RaspberryPi verbunden werden. Es ist der USB-Port "0" (Links oben) zu verwenden.
Außerdem muss auf der Megacard der VSEL (Voltage-Select) Jumper auf 5V oder 3.3V gesetzt sein.

Beispielbild:

![ ](Images/Pasted image 20220619122443.png)

Wenn die Verbindung hergestellt ist kann S1 oder S2 zum Fortsetzten gedrückt werden. 

Wenn das Flashen funktioniert (d.H. Bootloader ist richtig auf der Megacard + USB-Stecker + UART to USB-Chip) ergibt sich folgender Output:

```
Running...
Sucessfully communicated and loaded demo program. The Megacard v5 is ready to use.
```

Funktioniert die Kommunikation nicht ergibt sich folgender Output:

```
Running...
Failed.
There is an issue communicating between the computer and the AVR.
Most likely due to issues with the connecting chip or the USB-Port. Also make sure to connect to the right USB-Port.
```

Wie dargestellt, funktioniert in diesem Fall am wahrscheinlichsten der UART to USB-Chip nicht richtig bzw. der USB-Port wurde falsch aufgelötet bzw. mit dem falschen Port des RaspberryPis verbunden.


15. **Nächster Test**

Wird an einer Stelle im Test der Test abgebrochen (durch Taste S1, wenn dies möglicht ist), oder ein Test schlägt fehl, wird der Test nach Schritt 14. beendet. Nach Schritt 14. Wird wieder zu Schritt 2 gewechselt. Daher: Es wird überprüft, ob das Testboard angeschlossen ist (im normalfall sollte es dies noch sein). Es kann dann die nächste Megacard zum Testen angeschlossen werden.

# Funktionalität/Ergebnis/Fazit
## Ergebnis
Die MegacardV4 und die MegacardV5 kann unter einhaltung der Anleitung erfolgreich getestet werden.
Funktionsfähige Megacards schaffen es durch den Test. Fehlerhafe Megacards bzw. Verbindungen werden unseren Tests zu Folge, erfolgreich erkannt.

Das Ziel des Projektes wurde somit erreicht und wir sind als Projektteam zufrieden mit unserer Arbeit.

Wir haben eine Demonstration des Teststandes als [Video](https://youtu.be/NJlLUSWisFg) aufgenommen. 
[![Bild des Youtube Videos](https://img.youtube.com/vi/NJlLUSWisFg/0.jpg)](https://youtu.be/NJlLUSWisFg) zu finden.

## Probleme 
Größere Probleme im Projekt haben uns die Folgenden Dinge geboten, diese konnten jedoch alle behoben werden:

**RaspberryPi und Python**

Für beide Teammitglieder stellte dies das erste Projekt dar, in welchem wir mit einem RaspberryPi arbeiten. Ein Problem stellte daher dar, den RaspberryPi überhaupt zum Laufen zu bringen und darauf Programmieren zu können, ohne eine Tastatur, einen Bildschirm und eine Maus anschließen zu müssen. Glücklicherweise gibt es zu diesem Thema endlose Tutorials. Für die Programmierung im Projekt wurde der RaspberryPi mit dem Hotspot eines Notebooks verbunden und mittels Visual Studio Code Remote SSH programmiert.

Python stellte für die Programmierung soweit ein Problem dar, dass wir beide unerfahren damit sind. Python ist jedoch sehr anfängerfreundlich und das Internet/Bücher bilden eine gute Hilfe dazu. Das Programmieren mit Visual Studio Code hat das Ganze jedoch nicht einfacher gemacht, da IntelliSense (Vorschläge beim Programmieren) trotz Extension mit einer Remote-Verbindung relativ schlecht funktioniert.

**Kommunikation zwischen RaspberryPi und Megacard**
In unserem Projekt wird hierzu PD2 und PD3 verwendet. 

Die Kommunikation ist sehr einfach, sie stellt eher eine Art Takt dar, der beide Programm an der gleichen Stelle hält. Die Funktionalität wird erst durch das Übereinstimmen der programmierten Abläufe auf Megacard und RaspberryPi geboten. Grunsätzlich stand auch die Überlegung im Raum, auf UART, SPI oder I2C zur Kommunikation zurückzugreifen, jedoch wurden diese nicht verwendet, da sie einerseits teilweise bereits für andere Funktionen zum Einsatz kommen (I2C-Port-Expander), SPI-Schnittstelle bzw. Pins davon als ISP-Flashing. Und andererseits die Slave-Programmierung auf der Megacard nicht immer ideal umzusetzten ist. Vor Allem auch mit Hinblick auf den in der Megacardv5 an der UART-Schnittstelle angeschlossenen UART to USB-Converter wollten wir hier keinen unnötigen Aufwand und kein unnötiges Risiko eingehen.

Unser Projekt zeigt jedoch, dass durch gut abgestimmt programmierte Abläufe, der Testablauf auch ohne komplizierte Abläufe funktioniert.

**MegacardV5**

Die größten Probleme, welche sich mit der MegacardV5 ergeben haben ist, dass sie neu ist und uns relativ lange im Projekt nicht zur Verfügung stand. Zwar wurde uns eine MegacardV5 für das Projekt versprochen, jedoch haben wir sie erst nach einigen Wochen erhalten. 

Außerdem ist die MegacardV5 neu, daher, sie ist uns, sowie den meisten Schülern und auch Lehrern noch unbekannt. Die Änderungen mussten daher selbst aus den Schaltplänen erarbeitet werden. Auch die Ansteuerung des internen Displays, das Flashen mittels AVR-Dude, ... haben zeitweise ein Problem dargestellt.

Durch Rückgreifen auf die Unterlagen, welche Stefan Zudrell Koch jedoch auf seinen Laufwerken ausführlich zur Megacardv5 zur Verfügung stellt konnten auch diese Probleme, zwar mit einiger Arbeit Verbunden, letztendlich behoben/umgangen werden.

**Bootloader und ISP**

Einen grundlegenden Teil des Projektes stellt das Verbinden der Megacard mit dem RaspberryPi über die ISP-Schnittstelle dar. So werden die Bootloader auf die Megacards gespielt, was einen grundlegenden Teil unseres Projektes darstellt.

Das Problem stelle für uns einerseits das erhalten der Bootloader dar. Diese sind jedoch auf dem Schullaufwerk, zwar relativ versteckt, aber dennoch vorhanden.

Das viel größere Problem stellte jedoch das mangelnde Wissen über dieses Thema dar. Es erscheint zwar relativ einfach, wenn man weiß, dass der Bootloader sich ständig auf der Megacard befindet und dort dafür sorgt, dass Programme über USB geladen werden können, wenn einem dies jedoch nicht bewusst ist, und man sich überlegen muss, wie man Programme vom RaspberryPi auf die Megacard spielen kann kann einen dies vor ein Problem stellen.

# Anlagen & Dateien
## Raspberry-Pi-Verzeichnis
Das RaspberryPi-Verzeichnis befindet sich unter `/home/pi/testand/`. Es besteht aus folgenden Dateien:

**RaspberryPi-Testprogramm**

Das Herzstück des Projektes bildet das Python-Testprogramm. Es wird auf dem RaspberryPi ausgeführt.

[Link zum Testprogramm](Images/Test.py)

**Megacard-Testprogramme**

Die Megacard-Testprogramme werden zum Testen auf die Megacards gespielt.

[Link zum v4Testprogramm](Images/v4Programm.hex)

[Link zum v5Testprogramm](Images/v5Programm.hex)

**Bootloader**

Unser Programm spielt die Bootloader auf die Megacards, weshalb auch diese vorhanden sein müssen:

[Link zum v4Bootloader](Images/v4Bootloader.hex)

[Link zum v5Bootloader](Images/v5Bootloader.hex)

**V5-Demo-Programm**

Für die Megacardv5 wird der USB-Bootloader + Verbindung getestet, dazu wird über AVR-Dude ein normales Testprogramm auf die Megacard gespielt. Es ersetzt nicht den Bootloader.

[Link zum Demo-Programm](Images/v5AVRTestProgram.hex)


## Code-Files
Weitere Code-Dateien, welche im Projekt umgesetzt wurden, sind die Testprogramme für die MegacardV5, sowie die MegacardV4. Diese sind auf dem RaspberryPi nur als .hex-Files vorhanden (=was auf den ATMEGA16 gespielt wird). Sprich: Was der Compiler liefert, nicht aber die tatsächlichen Code-Files. Beide Programmierfiles sind sehr ähnlich, sie unterscheiden sich in Ausgabe des Buzzersignales und in der Displayausgabe.

[Link zum V4-Code-Verzeichnis (MicrochipStudioFiles)](Images/V4_Test.zip)
[Link zum V5-Code-Verzeichnis (MicrochipStudioFiles)](Images/V5_Test.zip)

## Layout & Schaltplan des Testboards
[Link zum Layout (Eagle-Files)](Images/Teststand_Board.zip)

### Schaltplan

[Link zum Schaltplan](Images/0_Draft_Schematic.pdf)

![ ](Images/Pasted image 20220619144949.png)

### Layout Work

[Link zum Layout-Work](Images/0_Draft_LayoutWorkTop.pdf)

![ ](Images/Pasted image 20220619145023.png)

### Bestückungsplan

[Link zum Bestückungsplan](Images/0_Draft_brd_Best_top.pdf)

![ ](Images/Pasted image 20220619145121.png)

### Stückliste
| Nr. | Bauteil                | Stück |
| --- | ---------------------- | ----- |
| 1   | R 330R 0805            | 1     |
| 2   | R  1k0 0805            | 2     |
| 3   | R  1k2 0805            | 1     |
| 4   | R  1k5 0805            | 1     |
| 5   | R  10k 0805            | 14    |
| 6   | BS138                  | 6     |
| 7   | SMD-Taster             | 2     |
| 8   | SMD-LED 1206           | 2     |
| 9   | MCP23017SP             | 3     |
| 10  | Polyfuse 200mA 1206    | 1     |
| 11  | ISP 6 Pin Buchse       | 1     |
| 12  | JTAG 10 Pin Buchse     | 1     |
| 13  | 20-Pol-Stiftleiste 90° | 2     |
| 14  | 2-Pol Stiftleiste      | 3      |
