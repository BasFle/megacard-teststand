import smbus
import RPi.GPIO as GPIO
import subprocess
import numpy
import time
import re
from rpi_lcd import LCD
from enum import Enum

# region Constants
IODIRA = 0x00
GPIOA = 0x12
OLATA = 0x14

IODIRB = 0x01
GPIOB = 0x13
OLATB = 0x15

PA0 = PB0 = PC0 = PD0 = 0
PA1 = PB1 = PC1 = PD1 = 1
PA2 = PB2 = PD2 = PD2 = 2
PA3 = PB3 = PD3 = PD3 = 3
PA4 = PB4 = PD4 = PD4 = 4
PA5 = PB5 = PD5 = PD5 = 5
PA6 = PB6 = PD6 = PD6 = 6
PA7 = PB7 = PD7 = PD7 = 7

InputDirection = 1
OutputDirection = 0

PORTA = 'A'
PORTB = 'B'

PortExpander_Megacard_PortAC = 0x20
PortExpander_Megacard_PortBD = 0x21
PortExpander_Megacard_Misc = 0x23

i2c_con = smbus.SMBus(1)  # Rev 2 Pi

S1_Pin = 16
S2_Pin = 20
ProtSense = 21


# endregion

def reverse_bitOrder_as_uint8(num):
    num = numpy.unpackbits(numpy.ubyte(num))
    i = 7
    result = numpy.ubyte(0)
    while i >= 0:
        result += (num[i] << (i))
        i -= 1

    return numpy.ubyte(result)


def invert_as_uint8(num):
    return ~numpy.ubyte(num)


class Errors(Enum):
    NoError = 0
    PortD_Connection_Error = 1
    PortA_Output_Error = 101
    PortA_Input_Error = 102
    PortB_Output_Error = 103
    PortB_Input_Error = 104
    PortC_Output_Error = 105
    PortC_Input_Error = 106
    PortD_Output_Error = 107
    PortD_Input_Error = 108
    LED_Single_Error = 201
    LED_All_Error = 202
    Taster0_Error = 301
    Taster1_Error = 302
    Taster2_Error = 303
    Taster3_Error = 304
    Buzzer_Error = 401
    ADC_Error = 501
    JTAG_Error = 601
    Display_Error = 701


class MegacardPort:
    def __init__(self, expanderAddress, a_or_b='a', inv=False):
        if a_or_b == 'a' or a_or_b == 'A':
            self.expanderAddress = expanderAddress
            self.DDR_Adr = IODIRA
            self.PORT_Adr = OLATA
            self.PIN_Adr = GPIOA
        elif a_or_b == 'b' or a_or_b == 'B':
            self.expanderAddress = expanderAddress
            self.DDR_Adr = IODIRB
            self.PORT_Adr = OLATB
            self.PIN_Adr = GPIOB

        self.inv = inv

    def get_Pin(self):
        swap = i2c_con.read_byte_data(self.expanderAddress, self.PIN_Adr)
        if self.inv:
            swap = reverse_bitOrder_as_uint8(swap)

        return numpy.ubyte(swap)

    def set_Port(self, value):
        swap = value
        if self.inv:
            swap = reverse_bitOrder_as_uint8(swap)

        i2c_con.write_byte_data(self.expanderAddress, self.PORT_Adr, swap)

    def get_Port(self):
        swap = i2c_con.read_byte_data(self.expanderAddress, self.PIN_Adr)
        if self.inv:
            swap = reverse_bitOrder_as_uint8(swap)

        return numpy.ubyte(swap)

    def set_DDR(self, value):
        swap = value
        if self.inv:
            swap = reverse_bitOrder_as_uint8(swap)

        i2c_con.write_byte_data(self.expanderAddress, self.DDR_Adr, invert_as_uint8(swap))

    def get_DDR(self):
        swap = i2c_con.read_byte_data(self.expanderAddress, self.DDR_Adr)
        if self.inv:
            swap = reverse_bitOrder_as_uint8(swap)

        return invert_as_uint8(swap)


def Clear_Console():
    subprocess.run("clear")


def S1_Or_S2_Decision():
    print("")
    while (True):
        if GPIO.input(S1_Pin) == 0:
            while GPIO.input(S1_Pin) == 0:
                x = 1
            return 1
        elif GPIO.input(S2_Pin) == 0:
            while GPIO.input(S2_Pin) == 0:
                x = 2

            return 2


def ReadyOrCancle():
    print("S2 when ready. S1 to cancle.")
    if S1_Or_S2_Decision() == 1:
        return 0
    else:
        return 1


def WaitTillReady():
    print("S2 or S1 when done\n")
    S1_Or_S2_Decision()


def InputPortTest(prt):
    if prt == PortA:
        error = Errors.PortA_Input_Error
    elif prt == PortB:
        error = Errors.PortB_Input_Error
    elif prt == PortC:
        error = Errors.PortC_Input_Error

    # region InputTests
    prt.set_Port(0x00)
    prt.set_DDR(0xFF)  # Port is output

    PortD.set_Port(0x00)
    PortD.set_DDR(invert_as_uint8((1 << PD2)))  # PD2 is Input

    print("\tLow-Input Test...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed")
        End_Tests(error)

    print("\tSucessful")

    time.sleep(0.025)

    print("\tHigh-Input Test...")
    t_end = time.time() + 2
    prt.set_Port(0xFF)  # all high output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(error)
    print("\tSucessful")

    time.sleep(0.025)

    print("\tHigh-Low-Input Test...")
    t_end = time.time() + 2
    prt.set_Port(0b10101010)  # high-low-output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(error)
    print("\tSucessful")

    time.sleep(0.025)

    print("\tLow-High-Input Test...")
    t_end = time.time() + 2
    prt.set_Port(0b01010101)  # all high output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(error)
    print("\tSucessful")

    prt.set_DDR(0x00)
    prt.set_Port(0x00)

    PortD.set_DDR((1 << PD2))
    # endregion


def OutputPortTest(prt):
    if prt == PortA:
        error = Errors.PortA_Output_Error
    elif prt == PortB:
        error = Errors.PortB_Output_Error
    elif prt == PortC:
        error = Errors.PortC_Output_Error
    # region Output Tests
    prt.set_Port(0x00)
    prt.set_DDR(0x00)  # Port is input

    PortD.set_Port(0b00000000)
    PortD.set_DDR(0b00000100)

    # region Port_Low_Test
    print("\tLow Test...")

    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (prt.get_Pin() == 0b00000000):
            break

    if (time.time() > t_end):
        print("Failed!")
        print("Wrongly Recieved:" + bin(prt.get_Pin()))
        End_Tests(error)

    print("\tSucessful\t" + bin(prt.get_Pin()))
    PortD.set_Port(0b00000100)
    # endregion

    # region Port_High_Test
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tHigh Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (prt.get_Pin() == 0b11111111):
            break

    if (time.time() > t_end):
        print("Failed!")
        print("Wrongly Recieved:" + bin(prt.get_Pin()))
        End_Tests(error)
    print("\tSucessful\t" + bin(prt.get_Pin()))
    PortD.set_Port(0b00000100);
    # endregion

    # region Port_High_Low_Test
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tHigh-Low-Pattern Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (prt.get_Pin() == 0b10101010):
            break

    if (time.time() > t_end):
        print("Failed!")
        print("Wrongly Recieved:" + bin(prt.get_Pin()))
        End_Tests(error)
    print("\tSucessful\t" + bin(prt.get_Pin()))
    PortD.set_Port(0b00000100);

    # endregion

    # region Port_Low_High_Test
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tLow-High-Pattern Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (prt.get_Pin() == 0b01010101):
            break

    if (time.time() > t_end):
        print("Failed!")
        print("Wrongly Recieved:" + bin(prt.get_Pin()))
        End_Tests(error)

    print("\tSucessful\t" + bin(prt.get_Pin()))
    PortD.set_Port(0b00000100);
    # endregion
    # endregion


def FullPortTest(prt):
    OutputPortTest(prt)
    InputPortTest(prt)


def PD2_Blink(dur=150e-3):
    PortD.set_Port((1 << PD2))
    time.sleep(dur)
    PortD.set_Port(0x00)


def End_Tests(error_code=Errors.NoError):
    PortA.set_DDR(0x00)
    PortB.set_DDR(0x00)
    PortC.set_DDR(0x00)
    PortD.set_DDR(0x00)
    JTAG.set_DDR(0x00)

    PortA.set_Port(0x00)
    PortB.set_Port(0x00)
    PortC.set_Port(0x00)
    PortD.set_Port(0x00)
    JTAG.set_Port(0x00)

    Clear_Console()
    if error_code == Errors.NoError:
        print("-------------------")
        print("Tests exited. No Errors detected.")
        print("-------------------\n")
    else:
        print(f"Tests exited with Error-Code: {error_code.value} - {error_code.name}\n")
    print("Set the PROG-Jumper to load the bootloader.")
    WaitTillReady()
    output = subprocess.run(
        f'sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 -U flash:w:"/home/pi/testand/v{megaVer}Bootloader.hex":i',
        shell=True, capture_output=True).stderr
    output = str(output)

    while not "initialized" in output:
        print("Failed to communicate. Set the PROG-Jumper and the ISP-Connector!")
        WaitTillReady()
        # Load Bootloader to megacard
        output = subprocess.run(
            f'sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 -U flash:w:"/home/pi/testand/v{megaVer}Bootloader.hex":i',
            shell=True, capture_output=True).stderr
        output = str(output)

    Clear_Console()
    print(f"Bootloader for v{megaVer} loaded.")
    print("Fully disconnect the board from the testboard.")
    WaitTillReady()

    if (megaVer == "5"):
        print("Testing AVR Flashing and USB-Connection Chip.")
        print("Connect Megacard over USB.")
        WaitTillReady()
        print("Running...")

        output = subprocess.run(
            f'sudo /usr/local/bin/avrdude -c arduino -p m16 -P /dev/ttyUSB0 -b 57600 -U flash:w:"/home/pi/testand/v5AVRTestProgram":a',
            shell=True, capture_output=True).stderr
        output = str(output)

        if (not "initialized" in output):
            print("Failed.")
            print("There is an issue communicating between the computer and the AVR.")
            print(
                "Most likely due to issues with the connecting chip or the USB-Port. Also make sure to connect to the right USB-Port.")

            return 
        else:
            print("Sucessfully communicated and loaded demo program. The Megacard v5 is ready to use.")
    print("\n------------------------")
    print("End of tests")
    print("------------------------")
    return

def TestProgramm():
    # region Setup
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ProtSense, GPIO.IN)
    GPIO.setup(S1_Pin, GPIO.IN)
    GPIO.setup(S2_Pin, GPIO.IN)

    Clear_Console()

    print("Connect the testboard to the raspberrypi!")

    line = ""
    while not ('20 21 -- 23' in line):
        p = subprocess.Popen(['i2cdetect', '-y', '1'], stdout=subprocess.PIPE, )
        line = str(p.stdout.readlines())
        if (not '20 21 -- 23' in line):
            #print("ERROR! You must connect to the testboard!")

    Clear_Console()
    print("Testboard connected.")

    PortA = MegacardPort(PortExpander_Megacard_PortAC, a_or_b='b')
    PortB = MegacardPort(PortExpander_Megacard_PortBD, a_or_b='b')
    PortC = MegacardPort(PortExpander_Megacard_PortAC, a_or_b='a', inv=True)
    PortD = MegacardPort(PortExpander_Megacard_PortBD, a_or_b='a', inv=True)
    JTAG = MegacardPort(PortExpander_Megacard_Misc, a_or_b='b')

    PortA.set_Port(0x00)
    PortB.set_Port(0x00)
    PortC.set_Port(0x00)
    PortD.set_Port(0x00)
    JTAG.set_Port(0x00)

    PortA.set_DDR(0x00)
    PortB.set_DDR(0x00)
    PortC.set_DDR(0x00)
    PortD.set_DDR(0x00)
    JTAG.set_DDR(0x00)
    # endregion Setput

    # region Connection
    print("Bitte eine Megacard über die Steckleiste anschliesen.")
    if ReadyOrCancle() == 0:
        return 0
    if GPIO.input(ProtSense) == 0:
        Clear_Console()
        print("Overcurrent detected!")
        print("Please disconnect and check for a short circuit.")
        print("Press a Button to exit.")
        S1_Or_S2_Decision()
        return 0

    Clear_Console()
    print("Bitte eine Megacard ueber ISP anschliesen.\nProg Jumper setzten!")
    if ReadyOrCancle() == 0:
        return 0
    while True:
        output = subprocess.run('sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 - U flash:r:"dev/null":R',
                                shell=True, capture_output=True).stderr
        output = str(output)

        if ("initialized" in output):
            break
        else:
            print("Error. Connect and try again! Set the PROG Jumper!")
            if ReadyOrCancle() == 0:
                return 0

    Clear_Console()

    print("Bitte eine Megacard ueber JTAG anschliesen.")
    if ReadyOrCancle() == 0:
        return 0

    Clear_Console()
    print("V4 (S2) or V5 (S1)?")
    if S1_Or_S2_Decision() == 2:
        txt = "4"
    else:
        txt = "5"

    megaVer = txt

    Clear_Console()
    # endregion Connection

    # region v4Flash
    print("Loading testprogramm to Device...\n")
    # Load Testprogram to megacard
    while True:
        output = subprocess.run(
            f'sudo /usr/local/bin/avrdude -c linuxspi -p m16 -P /dev/spidev0.0 -U flash:w:"/home/pi/testand/v{megaVer}Programm.hex":i',
            shell=True, capture_output=True).stderr
        output = str(output)

        if ("error" in output):
            print("ERROR. Could not load program. Let the PROG-Jumper connected!")
            if ReadyOrCancle() == 0:
                return 0
        else:
            break

    # endregion v4Flash
    Clear_Console()
    print("Testprogram loaded!\nDisconnect the PROG-Jumper now and start!\n")
    if megaVer == "4":
        print("Connect the external Display now as well.")

    WaitTillReady()
    Clear_Console()
    # region Tests
    # region PDConnection_Estab
    print("\nPD Connection Establishment...")

    t_end = time.time() + 5  # tries to connect for 5 seconds

    while time.time() < t_end:
        if PortD.get_Pin() & (1 << PD3):
            break

    if (time.time() > t_end):
        print("Could not establish connection with PortD")
        End_Tests(Errors.PortD_Connection_Error)

    PortD.set_DDR(PortD.get_DDR() | (1 << PD2))
    PortD.set_Port(PortD.get_Port() | (1 << PD2))

    t_end = time.time() + 5
    while time.time() < t_end:
        if (not (PortD.get_Pin() & (1 << PD3))):
            break

    if (time.time() > t_end):
        print("Could not establish connection with PortD")
        End_Tests(Errors.PortD_Connection_Error)

    PortD.set_Port(PortD.get_Port() & ~numpy.ubyte(1 << PD2))

    t_end = time.time() + 2
    while time.time() < t_end:
        if (not (PortD.get_Pin() & (1 << PD2))):
            break

    if (time.time() > t_end):
        print("Could not establish connection with PortD")
        End_Tests(Errors.PortD_Connection_Error)

    PortD.set_DDR(PortD.get_DDR() | (1 << PD3))
    PortD.set_Port(PortD.get_Port() | (1 << PD3))
    print("Connected\n")

    errorList = []  # Empty List of errors

    # endregion

    print("\n------------------")
    print("Testing...")
    print("--------------------")

    print("\nPort-Tests:")

    # region PortD Test
    # region OutputTests
    # region PD_Low_Test
    print("PortD Test...")
    PortD.set_DDR((1 << PD2))  # PD2 output rest input
    PortD.set_Port(0x00)
    print("\tLow Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (PortD.get_Pin() == 0b00000000):
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Output_Error)

    print("\tSucessful\t" + bin(PortD.get_Pin()))
    PortD.set_Port(0b00000100)

    # endregion

    # region PD_High_Test
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tHigh Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (PortD.get_Pin() == 0b11111011):
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Output_Error)

    print("\tSucessful\t" + bin(PortD.get_Pin()))
    PortD.set_Port(0b00000100);
    # endregion

    # region PD_High_Low_Test_A
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tHigh-Low-Pattern Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (PortD.get_Pin() == 0b10101010):
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Output_Error)

    print("\tSucessful\t " + bin(PortD.get_Pin()))
    PortD.set_Port(0b00000100);

    # endregion

    # region PD_High_Low_Test_B
    time.sleep(0.025)
    PortD.set_Port(0b00000000)

    print("\tLow-High-Pattern Test...")
    # Wait for result for max of 1 Second
    t_end = time.time() + 1
    while time.time() < t_end:
        if (PortD.get_Pin() == 0b01010001):  # PD2 is communication therefore not set
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Output_Error)

    print("\tSucessful\t" + bin(PortD.get_Pin()))
    PortD.set_Port(0b00000100);

    # endregion
    # endregion

    # region InputTests
    PortD.set_Port(0x00)
    PortD.set_DDR(invert_as_uint8((1 << PD2)))  # PD2 is Input, Rest is output

    print("\tLow-Input Test...")
    t_end = time.time() + 2
    PortD.set_Port(0x00)  # all low output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Input_Error)
    print("\tSucessful")

    time.sleep(0.025)

    print("\tHigh-Input Test...")
    t_end = time.time() + 2
    PortD.set_Port(0xFF)  # all high output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Input_Error)
    print("\tSucessful")

    time.sleep(0.025)

    print("\tHigh-Low-Input Test...")
    t_end = time.time() + 2
    PortD.set_Port(0b10101010)  # high-low-output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Input_Error)

    print("\tSucessful")

    time.sleep(0.025)

    print("\tLow-High-Input Test...")
    t_end = time.time() + 2
    PortD.set_Port(0b01010101)  # all high output

    while time.time() < t_end:
        if (PortD.get_Pin() & (1 << PD2)):  # wait for PD2
            break

    if (time.time() > t_end):
        print("Failed!")
        End_Tests(Errors.PortD_Input_Error)
    print("\tSucessful")
    # endregion InputTests

    print("PortD Test Sucessful\t.\n")

    PortD.set_DDR(0b00000100)
    # endregion PortD Test

    # region PortA Test
    print("PortA Test...")
    FullPortTest(PortA)
    print("PortA Test Sucessful\t.\n")
    # endregion PortA Test

    # region PortB Test
    print("PortB Test...")
    FullPortTest(PortB)
    print("PortB Test Sucessful\t.\n")
    # endregion PortB Test

    # region PortC Test
    print("PortC Test...")
    FullPortTest(PortC)
    print("PortC Test Sucessful.\n")
    # endregion PortC Test

    # region LED_Test
    print("LED test...")
    PD2_Blink()

    print(
        "\n\tThe light should be running through all LEDs now.\n\tMake sure the LED Jumper is connected.\n\tIs the test sucessful?")
    print("\tS2 sucessful. S1 else.")

    if S1_Or_S2_Decision() == 1:
        print("Failed")
        End_Tests(Errors.LED_Single_Error)

    print("Sucessful")

    PD2_Blink()
    # endregion LED_Test

    # region LED_Max_Test
    print("LED all test...")

    print(
        "\n\tAll lights should be turned on at maximum brightness.\n\tMake sure the LED Jumper is connected.\n\tIs the test sucessful?")
    print("\tS2 sucessful. S1 else.")

    if S1_Or_S2_Decision() == 1:
        print("Failed")
        End_Tests(Errors.LED_All_Error)

    PD2_Blink()
    print("Sucessful")
    # endregion LED_Max_Test

    # region Taster_Test
    print("Taster Test...")
    for i in range(0, 4):
        PortD.set_Port((1 << PD2))
        print(f"\tPlease push and release Button {i}...")
        t_end = time.time() + 12
        while time.time() < t_end:
            if (PortD.get_Pin() & (1 << PD3)):  # wait for PD3
                break

        if (time.time() > t_end):
            print("Failed. Time exceeded.")
            if i == 0:
                End_Tests(Errors.Taster0_Error)
            elif i == 1:
                End_Tests(Errors.Taster1_Error)
            elif i == 2:
                End_Tests(Errors.Taster2_Error)
            elif i == 3:
                End_Tests(Errors.Taster3_Error)
        else:
            print("\tSucessful.\n")
        PortD.set_Port(0x00)
        time.sleep(5e-3)

    print("Sucessful\n")
    # endregion Taster_Test

    # region Buzzer_Test
    PD2_Blink()
    print("Buzzer-Test...")
    print("\tThe Buzzer should be ringing now. Please make sure to connect the BEEP-Jumper.\n\tIs the test sucessful?")
    print("\tS2 sucessful. S1 else.")

    if S1_Or_S2_Decision() == 1:
        print("Failed")
        End_Tests(Errors.Buzzer_Error)

    print("Sucessful\n")
    PD2_Blink()
    # endregion Buzzer_Test

    # region ADC_Test
    print("ADC-Poti-Test")
    PD2_Blink()
    PortC.set_DDR(0x00)
    print(
        "\tWe will output the ADC-Value to the LEDs now. Rotate the Potentiometer and the value should change.\n\tRanging from 0 - 255.")
    print("\tS2 sucessful. S1 else.")

    if S1_Or_S2_Decision() == 1:
        print("Failed")
        End_Tests(Errors.ADC_Error)

    print("Sucessful\n")
    PD2_Blink(dur=20e-3)
    # endregion ADC_Test

    # region JTAG-Test
    PD2_Blink()
    print("JTAG-Test")

    print("\tDefault State Test...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if ((JTAG.get_Pin() & 0b00001111) == 0b00000000):  # wait for Default State expectation
            break

    if (time.time() > t_end):
        print("Failed")
        print("Wrongly recieved: " + bin(JTAG.get_Pin()))
        End_Tests(Errors.JTAG_Error)

    print("\tSucessful " + bin(JTAG.get_Pin()))

    PD2_Blink()
    print("\tTCK Test...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if (JTAG.get_Pin() & 0b00001111 == 0b00001000):
            break

    if (time.time() > t_end):
        print("Failed")
        print("Wrongly recieved: " + bin(JTAG.get_Pin()))
        End_Tests(Errors.JTAG_Error)

    print("\tSucessful " + bin(JTAG.get_Pin()))
    PD2_Blink()
    print("\tTDTest...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if (JTAG.get_Pin() & 0b00001111 == 0b00000100):
            break

    if (time.time() > t_end):
        print("Failed")
        print("Wrongly recieved: " + bin(JTAG.get_Pin()))
        End_Tests(Errors.JTAG_Error)

    print("\tSucessful " + bin(JTAG.get_Pin()))

    PD2_Blink()
    print("\tTMS Test...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if (JTAG.get_Pin() & 0b00001111 == 0b00000010):
            break

    if (time.time() > t_end):
        print("Failed")
        print("Wrongly recieved: " + bin(JTAG.get_Pin()))
        End_Tests(Errors.JTAG_Error)
    print("\tSucessful " + bin(JTAG.get_Pin()))

    PD2_Blink()
    print("\tTDI Test...")
    t_end = time.time() + 2

    while time.time() < t_end:
        if (JTAG.get_Pin() & 0b00001111 == 0b00000001):
            break

    if (time.time() > t_end):
        print("Failed")
        print("Wrongly recieved: " + bin(JTAG.get_Pin()))
        End_Tests(Errors.JTAG_Error)

    print("\tSucessful " + bin(JTAG.get_Pin()))

    PD2_Blink()

    print("Sucessful\n")
    # endregion JTAG-Test

    # region Display-Test
    PD2_Blink()

    print("Display Test...")
    print("\tCheck the Megacard Display. Can you read text there?\n\tMake sure to set a proper contrast on v4!")
    print("\tS2 sucessful. S1 else.")

    if S1_Or_S2_Decision() == 1:
        print("Failed")
        End_Tests(Errors.Display_Error)

    print("Sucessful")
    PD2_Blink()

    # endregion Display-Test

    End_Tests()
    # endregion Tests

while True:
    print("Starting testprogramm...")
    TestProgramm()
